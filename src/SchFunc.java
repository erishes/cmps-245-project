import generator.aggregation.elements.Relation;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import story.dbcore.exceptions.Scheherexception;
import story.scheherazade.elements.*;
import story.scheherazade.admin.StoryInterpreter;
import story.scheherazade.misc.NounName;
import story.scheherazade.misc.TimelineID;
import story.scheherazade.verbalizer.ModifierGrammaticalType;
import story.scheherazade.prologuer.Preposition;
import story.scheherazade.verbalizer.VerbalizerState;

/**
 * 1) Subordinate Clauses
 * 2) Prepositions
 */
public class SchFunc implements SchLexSynt{
    /** */
    String lexeme;

    /** */
    String word_class;

    /** */
    boolean sentBeg;

    /** */
    ArrayList<SchLexSynt> children;
    SchLexSynt parent;

    // for RST
    public static final String noRST = "";

    /**
     * Constructor
     * @param mod
     * @param type
     * @throws Scheherexception
     */
    public SchFunc(AssignedModifier mod, ModifierGrammaticalType type, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        lexeme = getLexeme(mod.getModifier().getType().toRawString()); //subordinating_conj
        // hack for 'of/toward/to something'
        if (lexeme.contains("something")) {lexeme = lexeme.substring(0, lexeme.indexOf("something")-1);}
        children = new ArrayList<SchLexSynt>();
        try {

            // 1) Subordinate Clause            // in order to, because
            if (type == ModifierGrammaticalType.SubordinateClause) {
                word_class = "preposition";

                // if 'of something'
                if (mod.getModifier().getType().toString().equals("^<of something>^")) {
                    for (Enumeration<Object> j = mod.getModifier().getSCHArguments().elements(); j.hasMoreElements(); ) {
                        Object argument = j.nextElement();
                        if (argument instanceof NounName) {
                            // prep can have only one argument
                            SchNoun noun = new SchNoun(storyInterpreter.findNounDefinition(timeline, (NounName)argument), ((NounName)argument).toString(), 1, storyInterpreter, timeline, verbalizerState);
                            //noun.setParent(this);
                            //children.add(noun);
                            setChild(noun);
                        } else {
                            System.err.println("#### MISSING HANDLING: ");
                            System.err.println("Modifier: " + this.getLexeme());
                            System.err.println("Argument: " + argument.getClass().toString());
                        }
                    }
                }
                else {
                    boolean subject = false;
                    boolean object = false;
                    int ind;
                    for (Enumeration<Object> j = mod.getModifier().getSCHArguments().elements(); j.hasMoreElements(); ) {
                        Object argument = j.nextElement();
                        boolean for_to_construction = !lexeme.startsWith("because");
                        // in order for subj to <verb> obj

                        // ValidAction
                        if (argument instanceof ValidAction) {
                            if (subject && object) {ind = 2;}
                            else {ind = 1;}
                            SchVerb verb = new SchVerb((ValidAction)argument, ind, storyInterpreter, timeline, verbalizerState);
                            if (for_to_construction) {
                                verb.setToInf(InfType.ForTo);
                                verb.setExtrapolated();
                            }
                            setChild(verb);
                        }
                        // Child is referring to an alternate timeline
                        else if (argument instanceof ValidTimeline) {
                            String tID = ((ValidTimeline) argument).getTimelineID().toString();
                            System.out.println(tID);
                            ArrayList<SchLexSynt> thisTimeline = StoryManager.accessTimeline(tID);
                            for (int i = 0; i < thisTimeline.size(); i++) {
                                children.add(thisTimeline.get(i));
                            }
                        }
                        // AssignedAction
                        else if (argument instanceof AssignedAction) {
                            SchLexSynt verb = StoryManager.returnAction((AssignedAction) argument, false);
                            children.add(verb);
                        }
                        // AssignedCondition
                        else if (argument instanceof AssignedCondition) {
                            SchLexSynt verb = StoryManager.returnAction((AssignedCondition) argument, false);
                            children.add(verb);
                        }

                        // ValidCondition
                        else if (argument instanceof ValidCondition) {
                            if (subject && object) {ind = 2;}
                            else {ind = 1;}
                            SchVerb verb = new SchVerb((ValidCondition) argument, ind, storyInterpreter, timeline, verbalizerState);
                            if (for_to_construction) {
                                verb.setToInf(InfType.ForTo);
                                verb.setExtrapolated();
                            }
                            setChild(verb);
                        }

                        // NounName
                        else if (argument instanceof NounName) {
                            if (!subject) {
                                subject = true;
                                ind = 1;
                            }
                            else {
                                object = true;
                                ind = 2;
                            }
                            SchNoun noun = new SchNoun(storyInterpreter.findNounDefinition(timeline, (NounName)argument), ((NounName)argument).toString(), ind, storyInterpreter, timeline, verbalizerState);
                            setChild(noun);
                        }

                        // I think it's Scheherade's bug that prepositional phrases have ModifierGrammaticalType.SubordinateClause
                        // fixing it here
                        else if (argument instanceof Preposition) {
                            Object noun = j.nextElement();
                            if ((noun instanceof NounName)) {
                                SchLexSynt prep_phrase = new SchFunc((Preposition) argument, (NounName) noun, storyInterpreter, timeline, verbalizerState);
                                prep_phrase.setParent(this);
                                this.lexeme = prep_phrase.getFeatures().get("lexeme");
                                children.addAll(prep_phrase.getChildren());
                            } else {
                                System.err.println("#### Unexpected prepositional phrase: " + argument.toString() + " " + noun.toString());
                            }
                        }

                        else {
                            System.err.println("#### MISSING HANDLING: ");
                            System.err.println("Modifier: " + this.getLexeme());
                            System.err.println("Argument: " + argument.getClass().toString());
                        }
                    }
                }
            }

            // 2) Preposition
            // with
            else if (type == ModifierGrammaticalType.Preposition) {
                word_class = "preposition";
                boolean subject = false;
                boolean object = false;
                int ind;

                for (Enumeration<Object> j = mod.getModifier().getSCHArguments().elements(); j.hasMoreElements();){
                    Object argument = j.nextElement();
                    if (argument instanceof NounName){
                        if (!subject) {
                            subject = true;
                            ind = 1;
                        }
                        else {
                            object = true;
                            ind = 2;
                        }
                        SchNoun noun = new SchNoun(storyInterpreter.findNounDefinition(timeline, (NounName)argument), ((NounName)argument).toString(), ind, storyInterpreter, timeline, verbalizerState); // prep can have only one argument
                        //noun.setParent(this);
                        //children.add(noun);
                        setChild(noun);
                    }
                    else
                    {
                        System.err.println("#### MISSING HANDLING: ");
                        System.err.println("Modifier: " + this.getLexeme());
                        System.err.println("Argument: "+argument.getClass().toString());
                    }
                }
            }
        } catch (Scheherexception e) {System.err.println("#### MISSING HANDLING: ");}
    }

    /**
     * Constructor
     * @param prep
     * @param argument
     */
    public SchFunc(Preposition prep, Object argument, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception{
        lexeme = getLexeme(prep.toString());
        children = new ArrayList<SchLexSynt>();
        word_class = "preposition";
        if (argument instanceof NounName){
            // prep can have only one argument
            SchNoun noun = new SchNoun(storyInterpreter.findNounDefinition(timeline, (NounName)argument), ((NounName)argument).toString(), 1, storyInterpreter, timeline, verbalizerState);
            //noun.setParent(this);
            //children.add(noun);
            setChild(noun);
        }

    }

    /**
     * Constructor
     * @param conj
     * @param children
     */
    public SchFunc(String conj, ArrayList<SchLexSynt> children){
        lexeme = conj;
        this.children = children;
        word_class = "coordinating_conj";
    }

    /**
     * Constructor
     * @param lexeme
     */
    public SchFunc(String lexeme){
        this.lexeme = lexeme;
        this.children = new ArrayList<SchLexSynt>();
        word_class = "coordinating_conj";
    }

    public SchFunc createCopy(){
        SchFunc newFunc = new SchFunc(this.lexeme);
        newFunc.word_class = this.word_class;
        newFunc.sentBeg = this.sentBeg;
        for (SchLexSynt child : this.children) {
            newFunc.children.add(child.createCopy());
        }
        return newFunc;
    }

    /**
     * By default, initialized to past
     * @param t
     */
    public void setTense(String t) {
        // recurse on children
        for (SchLexSynt child: this.children) {
            if (child instanceof SchVerb) {
                ((SchVerb) child).setTense(t);
            }
        }
    }

    /**
     * need to parse the modifier name into functional word + frame info
     * @param modifierInfo
     * @return
     */
    public String getLexeme(String modifierInfo) {
        String[] schLexeme = modifierInfo.split("(?=\\p{Upper})");
        String res = schLexeme[0];
        for (int i = 1; i < schLexeme.length; i++) {
            String next = schLexeme[i].toLowerCase();
            // ignore special words at the end if they are there
            if (next.equals("action") || next.equals("condition") || next.equals("timeline")
                    || next.equals("assigned") || next.equals("prop") || next.equals("for")) {continue;}
            res = res + "_" + next;
        }
        return res;
    }

    /**
     *
     * @return
     */
    public String getLexeme() {
        return this.lexeme;
    }

    /**
     *
     * @return
     */
    public String getRelation(){
        if (lexeme.equals("and")) {return "COORD";}
       // else if (lexeme.equals("to")) {return "II";}
        else return "ATTR";
    }

    /**
     *
     * @return
     */
    public Map<String,String> getFeatures(){
        HashMap<String, String> features = new HashMap<String, String>();
        if (lexeme != "TO"){features.put("class", word_class);}
        features.put("lexeme", lexeme);
        features.put("rel", getRelation());
        return features;
    }

    /**
     * Returns the elements this node governs
     * @return
     */
    public ArrayList<SchLexSynt> getChildren(){
        return children;
    }

    /**
     * Sets the elements this node governs
     * @param child
     */
    public void setChild(SchLexSynt child){
        this.children.add(child);
        child.setParent(this);
    }

    public void setParent(SchLexSynt p) {parent = p;}
    public SchLexSynt getParent() {return parent;}

    /**
     * Removes any children of this node
     */
    public void removeChildren(){this.children = new ArrayList<SchLexSynt>(); }

    /**
     *
     * @return
     */
    public boolean checkTextPlanConditions() {
        // TODO Check if there exist a mapping into Personage aggregation operators
        if(this.lexeme.equals("because")){return true;}
        return false;
    }

    /**
     *
     * @return
     */
    public String getRST() {
        // TODO Mapping to Personage aggregation operators (generator.aggregation.elements.Relation.java and Operation.java)
        if(this.lexeme.equals("because")){return Relation.JUSTIFY;}
        else if(this.lexeme.startsWith("SPEECH")){return Relation.DIRECT_SPEECH;}
        return Relation.SINGLE;
    }

    /**
     * Outputs the name of the speaker for a direct speech dsynt
     * @return
     */
    public String getVoice() {
        if(this.lexeme.startsWith("SPEECH_") && !this.lexeme.endsWith("_")){
            return this.lexeme.split("_")[1];
        }
        return "";
    }

}