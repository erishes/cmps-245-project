import edu.mit.jverbnet.data.*;
import edu.mit.jverbnet.index.IVerbIndex;
import edu.mit.jverbnet.index.VerbIndex;
import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.*;
import net.didion.jwnl.dictionary.Dictionary;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import edu.mit.jverbnet.data.*;
import edu.mit.jverbnet.index.IVerbIndex;
import edu.mit.jverbnet.index.VerbIndex;
import edu.mit.jverbnet.*;
import story.dbcore.exceptions.Scheherexception;

/**
 * Created by Steph on 8/4/2015.
 */
public class NCUS {

    public static Dictionary dictionary;
    public static IVerbIndex index;

    /**********************************************/

    public static void accessVerbnet(WordnetKey key, ArrayList<String> params) throws Scheherexception{

        Set<IMember> members = index.getMembers(key);
        if (members.isEmpty()) {
            System.out.println("No entry for " + key + " in VerbNet :(");
            return;
        }

        for(IMember member: members) {
            System.out.println(member.getName());
            IVerbClass verb = member.getVerbClass();
            IVerbClass parent = verb.getParent();

            if (parent != null) {
                System.out.println("Parent ID: " + parent.getID());
                System.out.println("# Thematic Roles: " + parent.getThematicRoles().size());
                for (IThematicRole role : parent.getThematicRoles()) {
                    System.out.println("Role type: " + role.getType());
                }
                System.out.println("# Frames: " + parent.getFrames().size());
                for (IFrame frame : parent.getFrames()) {
                    createFrame(frame, key.getLemma(), params);
                }
            }

            System.out.println("ID: " + verb.getID());
            System.out.println("Subclass: " + verb.getSubclasses());
            System.out.println("# Thematic Roles: " + verb.getThematicRoles().size());
            for (IThematicRole role: verb.getThematicRoles()) {
                System.out.println("Role type: " + role.getType());
            }

            System.out.println("# Frames: " + verb.getFrames().size());
            for (IFrame frame: verb.getFrames()) {
                createFrame(frame, key.getLemma(), params);
            }
        }
    }

    public static void createFrame(IFrame frame, String verbString, ArrayList<String> params) throws Scheherexception{
        BuildDSyntS dsynts = new BuildDSyntS(); // here for testing

        FrameType type = frame.getPrimaryType();
        String example = frame.getExamples().get(0);
        System.out.println("frame type: " + type.getID());
        System.out.println("example: " + example);


        ArrayList<String> stringPOS = new ArrayList<String>();
        for (String elt : type.getID().split(" ")) {
            stringPOS.add(elt);
        }

        // TESTING
        //ArrayList<String> stringPOS = new ArrayList<String>();
        //stringPOS.add("NP");
        //stringPOS.add("V");
        //stringPOS.add("PP.initial_location");
        //stringPOS.add("PP.destination");

        /**
         String[] params;

         // TESTING
         String[] go_params = {"hereos", "cabin", "cave"}; // subj, loc_from, loc_to
         String[] go_types = {"NP", "V", "PP.initial_location", "PP.destination"};
         stringPOS = go_types;
         params = go_params;


         String[] fight_params = {"heroes", "dragon", "cave"};
         String[] fight_types = {"NP", "V", "NP", "NP.location"};
         //stringPOS = fight_types;
         //params = fight_params;
         **/

        String[] selectedNouns = {};
        ArrayList<String> wordNetKeysNoun;
        for (String noun: params) {
            // TODO:
            if (noun.equals("heroes")) noun = "hero";
            long[] nounOffsets = getWordnetNounOffsets(noun);
            for (int i = 0; i < nounOffsets.length; i++) {
                if (nounOffsets[i] == 0) break;
                wordNetKeysNoun = getWordnetKeyNoun(nounOffsets[i]);
                // then iterate
                Iterator<String> iterator = wordNetKeysNoun.iterator();
                while (iterator.hasNext()) {
                    String wnKey = iterator.next();
                    System.out.println(wnKey);
                }
            }
        }

        // ignore the verb the verb first
        ArrayList<String> posOrder = new ArrayList<String>();
        for (int i = 0; i < stringPOS.size(); i++) {
            String elt = stringPOS.get(i);
            if (!(elt.equals("V"))) {
                posOrder.add(elt);
            }
        }

        //SchVerb verb = new SchVerb("go");
        SchVerb verb = new SchVerb(verbString);


        for (int i = 0; i < posOrder.size(); i++) {
            String word = params.get(i);

            if (posOrder.get(i).equals("NP")) {
                SchNoun noun = new SchNoun(word, i);
                verb.setChild(noun);
            }
            else if (posOrder.get(i).equals("NP.location")) {
                SchNoun noun = new SchNoun(word, i);
                noun.setChild(new SchFunc("at"));
                verb.setChild(noun);
            }
            else if (posOrder.get(i).equals("PP")) {
                verb.setChild(new SchFunc("prep"));
            }
            else if (posOrder.get(i).equals("PP.initial_location")) { // from
                SchNoun noun = new SchNoun(word, i);
                noun.setChild(new SchFunc("from"));
                verb.setChild(noun);
            }
            else if (posOrder.get(i).equals("PP.destination") || posOrder.get(i).equals("PP.location")) { // to
                SchNoun noun = new SchNoun(word, i);
                noun.setChild(new SchFunc("to"));
                verb.setChild(noun);
            }
            else if (posOrder.get(i).equals("PP.recipient")) {
                SchNoun noun = new SchNoun(word, i);
                verb.setChild(noun);
            }
            else if (posOrder.get(i).equals("PP.location")) { // out of the box
                SchNoun noun = new SchNoun(word, i);
                noun.setChild(new SchFunc("from"));
                verb.setChild(noun);
            }
            else if (posOrder.get(i).equals("NP.theme")) {

            }
            else if (posOrder.get(i).equals("PP.agent")) {

            }
            else if (posOrder.get(i).equals("PP.theme")) {

            }
            else if (posOrder.get(i).equals("PP.source")) {

            }
            else if (posOrder.get(i).equals("PP.co-agent")) {

            }
            else if (posOrder.get(i).equals("PP.beneficiary")) {

            }
            else if (posOrder.get(i).equals("PP.topic")) {

            }
            else if (posOrder.get(i).equals("PP.patient")) {

            }
            else if (posOrder.get(i).equals("PP.instrument")) {

            }
            else if (posOrder.get(i).equals("There")) {
                verb.setChild(new SchFunc("there"));
            }
        }

        System.out.println(verb);
        dsynts.addDSynt(verb);
        String directory = "C:\\Users\\Stephanie\\scheherazade-0.34-src\\scheherazade\\cmps-245-project\\data\\";

        String name = verbString;
        for (String elt: stringPOS) {
            name += "-" + elt;
        }

        String xml_file = directory + "xml/fantasy/test_" + name + ".xml";
        dsynts.saveDSynt(xml_file);
    }

    /**********************************************/

    // If we don't have the word offset, only the word
    public static long[] getWordnetNounOffsets(String noun) {
        long[] offsets = new long[10];
        try {
            IndexWord word = dictionary.getIndexWord(POS.NOUN, noun);
            //System.out.println(word.getLemma());

            // get synsets
            long[] nounSynset = word.getSynsetOffsets();
            for (int j = 0; j < nounSynset.length; j++) {
                offsets[j] = nounSynset[j];
            }
        }
        catch (JWNLException e) {
            System.err.println("Error thrown when quering wordnet: "+e.getMessage());
        }
        return offsets;
    }

    // If we have the wordnet offset
    public static ArrayList<String> getWordnetKeyNoun(long offset) {
        ArrayList<String> keys = new ArrayList<String>();
        try{
            // get the wordnet syset for the word given offset and POS
            Synset nounSynset = dictionary.getSynsetAt(POS.NOUN, offset);
            //System.out.println(nounSynset.getLexFileName());
            PointerTarget[] similarToTargets = nounSynset.getTargets();
            List<String> candidateSyns = new ArrayList<String>(nounSynset
                    .getWordsSize()
                    + similarToTargets.length);
            for (int j = 0; j < nounSynset.getWordsSize(); j++) {
                candidateSyns.add(nounSynset.getWords()[j].getLemma());
            }

            for (Word word: nounSynset.getWords()) {
                keys.add(word.getLemma());
            }
        }
        catch (JWNLException e) {
            System.err.println("Error thrown when quering wordnet: "+e.getMessage());
        }
        return keys;
    }



    /**********************************************/

    // If we don't have the offset, only the word
    // Returns an array list of wordnet offsets
    public static long[] getWordnetVerbOffsets(String verb) throws Exception {
        long[] offsets = new long[50];
        IndexWord word = dictionary.getIndexWord(POS.VERB, verb);
        //System.out.println(word.getLemma());
        // get synsets
        long[] nounSynset = word.getSynsetOffsets();
        for (int j = 0; j < nounSynset.length; j++) {
            offsets[j] = nounSynset[j];
        }
        return offsets;
    }

    // If we have the wordnet offset, we can create the wordnet key
    // Returns an array list of wordnet keys
    public static ArrayList<WordnetKey> getWordnetKeyVerb(long offset) throws Exception {
        ArrayList<WordnetKey> keys = new ArrayList<WordnetKey>();
        try{
            // get the wordnet syset for the word given offset and POS
            Synset verbSynset = dictionary.getSynsetAt(POS.VERB, offset);
            //System.out.println(verbSynset.getLexFileName());
            for (Word word: verbSynset.getWords())
            {
                //System.out.println(word.getLemma());
                long lex_filenum = verbSynset.getLexFileNum();
                long lex_id = word.getLexId();
                WordnetKey key = new WordnetKey(word.getLemma(), 2, (int)lex_filenum, (int)lex_id);
                keys.add(key);
            }
        }
        catch (JWNLException e) {
            System.err.println("Error thrown when quering wordnet: "+e.getMessage());
        }
        return keys;
    }








    public static void main(String[] args) throws Exception, IOException {
        // extract nouns, verbs, and arguments
        HashMap<String, String> nouns = new HashMap<String, String>();
        HashMap<String, ArrayList<String>> verbs_args = new HashMap<String, ArrayList<String>>();

        ArrayList<String> characters = new ArrayList<String>();//{"princess", "king", "dragon", "heroes"};
        ArrayList<String> locations = new ArrayList<String>();//{"castle", "foreign-land", "cave"};
        ArrayList<String> props = new ArrayList<String>();//{"mushrooms"};
        ArrayList<String> steps = new ArrayList<String>();//{"go_1", "search_2", "eat_3", "go_4", "get-hungry_5", "go_6", "drag-off_7"};
        ArrayList<String> verbs = new ArrayList<String>();//{"go", "search", "eat", "get-hungry", "drag-off", "scream", "fight", "rescue", "return-for-reward"};

        BufferedReader in;

        try {
            in = new BufferedReader(new FileReader("C:/Users/Stephanie/UCSC/NCSU/dragon-discourse-problem.pddl"));
            String nextline = in.readLine();
            while ((nextline = in.readLine()) != null) {
                //nextline = in.readLine();
                System.out.println(nextline);
                if (nextline.contains("story-world")) {
                    String noun = nextline.replace("    ", "").split(" ")[0];
                    String type = "";

                    if (nextline.contains("person")) {type="person";}
                    else if (nextline.contains("hero")) {type="hero";}
                    else if (nextline.contains("monster")) {type="monster";}
                    else if (nextline.contains("place")) {type="place";}
                    else if (nextline.contains("lair")) {type="lair";}
                    else if (nextline.contains("food")) {type="food";}
                    nouns.put(noun,type);

                    if (nextline.contains("person") || nextline.contains("monster") || nextline.contains("hero") ) {
                        characters.add(noun);}
                    else if (nextline.contains("place") || nextline.contains("lair")) {
                        locations.add(noun);}
                    else if (nextline.contains("food")) {
                        props.add(noun);}
                }

                else if (nextline.contains(" - step")) {
                    String verb = nextline.replace("    ", "").split(" ")[0];
                    if (verb.contains("initial") || verb.contains("goal")) {continue;}
                    verbs_args.put(verb, new ArrayList<String>());

                    steps.add(verb);
                    if (!verbs.contains(verb.split("_")[0])) {verbs.add(verb.split("_")[0]);}
                }

                else if (nextline.contains("(has-parameter")) {
                    String verb = nextline.replace("    (","").split(" ")[1];
                    String param = nextline.replace("    (","").split(" ")[2];
                    String arg = nextline.replace("    (","").split(" ")[3].replace(")\n","").replace(")","");
                    verbs_args.get(verb).add(new Integer(arg),param);

                }
                else if (nextline.contains("(has-subject")) {
                    String verb = nextline.replace("    (","").split(" ")[1];
                    String subject = nextline.replace("    (","").split(" ")[2].replace(")\n","").replace(")","");
                    //verbs_args.get(verb).put("subject",subject);
                }

            }
        } catch (IOException ioe) {
            System.err.println("oops ");
        }




        try {
            JWNL.initialize(new FileInputStream("C:/Users/Stephanie/UCSC/jwnl-tests/jwnl/file_properties.xml"));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        String pathToVerbnet = "C:\\Users\\Stephanie\\UCSC\\verb\\verbnet\\";
        URL url = new URL("file", null, pathToVerbnet);

        // construct the index and open it
        index = new VerbIndex(url);
        index.open();

        // wordney key
        // lemma % lex_sense
        // where lex_sense is encoded as:
        // ss_type:lex_filenum:lex_id:head_word:head_id
        // lex_filenum = lex_filenum is a two digit decimal integer representing the name of the lexicographer file containing the synset for the sense.
        // lex_id = lex_id is a two digit decimal integer that, when appended onto lemma , uniquely identifies a sense within a lexicographer file.
        dictionary = Dictionary.getInstance();

        long verb_see_offset = 2129289; // see%2:39:00
        long noun_adult_offset = 9605289; //adult%1:18:00
        long verb_feel_offset = 1771535;
        long verb_show_offset = 664788;
        long verb_rescue_offset = 2556841; // rescue%2:41:00
        String verb_rescue = "rescue";

        /*(:objects
        princess - story-world-person
        king - story-world-person
        dragon - story-world-monster
        heroes - story-world-hero
        castle - story-world-place
        cabin - story-world-place
        foreign-land - story-world-place
        cave - story-world-lair
        mushrooms - story-world-food
        initial0 - step
        go_1 - step
        search_2 - step
        eat_3 - step
        go_4 - step
        get-hungry_5 - step
        go_6 - step
        drag-off_7 - step
        eat_8 - step
        go_9 - step
        scream_10 - step
        go_11 - step
        fight_12 - step
        rescue_13 - step
        return-for-reward_14 - step
        get-hungry_15 - step
        goal16 - step
        */
        /**
         String[] persons = {"princess", "king"};
         String[] monster = {"dragon"};
         String[] hero = {"heroes"};
         String[] places = {"castle", "foreign-land"};
         String[] lair = {"cave"};
         String[] food = {"mushrooms"};

         String[] characters = {"princess", "king", "dragon", "heroes"};
         String[] locations = {"castle", "foreign-land", "cave"};
         String[] props = {"mushrooms"};

         String[] steps = {"go_1", "search_2", "eat_3", "go_4", "get-hungry_5", "go_6", "drag-off_7",
         "eat_8", "go_9", "scream_10", "go_11", "fight_12", "rescue_13", "return-for-reward_14", "get-hungry_15", "goal16"};
         //String[] verbs = {"go", "search", "eat", "get-hungry", "drag-off", "scream", "fight", "rescue", "return-for-reward"};
         **/
        /*
        (has-parameter get-hungry_15 princess 0)
        String[] get-hungry_params = {"princess"};

        (has-parameter go_1 princess 0)
        (has-parameter go_1 castle 1)
        (has-parameter go_1 forest 2)
        String[] go_params = {"princess", "castle", "forest"} // subj, loc_from, loc_to

        (has-parameter go_11 heroes 0)
        (has-parameter go_11 cabin 1)
        (has-parameter go_11 cave 2)
        Stirng go_params = {"hereos", "cabin", "cave"}; // subj, loc_from, loc_to

        (has-parameter fight_12 heroes 0)
        (has-parameter fight_12 dragon 1)
        (has-parameter fight_12 cave 2)
        String[] fight_params = {"heroes", "dragon", "cave"}; // subj, obj, in_prep

        (has-parameter rescue_13 heroes 0)
        (has-parameter rescue_13 dragon 1)
        (has-parameter rescue_13 princess 2)
        (has-parameter rescue_13 cave 3)
        String[] rescue_params = {"heroes", "dragon", "princess", "cave"}; // hero (I) rescues princess (II) from dragon (prep) in cave (prep)

        (has-parameter return-for-reward_14 heroes 0)
        (has-parameter return-for-reward_14 princess 1)
        (has-parameter return-for-reward_14 cave 2)
        (has-parameter return-for-reward_14 castle 3)
        String[] return-for-reward = {"heroes", "princess", "cave", "castle"}; // hero returns princess from cave for rewards to castle
         */



        /** NOUNS **/
        ArrayList<String> wordNetKeysNoun = null;

        // if we have the wn offset, first we get the wn keys
        wordNetKeysNoun = getWordnetKeyNoun(10231515);
        // then iterate
        Iterator<String> iterator = wordNetKeysNoun.iterator();
        while (iterator.hasNext()) {
            String wnKey = iterator.next();
            //System.out.println(wnKey);
        }

        // if we don't have wn offsets, first we need wn offsets

        long[] nounOffsets = getWordnetNounOffsets("king");
        for (int i = 0; i < nounOffsets.length; i++) {
            if (nounOffsets[i] == 0) break;
            wordNetKeysNoun = getWordnetKeyNoun(nounOffsets[i]);
            // then iterate
            iterator = wordNetKeysNoun.iterator();
            while (iterator.hasNext()) {
                String wnKey = iterator.next();
                //System.out.println(wnKey);
            }
        }


        /** VERBS **/
        ArrayList<WordnetKey> wordNetKeysVerb = null;

        // if we have the wn offset, first we get the wn keys
        wordNetKeysVerb = getWordnetKeyVerb(verb_feel_offset);
        // they iterate
        Iterator<WordnetKey> iteratorVerb = wordNetKeysVerb.iterator();
        while (iteratorVerb.hasNext()) {
            WordnetKey wnKey = iteratorVerb.next();
            //accessVerbnet(wnKey);
        }

        // if we don't have wn offsets, first we need wn offsets
        long[] verbOffsets = getWordnetVerbOffsets("go");
        for (int i = 0; i < verbOffsets.length; i++) {
            if (verbOffsets[i] == 0) break;
            wordNetKeysVerb = getWordnetKeyVerb(verbOffsets[i]);
            // then iterate
            iteratorVerb = wordNetKeysVerb.iterator();
            while (iteratorVerb.hasNext()) {
                WordnetKey wnKey = iteratorVerb.next();

                ArrayList<String> go_params = verbs_args.get("go_11"); //{"hereos", "cabin", "cave"}; // subj, loc_from, loc_to
                accessVerbnet(wnKey, go_params);
            }
        }
    }
}
