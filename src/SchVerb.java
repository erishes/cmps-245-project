import java.util.*;
import java.net.URL;

import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.Dictionary;

import edu.mit.jverbnet.index.*;
import edu.mit.jverbnet.data.*;

import story.dbcore.exceptions.Scheherexception;
import story.scheherazade.verbalizer.VerbalizerState;
import story.scheherazade.elements.ValidPredicate;
import story.scheherazade.elements.AssignedCondition;
import story.scheherazade.elements.AssignedAction;
import story.scheherazade.elements.TransitionTime;
import story.scheherazade.elements.ValidTimeline;
import story.scheherazade.misc.ActionType;
import story.scheherazade.prologuer.PredicateMode;
import story.scheherazade.elements.ValidAction;
import story.scheherazade.prologuer.Preposition;
import story.scheherazade.elements.ValidCondition;
import story.scheherazade.misc.*;
import story.scheherazade.admin.StoryInterpreter;
import story.scheherazade.misc.TimelineID;
import story.scheherazade.elements.ValidNoun;

import story.scheherazade.elements.*;
import story.scheherazade.verbalizer.*;


/**
 * Lexico-syntactic class for Verbs
 */
class SchVerb implements SchLexSynt{
    AssignedPredicate assignedPredicate;
    ValidAction validAction;
    ValidCondition validCondition;
    boolean assignedAction;

	/** */
    GenericScheherID action;

    /** */
	String supertype;

    /** */
	boolean isPredicate, isExtrapolated, isAble, conditionEnds, conditionBegins;

    String sourceText;

    /** */
    String lexeme;

    String tense;

    String mood; // conditional "would be"

    boolean isSpeech; // this is the head verb of a speech act; later decide if direct, indirect, free, or narrator
    String speakerName, speakerType; // if isSpeech, we have a speaker

    /** distinguish between inf with and without 'to' */
	InfType inf;

    /** */
	int truthDegree;

    /** */
    int index;

    /** */
	TransitionTime time;

    /** */
	Conditionality conditionality;

    /** */
	PredicateMode mode;

    /** */
	StringBuffer modifier;

    boolean isQuestion;

    boolean noSubject = false;

    int offset;

    /** */
	ArrayList<SchLexSynt> children;
    SchLexSynt parent;

    /** New to keep record of interpretation links */
    ArrayList<InterpArc> interpretationArcs;

    ArrayList<String> emotions; // might later change to be incorporated into a text plan

    /**
     *
     * @param
     */

    // AssignedAction, AssignedCondition
    public SchVerb(AssignedPredicate ap, int ind, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        assignedPredicate = ap;
        if (ap instanceof AssignedAction) {
            validAction = ((AssignedAction) ap).getAction();
            action = validAction.getType();
            truthDegree = validAction.getTruthDegree();
            mode = validAction.getMode();
            validCondition = null;
        }
        else if (ap instanceof AssignedCondition) {
            validCondition = ((AssignedCondition) ap).getCondition();
            action = validCondition.getType();
            truthDegree = validCondition.getTruthDegree();
            mode = validCondition.getMode();
            validAction = null;
        }

        //isPredicate = main;
        //if (isPredicate) {index = 0;}
        //else {index = 1;}
        index = ind;
        inf =  InfType.NoInf;
        children = new ArrayList<SchLexSynt>();
        interpretationArcs = new ArrayList<InterpArc>();
        tense = "";
        mood = "";
        isQuestion = false;
        isSpeech = false;
        emotions = null;
        this.setChildren(storyInterpreter, timeline, verbalizerState);
        this.setModifiers(storyInterpreter, verbalizerState);
    }

    // ValidAction (no modifiers)
    public SchVerb(ValidAction v, int ind, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        assignedPredicate = null;
        action = v.getType();
        truthDegree = v.getTruthDegree();
        mode = v.getMode();
        validAction = v;
        validCondition = null;
        //isPredicate = main;
        //if (isPredicate) {index = 0;}
        //else {index = 1;}
        index = ind;
        inf =  InfType.NoInf;
        children = new ArrayList<SchLexSynt>();
        interpretationArcs = new ArrayList<InterpArc>();
        tense = "";
        mood = "";
        isQuestion = false;
        isSpeech = false;
        emotions = null;
        this.setChildren(storyInterpreter, timeline, verbalizerState);
        this.setModifiers(storyInterpreter, verbalizerState);
    }

    // ValidCondition (no modifiers)
    public SchVerb(ValidCondition v, int ind, StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception{
        assignedPredicate = null;
        validAction = null;
        validCondition = v;
        children = new ArrayList<SchLexSynt>();
        interpretationArcs = new ArrayList<InterpArc>();
        action = v.getType();
        truthDegree = v.getTruthDegree();
        inf =  InfType.NoInf;
        //isPredicate = main;
        //if (isPredicate) {index = 0;}
        //else {index = 1;}
        index = ind;
        tense = "";
        mood = "";
        isQuestion = false;
        isSpeech = false;
        this.setChildren(storyInterpreter, timeline, verbalizerState);
        this.setModifiers(storyInterpreter, verbalizerState);
    }

    // input of the format:
    // verb1825237_want_something_wants_some_proposition
    // or
    // rescue
    public SchVerb(String word_info) throws Scheherexception {
        validAction = null; // not necessary to recover this info
        validCondition = null; // not necessary to recover this info

        if (word_info.contains("_")) {
            action = new ActionType(word_info);
        }
        else {
            lexeme = word_info;
        }

        mode = PredicateMode.ActionAssert;
        index = 1;
        isPredicate = true;
        inf =  InfType.NoInf;
        children = new ArrayList<SchLexSynt>();
        interpretationArcs = new ArrayList<InterpArc>();
        tense = "";
        mood = "";
        isQuestion = false;
        isSpeech = false;
        emotions = null;
    }

    /////////////////////

    public void setChildren(StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        ArrayList<SchLexSynt> arguments = getArguments(storyInterpreter, timeline, verbalizerState);
        // has been reassigned from a condition begins or ends; if so, then we can skip this part
        if (conditionEnds || conditionBegins) {return;}


        //adjectives
        if (action.toString().contains("#adj")) {
            // only need to reassign children and add an adjective node if we haven't already done a copyVerb()
            if (children.isEmpty()) {
                // set WN offset for adjective and remove it from this
                SchAdj adj = new SchAdj(action.toString());
                setChild(adj);
                offset = -1;

                // append any additonal children
                for (SchLexSynt arg : arguments) {
                    setChild(arg);
                }
            }
        }

        else if (!arguments.isEmpty()) {
            this.children = arguments;
        }

        // condition imperative
        // told the division ValidCondition I) expedient (adj) II) the division
        // -> told the divisiion expedient
        if (validCondition != null && validCondition.getMode() == PredicateMode.ConditionImperative) {
        //    removeSubj();

        }
    }

    /**
     * action is either of type ValidAction or ValidCondition, or an assignedPredicate, which has more information, but is not necessary for this method
     * look at the children of the current verb and determine what they are
     * Options include: noun, preposition, verb, timeline
     * @param
     * @return
     * @throws Scheherexception
     */
    public ArrayList<SchLexSynt> getArguments(StoryInterpreter storyInterpreter, TimelineID timeline, VerbalizerState verbalizerState) throws Scheherexception {
        ArrayList<SchLexSynt> res = new ArrayList<SchLexSynt>();
        int noun_ind = 0;
        Enumeration<Object> j;

        // ValidAction or ValidCondition
        if (assignedPredicate == null) {
            if (validAction != null) {j = validAction.getSCHArguments().elements();}
            else {j = validCondition.getSCHArguments().elements();}
        }
        // AssignedAction or AssignedCondition
        else {j = assignedPredicate.getValidPredicate().getSCHArguments().elements();}

        boolean subject = false;
        boolean object = false;
        int ind;
        while (j.hasMoreElements()){
            // getSCHArguments may return nested propositions
            Object argument = j.nextElement();

            // 1) Child is a noun
            if (argument instanceof NounName){
                ValidNoun n = storyInterpreter.findNounDefinition(TimelineID.REALITY, (NounName)argument);
                if (subject && object) {ind = noun_ind;}
                else if (!subject) {
                    subject = true;
                    ind = 0;
                }
                else {
                    object = true;
                    ind = 1;
                }
                SchNoun noun = new SchNoun(n, ((NounName)argument).toString(), ind, storyInterpreter, TimelineID.REALITY, verbalizerState);
                noun.setParent(this);
                res.add(noun);
                noun_ind++;
            }

            // 2) Child is a prepositional phrase
            else if (argument instanceof Preposition){
                // check whether there is a next element and it is a NounType
                Object noun = j.nextElement();
                if ((noun instanceof NounName)){
                    SchLexSynt prep_phrase = new SchFunc((Preposition) argument, (NounName) noun, storyInterpreter, timeline, verbalizerState);
                    prep_phrase.setParent(this);
                    res.add(prep_phrase);
                }
                else {
                    System.err.println("#### Unexpected prepositional phrase: " + argument.toString() + " " + noun.toString());
                }

            }

            // 3) Child is verb that is not the head
            // The narrator resumed reaching
            else if (argument instanceof ValidAction){
                if (subject && object) {ind = 2;}
                else {ind = 1;}
                SchVerb verb = new SchVerb((ValidAction) argument, ind, storyInterpreter, timeline, verbalizerState);
                verb.setParent(this);
                res.add(verb);
            }

            // 3) Child is condition that is not the head
            // The narrator noticed the ankle [was observed]
            // I shockingly saw the slimy bugs the bugs [were on] my apartment's ceiling.
            else if (argument instanceof ValidCondition){
                if (subject && object) {ind = 2;}
                else {ind = 1;}
                SchVerb verb = new SchVerb((ValidCondition)argument, ind, storyInterpreter, timeline, verbalizerState);
                verb.setParent(this);
                // this is a hack to make "say something AS proposition" work without handing gerunds
                // Ex fox saw that the group of grapes was sour
                // instead of
                // fox saw group of grapes as being sour
                if (noun_ind > 1 && res.get(1) instanceof SchNoun){
                    //res.remove(1); // remove object
                }

                // if the head is not a valid action (i.e. lexeme = "") we reassign this new verb to this verb
                if (getLexeme().equals("") || verb.action.toString().contains("conditionEnds") || verb.action.toString().contains("conditionBegins")) {
                    if (action.toString().contains("conditionEnds")) {conditionEnds = true;}
                    if (action.toString().contains("conditionBegins")) {conditionBegins = true;}
                    copyVerb(verb);
                }
                else { // else we add the new verb as a child
                    res.add(verb);
                }


            }

            // 4) Child is referring to an alternate timeline
            else if (argument instanceof ValidTimeline) {
                String tID = ((ValidTimeline) argument).getTimelineID().toString();
                ArrayList<SchLexSynt> thisTimeline = StoryManager.accessTimeline(tID);
                for (int i = 0; i < thisTimeline.size(); i++) {
                    res.add(thisTimeline.get(i));
                }
            }

            // 5) Child is referring to an assigned condition, another timeline node previously created
            else if (argument instanceof AssignedCondition) {
                if (subject && object) {ind = 2;}
                else {ind = 1;}
                SchVerb verb = new SchVerb((AssignedCondition)argument, ind, storyInterpreter, timeline, verbalizerState);
                verb.setParent(this);
                // this is a hack to make "say something AS proposition" work without handing gerunds
                // Ex fox saw that the group of grapes was sour
                // instead of
                // fox saw group of grapes as being sour
                if (noun_ind > 1 && res.get(1) instanceof SchNoun){
                    //res.remove(1); // remove object
                }
                res.add(verb);
            }

            // 5) Child is referring to an assigned action, another timeline node previously created
            // X enjoyed that [assigned action]
            else if (argument instanceof AssignedAction) {
                SchVerb verb = new SchVerb((AssignedPredicate) argument, 1, storyInterpreter, timeline, verbalizerState);
                verb.setTense("past");
                verb.assignedAction = true;
                SchAttr that = new SchAttr("", "that");
                that.setRelation("ATTR");
                that.setChild(verb);
                that.setParent(this);
                res.add(that);
            }

            else if (argument instanceof ValidNoun) {
                if (!subject) {
                    subject = true;
                    ind = 0;
                }
                else {
                    object = true;
                    ind = 1;
                }
                SchNoun noun = new SchNoun((ValidNoun) argument, ((ValidNoun) argument).getNounType().getMenuName(), ind, storyInterpreter, TimelineID.REALITY, verbalizerState);
                noun.setParent(this);
                res.add(noun);
                noun_ind++;

            }

            else {
                System.err.println("#### MISSING HANDLING: ");
                System.err.println("Story action: " + this.getLexeme());
                System.err.println("Argument: "+argument.getClass().toString());
            }

        }

        // TODO: if no arguments
        // "It snowed"
        //if (j. == 0) {
        //    setExtrapolated();
        //    noSubject = true;
        //}
        return res;
    }

    /**
     * action is either of type ValidAction or ValidCondition, or an assignedPredicate, which has more information, but is not necessary for this method
     * @param storyInterpreter
     * @param verbalizerState
     * @throws Scheherexception
     */
    public void setModifiers(StoryInterpreter storyInterpreter, VerbalizerState verbalizerState) throws Scheherexception{
        if (assignedPredicate == null) {return;}

        /**
         * 1) adverb
         *   a) future tense
         *   b) present tense
         *   c) direct speech (not exclusive) -- moved!
         *   d) adverb is added as a child of this verb (not future and present indicators)
         * 3) other
         */
        // speaker_type is the noun, e.x. wildcat, person
        // speaker_name may be defined, e.x. Jenny, Bob
        // if there is no supplied speaker name, speaker_name will default to the empty string
        // and we need to pass speaker_type to BuildDsynts class to fill 'voice' attribute of textplan
        // otherwise, can pass speaker_name as the 'voice
        String speaker_type = "";
        String speaker_name = "";

        for (Iterator<AssignedModifier> j = storyInterpreter.getModifiersOfPredicate(assignedPredicate).iterator(); j.hasNext(); ) {
            AssignedModifier mod = j.next();
            // get info about modifier type (adverbial / prepositional / attaches sub clause)
            ModifierGrammaticalType modGramType = verbalizerState.getPlanners().getModifierPlanner(mod.getModifier().getType()).getGrammaticalType();
            if (mod != null) {

                // inOrderForAction
                // 1) Adverbial modifiers only
                if (modGramType == ModifierGrammaticalType.Adverbial){
                    // TODO: HACK FOR DIRECTLY; we're getting rid of it
                    if (mod.getModifier().getType().toRawString().contains("directly")) {continue;}
                    SchAdverb adv = new SchAdverb(mod);
                    adv.setParent(this);
                    boolean presentTense = false;
                    boolean futureTense = false;

                    // a) future tense
                    // handle tense in this way if the verb is a verb of communication
                    // doesn't have to only be direct speech
                    // e.g. "I will try for me to obtain the cheese", the crow thought.
                    // e.g. The crow through she will try for her to obtain the cheese.
                    if (adv.getLexeme().equals("later") && this.isVerbOfSpeech()){
                        futureTense = true;
                        System.err.println("FOUND DIRECT SPEECH INSTANCE");
                        SchVerb vp_speech_frame= null;

                        try{
                            // if we are communicating a verb frame
                            // don't add modifier as a child of the verb
                            // change tense of the children
                            for (SchLexSynt child: this.getChildren()){
                                if (child instanceof SchVerb) {
                                    // rule: later modifier; don't realize it but put the verb in the future
                                    vp_speech_frame = ((SchVerb)child);
                                    vp_speech_frame.setTense("future");
                                }
                            }
                        }
                        catch (ClassCastException s) {
                            throw new Scheherexception("Unexpected direct speech structure in ");
                        }
                    }

                    // b) present tense
                    // handle tense in this way if the verb is a verb of communication
                    // doesn't have to only be direct speech
                    // e.g. The crow thought "I am scared of the dairyman."
                    // e.g. The crow thought she is scared of the dairyman.
                    if (adv.getLexeme().equals("now") && this.isVerbOfSpeech()){
                        presentTense = true;
                        System.err.println("FOUND DIRECT SPEECH INSTANCE");
                        SchVerb vp_speech_frame= null;

                        try{
                            // if we are communicating a verb frame
                            // don't add modifier as a child of the verb
                            // only change tense of the children
                            for (SchLexSynt child: this.getChildren()){
                                if (child instanceof SchVerb) {
                                    // rule: now modifier; don't realize it but put the verb in the present
                                    vp_speech_frame = ((SchVerb)child);
                                    vp_speech_frame.setTense("present");
                                }
                            }
                        }
                        catch (ClassCastException s) {
                            throw new Scheherexception("Unexpected direct speech structure in ");
                        }
                    }

                    // d) regular adverbial modifier
                    else
                    {
                        if (presentTense || futureTense) {continue;}
                        this.setModifier(adv);
                        System.out.println(this.action.toString() + " " + this.modifier);
                    }



                }

                // 2) not an adverbial modifier
                // subordinate clause or prep: in order to, because, but, with something, of something
                else
                {
                    SchFunc adjunct = new SchFunc(mod, modGramType, storyInterpreter, TimelineID.REALITY, verbalizerState);
                    adjunct.setParent(this);
                    //setSpeaker(adjunct, speaker_type, speaker_name);
                    //if (directSpeech) {adjunct.setTense("present");}
                    this.setModifier(adjunct);
                }
            }
        }
    }

    /**
     * Inserts 'and' between adverbial modifiers and prepositional groups with same prep
     * Ex: moved with dignity and with unconcern
     */
    public void addCoord(BuildDSyntS dsynts){
        if (this.children.size() != 0) {
            SchLexSynt prev = this.children.get(0);
            for (int i = 1; i < this.children.size(); i++){
                SchLexSynt child = this.children.get(i);
                if (prev.getClass() == child.getClass()){
                    if (prev instanceof SchFunc
                            && ((SchFunc) prev).getLexeme().equals(((SchFunc) child).getLexeme())){
                        SchFunc conj = new SchFunc("and", child.getChildren());
                        prev.setChild(conj);
                        this.children.set(i-1, prev);
                        this.children.remove(i);
                        i=i-1;
                    }
                    else if (prev instanceof SchAdverb){
                        ArrayList<SchLexSynt> tmp = new ArrayList<SchLexSynt>();
                        ((SchAdverb) child).setCoord();
                        tmp.add(child);
                        SchFunc conj = new SchFunc("and", tmp);
                        prev.setChild(conj);
                        this.children.set(i-1, prev);
                        this.children.remove(i);
                        i=i-1;
                    }
                    else if (prev instanceof SchVerb){
                        System.out.println("Coordination between verbs; potential for text plan");
                        this.children.remove(i); // can't handle it in RealPro
                        dsynts.addDSynt(prev);
                    }
                }
                prev = child;
            }
        }
    }


    public void copyVerb(SchVerb toCopy) {
        assignedAction = toCopy.assignedAction;
        validAction = toCopy.validAction;
        validCondition = toCopy.validCondition;
        supertype = toCopy.supertype;
        action = toCopy.action;
        truthDegree = toCopy.truthDegree;
        mode = toCopy.mode;
        isPredicate = toCopy.isPredicate;
        index = toCopy.index;
        inf =  toCopy.inf;
        children = toCopy.children;
        interpretationArcs = toCopy.interpretationArcs;
        tense = toCopy.tense;
        mood = toCopy.mood;
        isQuestion = toCopy.isQuestion;
        isSpeech = toCopy.isSpeech;
        isExtrapolated = toCopy.isExtrapolated;
        isAble = toCopy.isAble;
        lexeme = toCopy.lexeme;
        speakerName = toCopy.speakerName;
        speakerType = toCopy.speakerType;
        time = toCopy.time;
        conditionality = toCopy.conditionality;
        modifier = toCopy.modifier;
        emotions = toCopy.emotions;
    }

    /**
     * Deep SchVerb copy
     * @return
     */
    public SchVerb createCopy() {
        SchVerb newVerb = null;
        try {
            newVerb = new SchVerb("*<verb1494310_place_something_places_something_awayFromnearontoward_something>*"); // placehoder
            newVerb.assignedAction = this.assignedAction;
            newVerb.validAction = this.validAction;
            newVerb.validCondition = this.validCondition;
            newVerb.supertype = this.supertype;
            newVerb.action = this.action;
            newVerb.truthDegree = this.truthDegree;
            newVerb.mode = this.mode;
            newVerb.isPredicate = this.isPredicate;
            newVerb.index = this.index;
            newVerb.inf = this.inf;
            for (SchLexSynt child : this.children) {
                //newVerb.children.add(child.createCopy());
                newVerb.setChild(child.createCopy());
            }
            newVerb.parent = this.parent;
            newVerb.interpretationArcs = this.interpretationArcs;
            newVerb.tense = this.tense;
            newVerb.mood = this.mood;
            newVerb.isQuestion = this.isQuestion;
            newVerb.isSpeech = this.isSpeech;
            newVerb.isExtrapolated = this.isExtrapolated;
            newVerb.isAble = this.isAble;
            newVerb.lexeme = this.lexeme;
            newVerb.speakerName = this.speakerName;
            newVerb.speakerType = this.speakerType;
            newVerb.time = this.time;
            newVerb.conditionality = this.conditionality;
            newVerb.modifier = this.modifier;
            newVerb.emotions = this.emotions;
        } catch (Scheherexception e) {System.out.println(e);}
        return newVerb;
    }
    /******************************************************************************************************************/

    /**
     * Extracts the lexical file name of this verb
     * (e.g. verb of communication, stative, emotion...)
     */
    public String getLexFileName() {
        String lexFileName;
        Dictionary dictionary = Dictionary.getInstance();

        long offset = getOffset();
        //+ failed to parse offset
        if (offset < 0){return "";}

        if (action.toString().contains("#adj")) {
            lexFileName = action.toString().replaceAll("[\\<\\>\\[\\]]", "");
            String[] schLexeme = lexFileName.split("#");
            return schLexeme[0];
        }

        //+ LexFileName corresponds to grouping of annotated words into semantic categories
        else {
            try{
                lexFileName = dictionary.getSynsetAt(POS.VERB, offset).getLexFileName();
            }catch (JWNLException e) {
                System.err.println("Error thrown when quering wordnet: "+e.getMessage());
                return "";
            }
            return lexFileName;
        }
    }


    /**
     * Determines if this verb is a verb of communication in word net
     * @return
     */
    public boolean isVerbOfSpeech(){
        return getLexFileName().startsWith("verb.communication") || getLexFileName().startsWith("verb.cognition");
    }

    public void setSourceText(String text) {sourceText = text;}
    public String getSourceText() {return sourceText;}


    // <dsyntnode class=verb lexeme=word emotion_character=joy emotion_character2=despair
    public void setEmotions(ArrayList<String> e) { emotions = e;}




    /**
     *
     */
    public void getVerbNet() {
        try { // initialize VerbNet
            URL url = new URL("file", null, "jverbnet\\verbnet-3.2");
            // construct the index and open it
            IVerbIndex index = new VerbIndex(url);
            index.open();

            // look up a verb class and print out some info
            IVerbClass verb = index.getRootVerb("hit-18.1");
            IMember member = verb.getMembers().get(0) ;
            Set< IWordnetKey > keys = member.getWordnetTypes().keySet();
            IFrame frame = verb.getFrames().get(0);
            FrameType type = frame.getPrimaryType();
            String example = frame.getExamples().get(0) ;
            //System.out.println("id: " + verb.getID());
            //System.out.println(" first wordnet keys : " + keys);
            //System.out.println(" first frame type : " + type.getID());
            //System.out.println(" first example : " + example);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     *
     */
    public void removeSubj(){
        for (int i=0; i<children.size(); i++){
            if (children.get(i) instanceof SchNoun && ((SchNoun) children.get(i)).index == 0){
                children.remove(i);
                return;
            }
        }
    }

    public boolean hasObj() {
        for (int i=0; i<children.size(); i++){
            if (children.get(i) instanceof SchNoun && ((SchNoun) children.get(i)).index != 0){
                return true;
            }
        }
        return false;
    }

    public SchNoun getObj() {
        for (int i=0; i<children.size(); i++){
            if (children.get(i) instanceof SchNoun && ((SchNoun) children.get(i)).index != 0){
                return ((SchNoun)children.get(i));
            }
        }
        return null;
    }

    public SchNoun getSubj() {
        for (int i=0; i<children.size(); i++){
            if (children.get(i) instanceof SchNoun && ((SchNoun) children.get(i)).index == 0){
                return ((SchNoun)children.get(i));
            }
        }
        return null;
    }

    public boolean sameObjSubj(SchVerb v2) {
        SchNoun o1 = this.getObj();
        SchNoun s1 = v2.getSubj();

        if (o1 == null || s1 == null) {
            return false;
        }

        if (!(o1.name.equals("")) && !(s1.name.equals("")) && s1.name.equals(s1.name)) {
            return true;
        }

        // noun doesn't have a name
        if (o1.lexeme.equals(s1.lexeme)) {
            return true;
        }

        return false;
    }

    public boolean sameSubj(SchNoun s1, SchNoun s2) {
        if (s1 == null || s2 == null) {return false;}

        if (!(s1.name.equals("")) && !(s2.name.equals("")) && s2.name.equals(s1.name)) {
            return true; }

        // noun doesn't have a name
        if (s1.lexeme.equals(s2.lexeme)) {
            return true; }

        return false;
    }

    public boolean sameSubj(SchVerb v2) {
        SchNoun s1 = this.getSubj();
        SchNoun s2 = v2.getSubj();

        if (s1 == null || s2 == null) {return false;}

        if (!(s1.name.equals("")) && !(s2.name.equals("")) && s2.name.equals(s1.name)) {
            return true; }

        // noun doesn't have a name
        if (s1.lexeme.equals(s2.lexeme)) {
            return true; }

        return false;
    }

    public void removeChild(Object o) {
        getChildren().remove(o);
    }

    /**
     *
     */
    public void removeSubordinateVerb(){
        for (int i=0; i<children.size(); i++){
            if (children.get(i) instanceof SchVerb){
                children.remove(i);
                return;
            }
        }
    }

    /**
     * need to create SchLexSynt elements for modifiers and append them to children
     * @param modifier
     */
	void setModifier(SchLexSynt modifier) {
		//children.add(modifier);
        setChild(modifier);
	}

    void setQuestion() {isQuestion = true;}

    /**
     * set verb to infinitive
     * @param type
     */
	public void setToInf(InfType type){
		this.inf = type;
        this.tense = "inf-to";
        this.mood = "inf-to";
	}

    public void setLexeme(String lex) {
        lexeme = lex;
    }

    public InfType getToInf() {return this.inf;}

    /**
     *
     * @param cond
     */
    void setConditionality(Conditionality cond) {
        this.conditionality = cond;
    }

    /**
     * special feature to make 'in order for X to do smth' work
     */
	public void setExtrapolated(){
		isExtrapolated = true;
	}

    /**
     * set the noun or nouns to the verb
     * @param action
     */
    // TODO: REMVOE THIS
	void setChildren(ValidPredicate action) {
        // TODO: what's happening here? For some reason we weren't looking at the children of these verbs, when they actually have some
        if (mode != null && mode.equals(PredicateMode.ActionImperative)) {
            this.index = 2;
            //return;
        }
		//try {
			//ArrayList<SchLexSynt> arguments = getNouns(action);
			//children.addAll(arguments);
		//} catch (Scheherexception e) {
		//e.printStackTrace();
	}


    /**
     * By default, initialized to past
     * Operates recursively
     * Special case: if the child of a verb is another verb in rel=II, set to inf
     * E.g. I will try for me to obtain the cheese. me (rel=I), obtain (rel=II)
     * Need to remove "for me". It's automatically placed if obtain has a child ref subj.
     * Remove me (rel=I)
     * @param t
     */
    public void setTense(String t) {
        if (!tense.equals("")) {return;} // this tense has already been set

        tense = t;
        // recurse on children
        for (SchLexSynt child: this.children) {
            if (child instanceof SchVerb) {
                if (((SchVerb)child).getRelation().equals("II") && !((SchVerb)child).getMood().equals("gerund")) {
                    ((SchVerb) child).setToInf(InfType.ForTo);
                }
                ((SchVerb) child).setTense(t);
            }
            else if (child instanceof SchAttr || child instanceof SchFunc) {
                for (SchLexSynt grandchild: child.getChildren()) {
                    if (grandchild instanceof SchVerb) {
                        ((SchVerb) grandchild).setTense(t);
                    }
                }
            }
        }
    }

    /**
     * Sets the elements this node governs
     * @param child
     */
    public void setChild(SchLexSynt child){
        child.setParent(this);
        this.children.add(child);
    }

    public void setRel(int rel) {index = rel;}
    /**
     * Removes any children of this node
     */
    public void removeChildren(){this.children = new ArrayList<SchLexSynt>(); }

    /**
     *
     */
    public void setPredicate(){
        this.isPredicate = true;
        this.index = 0;
    }

    public void setSpeech(String s, String t) {
        speakerName = s;
        speakerType = t;
        isSpeech = true;
    }

    public int getIndex() {
        return index;
    }

    /**
     * need to parse the action name into lexeme and frame info
     * Need to separate multi-word actions, e.g. get away
     * Also handle things like weather, e.g. it stormed
     * @return
     */
	public String getLexeme() {
        if (lexeme != null) {return lexeme;}

		if (action instanceof ActionType){
            // hack for *<conditionEnds>*
            if (conditionEnds) {
                truthDegree = -5;
                return "be";
                // TODO: it's not always became, sometimes it's "started to be" i.e. "is"
                // for now, set back to is
            }
            else if (conditionBegins) {
                return "be";
            }

			String[] schLexeme = action.toString().split("_");
            String comlexAnswer = "";
            String suffix = "";
            String infinitive = "";
            if (schLexeme.length>1) {infinitive = schLexeme[1];}
            String[] suffixes = {"out", "away", "up", "down", "on", "around", "by", "through", "off", "place", "step", "fry", "a chance", "back", "about", "over", "out", "in", "apart", "shape", "it"};
            for (int i = 0; i < suffixes.length; i++) {
                if (infinitive.endsWith(suffixes[i])) {
                    for (int j = 0; j < schLexeme.length; j++) { // make sure it's a full word in the ID
                       if (suffixes[i].equals(schLexeme[j].replace(">*", ""))) {
                           suffix = suffixes[i];
                           infinitive = infinitive.substring(0, infinitive.length() - (suffixes[i].length()));

                           if (infinitive.equals("")) {break;} // hack for "place"

                           // add suffix as a child
                           try {
                               boolean exists = false; // need to check if we've already done it
                               for (SchLexSynt child : children) {
                                   if (child instanceof SchAttr && ((SchAttr) child).lexeme == suffix) {
                                       exists = true;
                                   }
                               }
                               if (!exists) {
                                   SchAttr part = new SchAttr("participle", suffix, "II");
                                   //part.setParent(this);
                                   //children.add(part);
                                   setChild(part);
                               }
                           }
                           catch (Scheherexception e) {System.out.println(e);}
                           return infinitive;
                       }
                        // special case
                       if (schLexeme[1].equals("goback")) {return "go";}

                    }
                }
            }

            if (schLexeme.length <= 1) {
                System.err.println("#### Unexpected ActionType: " + action.toString());
                return "";
            }
            return schLexeme[1];
		}

		else if(action instanceof ConditionType){
			String name = action.toString();
			name = name.replaceAll("[\\<\\>\\[\\]]", "");

			String[] schLexeme = name.split("(?=\\p{Upper})");
			String result = schLexeme[0];
			if (result.contains("adj")){
                if (validCondition != null && validCondition.getMode() == PredicateMode.ConditionImperative) {setToInf(InfType.ForTo);}
                return "be";}

            else if (result.contains("desire")) {
                return "desire";
            }
            // TODO: is possible
            else if (name.contains("timelinePossible")) {
                return "possible";
            }

            if (conditionEnds) {
                truthDegree = -5;
                return "be"; // TODO: it's not always became, sometimes it's "started to be" i.e. "is"
            }
            else if (conditionBegins) {return "be";} // TODO: it's not always became, sometimes it's "started to be" i.e. "is"
            else if (result.contains("able")) {return "able";}

            // check for preposition phrases "on/around/over/etc. something" is being picked up as a verb, but it's actually "is"
            if (name.equals("on/around/over/etc. something")) {return "be";}
            if (name.equals("nounsEqual")) {return "equal";}
            if (name.equals("charIsChar") || name.equals("propIsProp")) {return "be";}
            if (name.equals("charIsCharType") || name.equals("propIsPropType")) {return "be";}
			else if (result.startsWith("belief")){return result.replace("belief", "believe");}
			else if (result.startsWith("need")){return "need";}

			return result;
		}
		return "";
	}

    /**
     *
     * @return
     */
	public long getOffset(){
        if (offset == -1) {return -1;}
        if (action.toString().contains("adj")) {return -1;}
		Pattern p = Pattern.compile("(\\d+)");
        if (action == null) { return -1;}
		Matcher m = p.matcher(action.toString());
		if(m.find()){
			return Long.parseLong(m.group(0), 10);
		}
		else {
			System.err.println("#### Couldn't extract offset for "+ action.toString());
			return -1;
		}
	}

    /**
     * Conditional, "would be"
     * @return
     */
	public String getMood() {
        if (!mood.equals("")) {return mood;}
        else
            // TODO: HACK FOR GERUND
            if (lexeme != null) return "inf";
            if (mood.equals("inf-to")) return "inf-to";
            else {
                return (inf.equals(InfType.ForTo) ? "inf-to" : (inf.equals(InfType.WithoutTo) ? "inf" : "ind"));
            }
	}

    public void setMood(String m) {
        mood = m;
        tense = "past";
        mode = PredicateMode.ActionImperative;
    }

    public void setMode(String m) {
        mode = PredicateMode.ConditionAssert;
    }


    // Imperative to do something
    // TODO: imperative
    public String getMode() {
        if (mode.equals(PredicateMode.ActionInfinitive)) { // For John to like Mary
            return "inf-to";}
        else if (mood.contains("cond"))
            return "cond";
        else if (mode.equals(PredicateMode.ActionImperative) && !mood.contains("inf")) { // Call Mary
            return "imp";}
        else if (mood.contains("inf")) {
            return "inf-to";}
        return "";
    }

    /**
     *
     * @return
     */
    public String getTense()  {
        /**
        // this presupposes anything else;
        // we previous set this verb to be an infinitive, so it needs to keep the inf tense
        // "The narrator planned for her to make the mead"
        // this is dealing with the second verb. We need to remove the "for her" by removing the subject
        for (SchLexSynt child: this.children) {
            if (child instanceof SchVerb) {
                if (((SchVerb) child).getRelation().equals("II") && !((SchVerb) child).getMood().equals("gerund")) {
                    ((SchVerb) child).setToInf(InfType.ForTo);
                    // VERB INFINITIVE
                    // if the second subject is the same as the first and there is no object, remove the subject
                    if (sameSubj(this.getSubj(), ((SchVerb) child).getSubj()) && ((SchVerb) child).hasObj()) {
                        ((SchVerb) child).removeSubj();
                        // set index = 2
                        //((SchVerb) child).getSubj().index = 2;
                    }
                }
            }
        }
         **/

        if (getMood().equals("inf-to")) {return "inf-to";} // to lexeme
        if (getMood().equals("gerund")) {return "inf";} // lexeme (no to, no conjugation)
        if (mode != null && mode.equals(PredicateMode.ActionInfinitive)) {return "inf-to";} // infinitive, to lexeme

        // if the tense has been defined somewhere else
        else if (!tense.equals("")) {return tense;}

        else {if (getMood().equals("ind")) { return "past";}}
        return "past"; // default

    }

    /**
     *
     * @return
     */
    public String getRelation() {
        if (this.index == 0) {return "I";}
        else if (this.index == 1) {return "II";}
        else if (this.index == 2) {return "III";}
        else return "IV";
    }

    /**
     *
     * @return
     */
    public ArrayList<SchLexSynt> getChildren(){
        return children;
    }

    public void setParent(SchLexSynt p) {parent = p;}
    public SchLexSynt getParent() {return parent;}

    public void setTruthDegree(int t) {truthDegree = t;}
    public boolean isPredicate() {return isPredicate;}
    public ValidAction getValidAction() {return validAction;}
    public ValidCondition getValidCondition() {return validCondition;}


    /**********************************************************************/

    /**
     *
     * @return
     */
	public Map<String,String> getFeatures(){
		HashMap<String, String> features = new HashMap<String, String>();
		features.put("class", "verb");
		features.put("lexeme", getLexeme());
		features.put("mood", getMood());
        features.put("tense", getTense());
        features.put("wn_offset", String.valueOf(getOffset()));
        if (isQuestion) {features.put("question", "+");}
        if (mode != null) {features.put("mode", getMode());}
        features.put("rel", getRelation());

        if (emotions != null) {
            for (String emotion: emotions) {
                String character = emotion.substring(0, emotion.indexOf(':'));
                String emot = emotion.substring(emotion.indexOf(':')+1, emotion.length());
                features.put("emotion_"+character, emot);
            }
        }

		if (truthDegree < 0){features.put("polarity", "neg");}
        if (isExtrapolated && noSubject){features.put("extrapo", "i");}
        else if (isExtrapolated){features.put("extrapo", "+");}

		return features;
	}

    /**********************************************************************/
	

}
