import generator.aggregation.elements.Relation;

/*
All text plans have a voice attribute. By default, it is set to Formal. If we are actually realizing a direct speech utterance,
   We want to realize the nucleus in Formal and the satelite in whatever character voice we assign
*/
public class TextPlan {
    private String nucleus;
    private String satelite;
    private String relation;
    private String voice = "";

    boolean isDummy = false;



    /**
     *
     * @param prop1: I think this is actually an int refering to the index of the dsynt in the dsynts_list
     * @param prop2
     * @param rel
     */
    public TextPlan(String prop1, String prop2, String rel){
        nucleus = prop1;
        satelite = prop2;
        relation = rel;
        voice = "Narrator"; // corespond to NARRATOR ("Narrator", Personalities.getDefaultParameters()) in Personalities.java
    }
    public TextPlan(String prop1, String prop2, SchFunc unit){
        relation = unit.getRST();
        voice = unit.getVoice();
        voice = "Narrator"; // default
        if (relation.equals(Relation.DIRECT_SPEECH))
        {
            nucleus = prop2;
            satelite = prop1;
        }
        else
        {
            nucleus = prop1;
            satelite = prop2;
        }
    }
    // creates a dummy textplan
    public TextPlan(){
        isDummy = true;
    }

    // set the voice attribute to the character name
    // will handle the Personalities in Personage code
    public void setVoice(String charVoice) { voice = charVoice; }

    public String getNucleus() {
        return nucleus;
    }
    public String getSatelite() {
        return satelite;
    }
    public String getRelation() {
        return relation;
    }
    public String getVoice() {
        return voice;
    }

    public boolean isDummy(){
        return isDummy;
    }

}