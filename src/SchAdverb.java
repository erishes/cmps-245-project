import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import story.dbcore.exceptions.Scheherexception;
import story.scheherazade.elements.AssignedModifier;
import story.scheherazade.misc.NounName;
import story.scheherazade.verbalizer.Verbalizer;
import story.scheherazade.verbalizer.VerbalizerState;
import story.scheherazade.verbalizer.ModifierGrammaticalType;
import story.scheherazade.elements.ValidAction;

/**
 * Lexico-syntactic class for Adverb
 */
public class SchAdverb implements SchLexSynt{
    /** */
    String modifierInfo;

    /** */
	boolean sentBeg, coord;

    /** */
	ArrayList<SchLexSynt> children;
	SchLexSynt parent;

    /**
     *
     * @param mod
     * @throws Scheherexception
     */
	public SchAdverb(AssignedModifier mod) throws Scheherexception {
		modifierInfo = mod.getModifier().getType().toRawString();
		children = new ArrayList<SchLexSynt>();
	}

    public SchAdverb(String mod) {
        modifierInfo = mod;
        children = new ArrayList<SchLexSynt>();
    }

    public SchAdverb createCopy() {
        SchAdverb newAdverb = new SchAdverb(this.modifierInfo);
        newAdverb.sentBeg = this.sentBeg;
        newAdverb.coord = this.coord;
        for (SchLexSynt child : this.children) {
            //newAdverb.children.add(child.createCopy());
			newAdverb.setChild(child.createCopy());
        }
        return newAdverb;
    }

    /**
     *
     */
	public void setCoord(){
		coord = true;
	}

    /**
     *
     */
	public void setSentBeg(){
		sentBeg = true;
	}

    /**
     *
     * @return
     */
	public String getRelation(){
		// when 2 adverbs are coordinated by 'and', second should have 'II' relation
		return (coord ? "II" : "ATTR");
	}

    /**
     *
     * @return
     */
	public Map<String,String> getFeatures(){
		HashMap<String, String> features = new HashMap<String, String>();
		features.put("class", "adverb");
		features.put("lexeme", modifierInfo);
        features.put("position", "pre-verbal");
		features.put("rel", getRelation());
		if (sentBeg){
			features.put("starting_point", "+");
		}
		return features;
	}

    /**
     * Returns the elements this node governs
     * @return
     */
	public ArrayList<SchLexSynt> getChildren(){
		return children;
	}

    /**
     * Sets the elements this node governs
     * @param child
     */
	public void setChild(SchLexSynt child){
		this.children.add(child);
		child.setParent(this);
	}

	public void setParent(SchLexSynt p) {parent = p;}
	public SchLexSynt getParent() {return parent;}

    /**
     * Removes any children of this node
     */
    public void removeChildren(){this.children = new ArrayList<SchLexSynt>(); }

    /**
     *
     * @return
     */
	public Object getLexeme() {
		return modifierInfo;
	}

}
