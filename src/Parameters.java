/**
 * Created by Steph on 11/9/2015.
 */
public class Parameters {

    public static int narr_pov; // 1 or 3
    public static String narr_foc; // most likely to be narrator
    public static int narr_directSpeech;
    public static int sent_deagg;

    public Parameters() {
        narr_pov = 3;
        narr_foc = "";
        narr_directSpeech = 0;
        sent_deagg = 0;
    }

    public void setNarr_directSpeech(int ds) {narr_directSpeech = ds;}
    public double getNarr_directSpeech() {return narr_directSpeech;}

    public static void setSent_deagg(int deagg) {sent_deagg = deagg;}
    public double getSent_deagg() {return sent_deagg;}

    public static void setNarr_pov(int p) {narr_pov = p;}
    public int getNarr_pov() {return narr_pov;}

    public static void setNarr_foc(String f) {narr_foc = f;}
    public String getNarr_foc() {return narr_foc;}

    public String toString() {
        String name = "";

        if (narr_pov != 0) {name += "pov" + narr_pov;}
        if (narr_foc.length() > 0 && narr_pov != 3) {name += "_" + narr_foc;}
        if (narr_directSpeech != 0) {name += "_ds" + narr_directSpeech;}
        if (sent_deagg != 0) {name += "_deagg" + sent_deagg;}
        if (name.length() > 0) {return "_" + name;}
        else {return name;}
    }



    /**
     * 0: pov1-narrator
     * 1: pov1-narrator_deagg50
     * 2: pov1-narrator_deagg100
     * 3: pov1-narrator_ds50
     * 4: pov1-narrator_ds100
     * 5: pov1-narrator_ds50_deagg50
     * 6: pov1-narrator_ds100_deagg100
     * 7: pov1-narrator_ds100_deagg50
     * 8: pov1-narrator_ds50_deagg100
     * 9: pov3
     * 10: pov3_deagg50
     * 11: pov3_deagg100
     * 12: pov3_ds50
     * 13: pov3_ds100
     * 14: pov3_ds50_deagg50
     * 15: pov3_ds100_deagg100
     * 16: pov3_ds100_deagg50
     * 17: pov3_ds50_deagg100
     * @param i
     */
    public void setCombination(int i) {
        switch (i) {
            case 0:
                narr_pov = 1;
                narr_directSpeech = 0;
                sent_deagg = 0;
                break;
            case 1:
                narr_pov = 1;
                narr_directSpeech = 0;
                sent_deagg = 50;
                break;
            case 2:
                narr_pov = 1;
                narr_directSpeech = 0;
                sent_deagg = 100;
                break;
            case 3:
                narr_pov = 1;
                narr_directSpeech = 50;
                sent_deagg = 0;
                break;
            case 4:
                narr_pov = 1;
                narr_directSpeech = 100;
                sent_deagg = 0;
                break;
            case 5:
                narr_pov = 1;
                narr_directSpeech = 50;
                sent_deagg = 50;
                break;
            case 6:
                narr_pov = 1;
                narr_directSpeech = 100;
                sent_deagg = 100;
                break;
            case 7:
                narr_pov = 1;
                narr_directSpeech = 100;
                sent_deagg = 50;
                break;
            case 8:
                narr_pov = 1;
                narr_directSpeech = 50;
                sent_deagg = 100;
                break;
            case 9:
                narr_pov = 3;
                narr_directSpeech = 0;
                sent_deagg = 0;
                break;
            case 10:
                narr_pov = 3;
                narr_directSpeech = 0;
                sent_deagg = 50;
                break;
            case 11:
                narr_pov = 3;
                narr_directSpeech = 0;
                sent_deagg = 100;
                break;
            case 12:
                narr_pov = 3;
                narr_directSpeech = 50;
                sent_deagg = 0;
                break;
            case 13:
                narr_pov = 3;
                narr_directSpeech = 100;
                sent_deagg = 0;
                break;
            case 14:
                narr_pov = 3;
                narr_directSpeech = 50;
                sent_deagg = 50;
                break;
            case 15:
                narr_pov = 3;
                narr_directSpeech = 100;
                sent_deagg = 100;
                break;
            case 16:
                narr_pov = 3;
                narr_directSpeech = 100;
                sent_deagg = 50;
                break;
            case 17:
                narr_pov = 3;
                narr_directSpeech = 50;
                sent_deagg = 100;
                break;
        }
    }
}
