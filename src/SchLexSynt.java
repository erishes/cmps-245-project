import java.util.*;

/**
 * A generic Lexico-Syntactic unit
 * has a method to extract its DSynts features
 * has a reference to the elements it governs
 */
public interface SchLexSynt {
    /**
     *
     * @return
     */
	public Map<String,String> getFeatures();

    /**
     *
     * @return
     */
	public ArrayList<SchLexSynt> getChildren();

    /**
     *
     * @param child
     */
	public void setChild(SchLexSynt child);

    public void setParent(SchLexSynt parent);
    public SchLexSynt getParent();

    /**
     *
     */
    public void removeChildren();

    /**
     * Creates a deep copy of this node
     * @return
     */
    public SchLexSynt createCopy();
}
