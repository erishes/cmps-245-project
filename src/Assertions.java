import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Steph on 11/9/2015.
 */
public class Assertions {

    ArrayList<Pair<String, String>> finalOutput;
    String assertionFile, prologFile;

    public Assertions(String aFile, String pFile) {
        assertionFile = aFile;
        prologFile = pFile;
        finalOutput = new ArrayList<Pair<String, String>>();
        finalOutput = getPrologRules();
    }

    public String strip(String str) {
        if (str.charAt(str.length()-1) == ' ') {str = str.substring(0, str.length()-1);}
        return str.replace("\'", "").replace("\"", "").replace(",","").replace("\\", "");
    }

    public String trimFirstChar(String str) {
        return str.substring(1, str.length());
    }

    public ArrayList<Pair<String, String>> getAssertions() {return finalOutput;}

    public ArrayList<String> find(String sourceText) {
        ArrayList<String> emotions = new ArrayList<String>();
        if (finalOutput == null) {return emotions;}

        for (Pair<String, String> pair: finalOutput) {
            if (strip(pair.getKey()).equals(strip(sourceText)) && !emotions.contains(pair.getValue())) {
                emotions.add(pair.getValue());
            }
        }
        return emotions;
    }

    private ArrayList<Pair<String, String>> getPrologRules() {
        // timeline, emotion
        HashMap<String, String> propositions = new HashMap<String, String>();
        ArrayList<Pair<String, String>> emotions = new ArrayList<Pair<String, String>>();


        // PROPOSITIONS AND SOURCE TEXT
        String line = "";
        try {
            File file = new File(assertionFile);
            if (!file.exists()) {
                System.out.println("Assertions file " + file + " doesn't exist");
                return null;
            }

            // sourceText(timelineProposition1, 'A Lion watched a fat Bull').
            Scanner fileScanner = new Scanner(file);
            while (fileScanner.hasNext()) {
                line = fileScanner.nextLine();
                if (!line.contains("sourceText(")) {continue;}

                String[] elements = line.split(",");
                String timeline = elements[0].substring("sourceText(".length());
                String source = "";
                // some source texts include a comma
                //if (elements.length > 2) { // Believe me, you would do much better without them.
                //    source = elements[1].substring(" ".length());
                //} else {
                //    source = elements[1].substring(" ".length(), elements[1].indexOf(")"));}
                for (int i = 1; i < elements.length; i++) {
                  source += elements[i]  ;
                }
                source = source.substring(" ".length(), source.indexOf(")"));

                // (timelinePropositionX, "text")
                propositions.put(timeline, strip(source));
            }
        } catch (IOException e) {
            System.out.println("Could not process assertions file");
        }


        /// EMOTIONS
        line = "";
        try {
            File file = new File(prologFile);
            if (!file.exists()) {
                System.out.println("Prolog file doesn't exist");
                return null;
            }

            // A108-TheWilyLion.vgl	\t joy \t [(timelineProposition1,goal36), (timelineProposition14,THE_LION:Core_Goal)]
            Scanner fileScanner = new Scanner(file);
            while (fileScanner.hasNext()) {
                line = fileScanner.nextLine();
                String[] elements = line.split("\\t");
                String emotion = elements[1];
                String[] pairs = elements[2].split("\\)");
                // [(timelineProposition1,goal36
                // , (timelineProposition14,THE_LION:Core_Goal
                // ]

                for (String pair: pairs) {
                    if (!pair.contains("Core_Goal")) {continue;}

                    String timeline = pair.substring(pair.indexOf("timelineProposition"), pair.lastIndexOf(","));
                    String agent = pair.substring(pair.lastIndexOf(",")+1, pair.indexOf(":"));

                    if (propositions.get(timeline) != null) {
                        finalOutput.add(new Pair(propositions.get(timeline), agent + ":" + emotion));
                    }
                }


            }
        } catch (IOException e) {
            System.out.println("Could not process assertions file");
        }

        return finalOutput;
    }
}
