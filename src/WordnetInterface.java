import edu.mit.jverbnet.data.*;
import edu.mit.jverbnet.index.IVerbIndex;
import edu.mit.jverbnet.index.VerbIndex;
import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.*;
import net.didion.jwnl.dictionary.Dictionary;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import edu.mit.jverbnet.data.*;
import edu.mit.jverbnet.index.IVerbIndex;
import edu.mit.jverbnet.index.VerbIndex;
import edu.mit.jverbnet.*;
import story.dbcore.exceptions.Scheherexception;

/**
 * Created by Steph on 8/4/2015.
 */
public class WordnetInterface {

    public static Dictionary dictionary;
    public static IVerbIndex index;

    /**********************************************/

    public static void accessVerbnet(WordnetKey key) throws Scheherexception{

        Set<IMember> members = index.getMembers(key);
        if (members.isEmpty()) {
            System.out.println("No entry for " + key + " in VerbNet :(");
            return;
        }

        for(IMember member: members) {
            System.out.println(member.getName());
            IVerbClass verb = member.getVerbClass();
            IVerbClass parent = verb.getParent();

            if (parent != null) {
                System.out.println("Parent ID: " + parent.getID());
                System.out.println("# Thematic Roles: " + parent.getThematicRoles().size());
                for (IThematicRole role : parent.getThematicRoles()) {
                    System.out.println("Role type: " + role.getType());
                }
                System.out.println("# Frames: " + parent.getFrames().size());
                for (IFrame frame : parent.getFrames()) {
                    createFrame(frame, key.getLemma());
                }
            }

            System.out.println("ID: " + verb.getID());
            System.out.println("Subclass: " + verb.getSubclasses());
            System.out.println("# Thematic Roles: " + verb.getThematicRoles().size());
            for (IThematicRole role: verb.getThematicRoles()) {
                System.out.println("Role type: " + role.getType());
            }

            System.out.println("# Frames: " + verb.getFrames().size());
            for (IFrame frame: verb.getFrames()) {
                createFrame(frame, key.getLemma());
            }
        }
    }

    public static void createFrame(IFrame frame, String verbString) throws Scheherexception{
        BuildDSyntS dsynts = new BuildDSyntS(); // here for testing

        FrameType type = frame.getPrimaryType();
        String example = frame.getExamples().get(0);
        System.out.println("frame type: " + type.getID());
        System.out.println("example: " + example);

        String[] fight_params = {"heroes", "dragon", "cave"};
        String[] fight_types = {"NP", "V", "NP", "NP.location"};
        String[] params = fight_params;

        String[] selectedNouns = {};
        ArrayList<String> wordNetKeysNoun;
        for (String noun: params) {
            // TODO:
            if (noun.equals("heroes")) noun = "hero";
            long[] nounOffsets = getWordnetNounOffsets(noun);
            for (int i = 0; i < nounOffsets.length; i++) {
                if (nounOffsets[i] == 0) break;
                wordNetKeysNoun = getWordnetKeyNoun(nounOffsets[i]);
                // then iterate
                Iterator<String> iterator = wordNetKeysNoun.iterator();
                while (iterator.hasNext()) {
                    String wnKey = iterator.next();
                    System.out.println(wnKey);
                }
            }
        }
    }

    /**********************************************/

    // If we don't have the word offset, only the word
    public static long[] getWordnetNounOffsets(String noun) {
        long[] offsets = new long[10];
        try {
            IndexWord word = dictionary.getIndexWord(POS.NOUN, noun);
            //System.out.println(word.getLemma());

            // get synsets
            long[] nounSynset = word.getSynsetOffsets();
            for (int j = 0; j < nounSynset.length; j++) {
                offsets[j] = nounSynset[j];
            }
        }
        catch (JWNLException e) {
            System.err.println("Error thrown when quering wordnet: "+e.getMessage());
        }
        return offsets;
    }

    // If we have the wordnet offset
    public static ArrayList<String> getWordnetKeyNoun(long offset) {
        ArrayList<String> keys = new ArrayList<String>();
        try{
            // get the wordnet syset for the word given offset and POS
            Synset nounSynset = dictionary.getSynsetAt(POS.NOUN, offset);
            //System.out.println(nounSynset.getLexFileName());
            PointerTarget[] similarToTargets = nounSynset.getTargets();
            List<String> candidateSyns = new ArrayList<String>(nounSynset
                    .getWordsSize()
                    + similarToTargets.length);
            for (int j = 0; j < nounSynset.getWordsSize(); j++) {
                candidateSyns.add(nounSynset.getWords()[j].getLemma());
            }

            for (Word word: nounSynset.getWords()) {
                keys.add(word.getLemma());
            }
        }
        catch (JWNLException e) {
            System.err.println("Error thrown when quering wordnet: "+e.getMessage());
        }
        return keys;
    }



    /**********************************************/

    // If we don't have the offset, only the word
    // Returns an array list of wordnet offsets
    public static long[] getWordnetVerbOffsets(String verb) throws Exception {
        long[] offsets = new long[50];
        IndexWord word = dictionary.getIndexWord(POS.VERB, verb);
        //System.out.println(word.getLemma());
        // get synsets
        long[] nounSynset = word.getSynsetOffsets();
        for (int j = 0; j < nounSynset.length; j++) {
            offsets[j] = nounSynset[j];
        }
        return offsets;
    }

    // If we have the wordnet offset, we can create the wordnet key
    // Returns an array list of wordnet keys
    public static ArrayList<WordnetKey> getWordnetKeyVerb(long offset) throws Exception {
        ArrayList<WordnetKey> keys = new ArrayList<WordnetKey>();
        try{
            // get the wordnet syset for the word given offset and POS
            Synset verbSynset = dictionary.getSynsetAt(POS.VERB, offset);
            //System.out.println(verbSynset.getLexFileName());
            for (Word word: verbSynset.getWords())
            {
                //System.out.println(word.getLemma());
                long lex_filenum = verbSynset.getLexFileNum();
                long lex_id = word.getLexId();
                WordnetKey key = new WordnetKey(word.getLemma(), 2, (int)lex_filenum, (int)lex_id);
                keys.add(key);
            }
        }
        catch (JWNLException e) {
            System.err.println("Error thrown when quering wordnet: "+e.getMessage());
        }
        return keys;
    }








    public static void main(String[] args) throws Exception, IOException {
        try {
            JWNL.initialize(new FileInputStream("C:/Users/Stephanie/UCSC/jwnl-tests/jwnl/file_properties.xml"));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        String pathToVerbnet = "C:\\Users\\Stephanie\\UCSC\\verb\\verbnet\\";
        URL url = new URL("file", null, pathToVerbnet);

        // construct the index and open it
        index = new VerbIndex(url);
        index.open();

        // wordney key
        // lemma % lex_sense
        // where lex_sense is encoded as:
        // ss_type:lex_filenum:lex_id:head_word:head_id
        // lex_filenum = lex_filenum is a two digit decimal integer representing the name of the lexicographer file containing the synset for the sense.
        // lex_id = lex_id is a two digit decimal integer that, when appended onto lemma , uniquely identifies a sense within a lexicographer file.
        dictionary = Dictionary.getInstance();

        long verb_see_offset = 2129289; // see%2:39:00
        long noun_adult_offset = 9605289; //adult%1:18:00
        long verb_feel_offset = 1771535;
        long verb_show_offset = 664788;
        long verb_rescue_offset = 2556841; // rescue%2:41:00
        String verb_rescue = "rescue";

        getWordnetKeyVerb(2723951);

        /** NOUNS **/
        ArrayList<String> wordNetKeysNoun = null;

        // if we have the wn offset, first we get the wn keys
        wordNetKeysNoun = getWordnetKeyNoun(10231515);
        // then iterate
        Iterator<String> iterator = wordNetKeysNoun.iterator();
        while (iterator.hasNext()) {
            String wnKey = iterator.next();
            System.out.println(wnKey);
        }

        // if we don't have wn offsets, first we need wn offsets

        long[] nounOffsets = getWordnetNounOffsets("king");
        for (int i = 0; i < nounOffsets.length; i++) {
            if (nounOffsets[i] == 0) break;
            wordNetKeysNoun = getWordnetKeyNoun(nounOffsets[i]);
            // then iterate
            iterator = wordNetKeysNoun.iterator();
            while (iterator.hasNext()) {
                String wnKey = iterator.next();
                System.out.println(wnKey);
            }
        }


        /** VERBS **/
        ArrayList<WordnetKey> wordNetKeysVerb = null;

        // if we have the wn offset, first we get the wn keys
        wordNetKeysVerb = getWordnetKeyVerb(verb_feel_offset);
        // they iterate
        Iterator<WordnetKey> iteratorVerb = wordNetKeysVerb.iterator();
        while (iteratorVerb.hasNext()) {
            WordnetKey wnKey = iteratorVerb.next();
            accessVerbnet(wnKey);
        }

        // if we don't have wn offsets, first we need wn offsets
        long[] verbOffsets = getWordnetVerbOffsets("go");
        for (int i = 0; i < verbOffsets.length; i++) {
            if (verbOffsets[i] == 0) break;
            wordNetKeysVerb = getWordnetKeyVerb(verbOffsets[i]);
            // then iterate
            iteratorVerb = wordNetKeysVerb.iterator();
            while (iteratorVerb.hasNext()) {
                WordnetKey wnKey = iteratorVerb.next();
                accessVerbnet(wnKey);
            }
        }
    }
}
