#java -XX:PermSize=128M -Xmx1024m -Xms1024m -cp scheherazade-0.35.jar story.scheherazade.analysis.PrologTranslator assertions data/vgl/$1/$2.vgl > data/assertions/$1/assertions-$2.vgl.prolog
java -XX:PermSize=128M -Xmx1024m -Xms1024m -cp scheherazade-0.33.jar story.scheherazade.analysis.PrologTranslator data/vgl/$1/$2.vgl > data/assertions/$1/assertions-$2.vgl.prolog

