A lion began to love the daughter of a cottager and wanted to marry the daughter of the cottager.  

The cottager didn't allow the lion to marry the daughter of the cottager because the cottager feared the lion.  

The cottager didn't want to offend the lion and decided to compromise.  

The cottager approached the lion and said to him that -- if the lion were to allow the cottager to extract every tooth of the lion and were to allow him to pare every nail of the lion -- he would allow the lion to marry the daughter of the cottager.  

The lion said to the cottager that the lion allowed the cottager to extract every tooth of the lion and allowed him to pare every nail of the lion because the lion loved the daughter of the cottager.  

The cottager extracted every tooth of the lion and pared every nail of the lion.  

The cottager didn't fear the lion and expelled him.  

