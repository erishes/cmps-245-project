=========TOTALS ACROSS ALL FILES IN DEV SET=========
Average Levenshtein distance: 84.000000 
Average Set overlap: 0.380952 
=========RESULTS PER FILE=========
fox_n_grapes: 
	Levenshtein Distance : 84.000000
	Proportion of words that overlap : 0.38
	Words missing from the generated:
	['all', 'just', 'into', 'some', 'it', 'an', 'high', 'see', 'are', 'trained', 'hanging', 'fine', 'as', 'out', 'hungry', 'best', 'bunches', 'vain', 'gave', 'them', 'his', 'that', 'but', 'remarking', 'jumping', 'they', 'along', 'trying', 'by', 'those', 'he', 'a', 'i', 'could', 'up', 'air', 'so', 'were', 'quite']
	Extra words in the generated:
	['on', 'because', 'group', 'not', 'jumped', 'earlier', 'able', 'obtain', 'altruistically', 'hung', 'said', 'delusively', 'order']
	Levenshtein Trace :
[('ins', ['the', 'group', 'of']), ('sub', [('a', 'grapes'), ('hungry', 'hung'), ('fox', 'on'), ('saw', 'the'), ('some', 'vine'), ('fine', 'the'), ('bunches', 'vine'), ('of', 'hung'), ('grapes', 'on'), ('hanging', 'the'), ('from', 'trellis'), ('a', 'the'), ('vine', 'fox'), ('that', 'altruistically'), ('was', 'and'), ('trained', 'delusively'), ('along', 'saw'), ('a', 'the'), ('high', 'group'), ('trellis', 'of'), ('and', 'grapes'), ('did', 'the'), ('his', 'fox'), ('best', 'jumped'), ('to', 'in'), ('reach', 'order'), ('them', 'for'), ('by', 'the'), ('jumping', 'fox'), ('as', 'to'), ('high', 'obtain'), ('as', 'the'), ('he', 'group'), ('could', 'of'), ('into', 'grapes')]), ('match', ['the']), ('ins', ['fox', 'did']), ('sub', [('air', 'not'), ('but', 'obtain'), ('it', 'the'), ('was', 'group'), ('all', 'of'), ('in', 'grapes'), ('vain', 'because'), ('for', 'the'), ('they', 'fox'), ('were', 'was'), ('just', 'not'), ('out', 'able'), ('of', 'to')]), ('match', ['reach']), ('sub', [('so', 'the'), ('he', 'group'), ('gave', 'of'), ('up', 'grapes'), ('trying', 'the'), ('and', 'fox')]), ('match', ['walked', 'away']), ('sub', [('with', 'from'), ('an', 'the'), ('air', 'group')]), ('match', ['of']), ('ins', ['grapes', 'with']), ('match', ['dignity', 'and', 'unconcern']), ('ins', ['the', 'fox', 'said', 'the']), ('sub', [('remarking', 'fox'), ('i', 'earlier')]), ('match', ['thought']), ('ins', ['the', 'group']), ('sub', [('those', 'of')]), ('match', ['grapes']), ('sub', [('were', 'was')]), ('match', ['ripe']), ('ins', ['the', 'fox']), ('sub', [('but', 'said'), ('i', 'the'), ('see', 'fox')]), ('match', ['now']), ('ins', ['saw', 'the', 'group']), ('sub', [('they', 'of'), ('are', 'grapes'), ('quite', 'was')]), ('match', ['sour'])]

