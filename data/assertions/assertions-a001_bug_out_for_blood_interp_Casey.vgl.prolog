encoding('a001_bug_out_for_blood_interp_Casey.vgl').
type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
type(proposition21, type, proposition).
type(proposition22, type, proposition).
type(proposition23, type, proposition).
type(proposition24, type, proposition).
type(proposition25, type, proposition).
type(proposition26, type, proposition).
type(proposition27, type, proposition).
type(proposition28, type, proposition).
type(proposition29, type, proposition).
type(proposition30, type, proposition).
type(proposition31, type, proposition).
type(proposition32, type, proposition).
type(proposition33, type, proposition).
type(proposition34, type, proposition).
type(proposition35, type, proposition).
type(proposition36, type, proposition).
type(proposition37, type, proposition).
type(goal38, type, goal).
type(leisure39, type, leisure).
type(interpAction40, type, interpAction).
type(goal41, type, goal).
type(leisure42, type, leisure).
type(interpAction43, type, interpAction).
type(goal44, type, goal).
type(leisure45, type, leisure).
type(interpAction46, type, interpAction).
type(goal47, type, goal).
type(interpAction48, type, interpAction).
type(leisure49, type, leisure).
type(life50, type, life).
type(goal51, type, goal).
type(justice52, type, justice).
type(interpAction53, type, interpAction).
type(goal54, type, goal).
type(leisure55, type, leisure).
type(interpAction56, type, interpAction).
followedBy(proposition1, proposition4).
followedBy(proposition4, proposition5).
followedBy(proposition5, proposition7).
followedBy(proposition7, proposition8).
followedBy(proposition8, proposition10).
followedBy(proposition10, proposition11).
followedBy(proposition11, proposition13).
followedBy(proposition13, proposition14).
followedBy(proposition14, proposition15).
followedBy(proposition15, proposition16).
followedBy(proposition16, proposition17).
followedBy(proposition17, proposition18).
followedBy(proposition18, proposition20).
followedBy(proposition20, proposition23).
followedBy(proposition23, proposition24).
followedBy(proposition24, proposition25).
followedBy(proposition25, proposition27).
followedBy(proposition27, proposition28).
followedBy(proposition28, proposition30).
followedBy(proposition30, proposition32).
followedBy(proposition32, proposition36).
ceases(proposition8, interpAction46).
ceases(proposition16, interpAction48).
ceases(proposition24, interpAction48).
modifies(proposition2, proposition1).
modifies(proposition3, proposition1).
modifies(proposition6, proposition5).
modifies(proposition9, proposition8).
modifies(proposition12, proposition11).
modifies(proposition19, proposition18).
modifies(proposition21, proposition20).
modifies(proposition22, proposition20).
modifies(proposition26, proposition25).
modifies(proposition29, proposition28).
modifies(proposition31, proposition30).
modifies(proposition33, proposition32).
modifies(proposition34, proposition32).
modifies(proposition35, proposition32).
modifies(proposition37, proposition36).
agent(goal38, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(leisure39, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(goal41, '"anon_GROUP(CharacterGender.Neutral, noun2236355:bug(CharacterGender.Neutral), ())_1"').
agent(leisure42, '"anon_GROUP(CharacterGender.Neutral, noun2236355:bug(CharacterGender.Neutral), ())_1"').
agent(goal44, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(leisure45, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(goal47, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(leisure49, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(life50, '"anon_GROUP(CharacterGender.Neutral, noun2236355:bug(CharacterGender.Neutral), ())_1"').
agent(goal51, '"anon_character the role of something(CharacterGender.Neutral, noun10082146:fatso_2(CharacterGender.Neutral), anon_GROUP(CharacterGender.Neutral, noun2236355:bug(CharacterGender.Neutral), ())_1)_1"').
agent(justice52, '"anon_character the role of something(CharacterGender.Neutral, noun10082146:fatso_2(CharacterGender.Neutral), anon_GROUP(CharacterGender.Neutral, noun2236355:bug(CharacterGender.Neutral), ())_1)_1"').
agent(goal54, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
agent(leisure55, '"anon_noun10345804:narrator(CharacterGender.Neutral)_1"').
attemptToCause(proposition14, interpAction48).
attemptToCause(proposition28, interpAction56).
attemptToCause(proposition20, interpAction53).
attemptToCause(proposition15, interpAction48).
attemptToCause(proposition27, interpAction56).
attemptToCause(proposition17, interpAction48).
attemptToCause(proposition18, interpAction48).
attemptToCause(proposition11, interpAction48).
damages(interpAction48, life50).
interpretedAs(proposition7, interpAction46).
interpretedAs(proposition7, goal44).
interpretedAs(proposition1, interpAction40).
interpretedAs(proposition1, goal38).
interpretedAs(proposition13, goal47).
interpretedAs(proposition30, goal54).
interpretedAs(proposition18, goal47).
interpretedAs(proposition25, goal51).
interpretedAs(proposition4, interpAction43).
interpretedAs(proposition4, goal41).
interpNodeIn(leisure39, goal38).
interpNodeIn(interpAction40, goal38).
interpNodeIn(leisure42, goal41).
interpNodeIn(interpAction43, goal41).
interpNodeIn(leisure45, goal44).
interpNodeIn(interpAction46, goal44).
interpNodeIn(interpAction48, goal47).
interpNodeIn(leisure49, goal47).
interpNodeIn(life50, goal47).
interpNodeIn(justice52, goal51).
interpNodeIn(interpAction53, goal51).
interpNodeIn(leisure55, goal54).
interpNodeIn(interpAction56, goal54).
providesFor(interpAction48, leisure49).
providesFor(interpAction56, leisure55).
providesFor(interpAction43, leisure42).
providesFor(interpAction46, leisure45).
providesFor(interpAction40, leisure39).
providesFor(interpAction53, justice52).
verbalization(interpAction40, 'a narrator opens the door of the patio of the narrator').
verbalization(interpAction43, 'a group of bugs enters the apartment of a narrator').
verbalization(interpAction46, 'a narrator sleeps').
verbalization(interpAction48, 'a narrator kills a group of bugs').
verbalization(interpAction53, 'the big fatty of a group of bugs who was more large than every bug who wasn't the fatty of the group of bugs retaliates against a narrator').
verbalization(interpAction56, 'a narrator finds the big fatty of a group of bugs who was more large than every bug who wasn't the fatty of the group of bugs').
sourceText(proposition1, 'I left the patio door open').
sourceText(proposition2, 'the other night,').
sourceText(proposition3, 'just long enough').
sourceText(proposition4, 'to let in a dozen bugs of various size.').
sourceText(proposition5, 'I didn't notice them').
sourceText(proposition6, until).
sourceText(proposition7, 'until the middle of the night,').
sourceText(proposition8, 'until the middle of the night, when').
sourceText(proposition9, 'until the middle of the night,').
sourceText(proposition10, 'I saw them clinging to the ceiling.').
sourceText(proposition11, 'I grabbed the closest object within reach, and with a rolled-up comic book').
sourceText(proposition12, 'Since I'm such a bugaphobe,').
sourceText(proposition13, 'mine enemies').
sourceText(proposition14, 'and smeared their greasy bug guts.').
sourceText(proposition15, 'All except for the biggest one.').
sourceText(proposition16, 'I only clipped that one,').
sourceText(proposition17, 'taking off one of its limbs.').
sourceText(proposition18, 'I smote mine enemies').
sourceText(proposition19, 'and with a rolled-up comic book').
sourceText(proposition20, 'there's a five-limbed insect lurking').
sourceText(proposition21, 'in the apartment,').
sourceText(proposition22, 'So now').
sourceText(proposition23, 'a five-limbed insect').
sourceText(proposition24, 'So now there's a five-limbed insect lurking in the apartment,').
sourceText(proposition25, 'no doubt looking for some vengeance against me.').
sourceText(proposition26, 'no doubt').
sourceText(proposition27, 'I'm looking around corners,').
sourceText(proposition28, 'checking the toilet').
sourceText(proposition29, 'before sitting down,').
sourceText(proposition30, 'wondering when it's going to jump out.').
sourceText(proposition31, 'when it's going to jump out.').
sourceText(proposition32, 'and the cicadas haven't even arrived yet.').
sourceText(proposition33, even).
sourceText(proposition34, 'arrived yet.').
sourceText(proposition35, 'yet.').
sourceText(proposition36, 'All this,').

