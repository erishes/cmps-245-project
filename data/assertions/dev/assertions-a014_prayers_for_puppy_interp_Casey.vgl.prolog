encoding('a014_prayers_for_puppy_interp_Casey.vgl').
agent(timelineProposition1, 'the kidney of the dog').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the dog').
agent(timelineProposition10, 'the kidney of the dog').
agent(timelineProposition11, 'the dog').
agent(timelineProposition12, 'the dog').
agent(timelineProposition13, 'the dog').
agent(timelineProposition14, 'the dog').
agent(timelineProposition15, 'the dog').
agent(timelineProposition16, 'the dog').
agent(timelineProposition17, 'a family of the narrator').
agent(timelineProposition18, 'a family of the narrator').
agent(timelineProposition19, 'the owner of the dog').
agent(timelineProposition20, 'the owner of the dog').
agent(timelineProposition21, 'the narrator').
agent(goalBox22, 'the narrator').
agent(goal23, 'the narrator').
agent('THE_DOG:Core_Goal', 'the dog').
agent(goalBox24, 'the narrator').
agent(goal25, 'the narrator').
agent(goalBox26, 'the owner of the dog').
agent('THE_OWNER_OF_THE_DOG:Core_Goal', 'the owner of the dog').
agent(goal27, 'the owner of the dog').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition13, goal23).
attemptToPrevent(timelineProposition15, goal23).
attemptToPrevent(timelineProposition2, goal23).
attemptToPrevent(timelineProposition19, goal27).
attemptToPrevent(timelineProposition1, goal23).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(goalBox22, goalBox).
type(goal23, goal).
type('THE_DOG:Core_Goal', coreGoal).
type(goalBox24, goalBox).
type(goal25, goal).
type(goalBox26, goalBox).
type('THE_OWNER_OF_THE_DOG:Core_Goal', coreGoal).
type(goal27, goal).
sourceTextEndOffset(timelineProposition2, 118).
sourceTextEndOffset(timelineProposition3, 71).
sourceTextEndOffset(timelineProposition4, 157).
sourceTextEndOffset(timelineProposition5, 165).
sourceTextEndOffset(timelineProposition6, 218).
sourceTextEndOffset(timelineProposition7, 255).
sourceTextEndOffset(timelineProposition8, 302).
sourceTextEndOffset(timelineProposition9, 247).
sourceTextEndOffset(timelineProposition10, 340).
sourceTextEndOffset(timelineProposition11, 360).
sourceTextEndOffset(timelineProposition13, 367).
sourceTextEndOffset(timelineProposition14, 367).
sourceTextEndOffset(timelineProposition15, 378).
sourceTextEndOffset(timelineProposition16, 378).
sourceTextEndOffset(timelineProposition17, 398).
sourceTextEndOffset(timelineProposition18, 482).
sourceTextEndOffset(timelineProposition19, 631).
sourceTextEndOffset(timelineProposition20, 654).
sourceTextEndOffset(timelineProposition21, 831).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition7, goalBox24).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition4, goal23).
ceases(timelineProposition11, goal23).
ceases(timelineProposition10, goal23).
providesFor(dummy48, dummy49).
providesFor(goal25, 'THE_DOG:Core_Goal').
providesFor(goal23, 'THE_DOG:Core_Goal').
providesFor(goal27, 'THE_OWNER_OF_THE_DOG:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition2, 49).
sourceTextBeginOffset(timelineProposition3, 62).
sourceTextBeginOffset(timelineProposition4, 119).
sourceTextBeginOffset(timelineProposition5, 158).
sourceTextBeginOffset(timelineProposition6, 166).
sourceTextBeginOffset(timelineProposition7, 219).
sourceTextBeginOffset(timelineProposition8, 256).
sourceTextBeginOffset(timelineProposition9, 219).
sourceTextBeginOffset(timelineProposition10, 303).
sourceTextBeginOffset(timelineProposition11, 341).
sourceTextBeginOffset(timelineProposition13, 361).
sourceTextBeginOffset(timelineProposition14, 345).
sourceTextBeginOffset(timelineProposition15, 368).
sourceTextBeginOffset(timelineProposition16, 341).
sourceTextBeginOffset(timelineProposition17, 379).
sourceTextBeginOffset(timelineProposition18, 460).
sourceTextBeginOffset(timelineProposition19, 573).
sourceTextBeginOffset(timelineProposition20, 632).
sourceTextBeginOffset(timelineProposition21, 729).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal23, goalBox22).
interpNodeIn(goal25, goalBox24).
interpNodeIn(goal27, goalBox26).
sourceText(timelineProposition2, 'we found out on Friday that our 11 year old puppy has kidney failure.').
sourceText(timelineProposition3, 'on Friday').
sourceText(timelineProposition4, 'Too extensive to do anything about it.').
sourceText(timelineProposition5, 'Sudden,').
sourceText(timelineProposition6, 'in that it was not noticed at her vet visit in June.').
sourceText(timelineProposition7, 'She had a wonderful Saturday walking').
sourceText(timelineProposition8, 'her favorite (nearby home) letterboxing spots.').
sourceText(timelineProposition9, 'She had a wonderful Saturday').
sourceText(timelineProposition10, 'But, Monday am she began to crash and').
sourceText(timelineProposition11, 'she is now too weak').
sourceText(timelineProposition13, 'to eat').
sourceText(timelineProposition14, 'is now too weak to eat').
sourceText(timelineProposition15, 'or get up.').
sourceText(timelineProposition16, 'she is now too weak to eat or get up.').
sourceText(timelineProposition17, 'We are terribly sad').
sourceText(timelineProposition18, 'we are a bit in shock.').
sourceText(timelineProposition19, 'And, her 19 year old \'owner\' had to leave her this morning').
sourceText(timelineProposition20, 'to go back to college.').
sourceText(timelineProposition21, 'So, prayers for soon-relief for Daffnee, and peace and healing for the family who loves her very much.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition21, goal23).
attemptToCause(timelineProposition21, goal25).
attemptToCause(timelineProposition7, goal25).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition2).
followedBy(timelineProposition2, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition7).
followedBy(timelineProposition7, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition15).
followedBy(timelineProposition15, timelineProposition17).
followedBy(timelineProposition17, timelineProposition18).
followedBy(timelineProposition18, timelineProposition19).
followedBy(timelineProposition19, timelineProposition21).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition9, goal25).
interpretedAs(timelineProposition20, goalBox26).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition3, timelineProposition2).
modifies(timelineProposition6, timelineProposition5).
modifies(timelineProposition8, timelineProposition7).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition14, timelineProposition13).
modifies(timelineProposition16, timelineProposition15).
modifies(timelineProposition20, timelineProposition19).
text(timelineProposition14, 'is now too weak to eat').
text(goalBox26, 'The owner of the dog\'s goal for the owner of the dog to attend to a college.').
text(timelineProposition2, 'we found out on Friday that our 11 year old puppy has kidney failure.').
text(timelineProposition18, 'we are a bit in shock.').
text(goalBox24, 'The narrator\'s goal for the dog to be comfortable.').
text(timelineProposition4, 'Too extensive to do anything about it.').
text(goalBox22, 'The narrator\'s goal for the dog to be healthy.').
text(timelineProposition13, 'to eat').
text(timelineProposition3, 'on Friday').
text(timelineProposition17, 'We are terribly sad').
text(goal23, 'the dog is healthy').
text(timelineProposition5, 'Sudden,').
text(timelineProposition8, 'her favorite (nearby home) letterboxing spots.').
text(goal27, 'the owner of the dog attends to a college').
text(timelineProposition9, 'She had a wonderful Saturday').
text(timelineProposition10, 'But, Monday am she began to crash and').
text(timelineProposition20, 'to go back to college.').
text(timelineProposition6, 'in that it was not noticed at her vet visit in June.').
text(timelineProposition21, 'So, prayers for soon-relief for Daffnee, and peace and healing for the family who loves her very much.').
text(timelineProposition15, 'or get up.').
text(timelineProposition19, 'And, her 19 year old \'owner\' had to leave her this morning').
text(timelineProposition11, 'she is now too weak').
text(timelineProposition7, 'She had a wonderful Saturday walking').
text(goal25, 'the dog is comfortable').
text(timelineProposition16, 'she is now too weak to eat or get up.').

