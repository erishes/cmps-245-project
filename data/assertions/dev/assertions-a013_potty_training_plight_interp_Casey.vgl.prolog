encoding('a013_potty_training_plight_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the boy').
agent(timelineProposition3, 'the boy').
agent(timelineProposition4, 'the boy').
agent(timelineProposition5, 'the boy').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the boy').
agent(timelineProposition8, 'the boy').
agent(timelineProposition9, 'the boy').
agent(timelineProposition10, 'the boy').
agent(timelineProposition11, 'the boy').
agent(timelineProposition12, 'the boy').
agent(timelineProposition13, 'the boy').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the boy').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the boy').
agent(timelineProposition16, 'the boy').
agent(timelineProposition17, 'the narrator').
agent(goalBox18, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal19, 'the narrator').
agent(goalBox20, 'the narrator').
agent(goal21, 'the narrator').
agent(goalBox22, 'the boy').
agent('THE_BOY:Core_Goal', 'the boy').
agent(goal23, 'the boy').
agent(goalBox24, 'the boy').
agent(goal25, 'the boy').
agent('THE_BOY_AND_THE_NARRATOR:Core_Goal', 'the boy').
agent('THE_BOY_AND_THE_NARRATOR:Core_Goal', 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition15, goal19).
attemptToPrevent(timelineProposition3, goal19).
attemptToPrevent(timelineProposition16, goal19).
attemptToPrevent(timelineProposition2, goal19).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(goalBox18, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal19, goal).
type(goalBox20, goalBox).
type(goal21, goal).
type(goalBox22, goalBox).
type('THE_BOY:Core_Goal', coreGoal).
type(goal23, goal).
type(goalBox24, goalBox).
type(goal25, goal).
type('THE_BOY_AND_THE_NARRATOR:Core_Goal', coreGoal).
sourceTextEndOffset(timelineProposition2, 167).
sourceTextEndOffset(timelineProposition3, 205).
sourceTextEndOffset(timelineProposition4, 220).
sourceTextEndOffset(timelineProposition5, 271).
sourceTextEndOffset(timelineProposition6, 338).
sourceTextEndOffset(timelineProposition7, 368).
sourceTextEndOffset(timelineProposition8, 382).
sourceTextEndOffset(timelineProposition9, 418).
sourceTextEndOffset(timelineProposition10, 428).
sourceTextEndOffset(timelineProposition11, 488).
sourceTextEndOffset(timelineProposition12, 502).
sourceTextEndOffset(timelineProposition13, 631).
sourceTextEndOffset(timelineProposition14, 647).
sourceTextEndOffset(timelineProposition15, 711).
sourceTextEndOffset(timelineProposition16, 722).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition6, goalBox20).
implies(timelineProposition7, goalBox22).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition9, goal23).
ceases(timelineProposition9, goal21).
providesFor(dummy48, dummy49).
providesFor(goal21, 'THE_NARRATOR:Core_Goal').
providesFor(goal19, 'THE_NARRATOR:Core_Goal').
providesFor(goal23, 'THE_BOY:Core_Goal').
providesFor(goal25, 'THE_BOY_AND_THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition2, 117).
sourceTextBeginOffset(timelineProposition3, 168).
sourceTextBeginOffset(timelineProposition4, 206).
sourceTextBeginOffset(timelineProposition5, 221).
sourceTextBeginOffset(timelineProposition6, 313).
sourceTextBeginOffset(timelineProposition7, 339).
sourceTextBeginOffset(timelineProposition8, 369).
sourceTextBeginOffset(timelineProposition9, 392).
sourceTextBeginOffset(timelineProposition10, 419).
sourceTextBeginOffset(timelineProposition11, 472).
sourceTextBeginOffset(timelineProposition12, 489).
sourceTextBeginOffset(timelineProposition13, 616).
sourceTextBeginOffset(timelineProposition14, 632).
sourceTextBeginOffset(timelineProposition15, 699).
sourceTextBeginOffset(timelineProposition16, 712).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal19, goalBox18).
interpNodeIn(goal21, goalBox20).
interpNodeIn(goal23, goalBox22).
interpNodeIn(goal25, goalBox24).
sourceText(timelineProposition2, 'He still won\'t sit on the toilet and do his thing.').
sourceText(timelineProposition3, 'Though he wants me to dump his poopie').
sourceText(timelineProposition4, 'in the toilet,').
sourceText(timelineProposition5, 'and insists that he flushes the toilet on his own.').
sourceText(timelineProposition6, 'I was watching Nicktoons,').
sourceText(timelineProposition7, 'and the little boy was hiding').
sourceText(timelineProposition8, 'in the closet').
sourceText(timelineProposition9, 'when he bursted out crying').
sourceText(timelineProposition10, 'for help.').
sourceText(timelineProposition11, 'with clasp hands').
sourceText(timelineProposition12, 'on his chest,').
sourceText(timelineProposition13, 'So I dashed him').
sourceText(timelineProposition14, 'to the bathroom').
sourceText(timelineProposition15, 'He did it on').
sourceText(timelineProposition16, 'his nappy,').
inverseOf(dummy56, dummy57).
inverseOf(goal19, timelineProposition2).
inverseOf(timelineProposition2, goal19).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition6, goal21).
attemptToCause(timelineProposition11, goal25).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition2).
followedBy(timelineProposition2, timelineProposition3).
followedBy(timelineProposition3, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition9).
followedBy(timelineProposition9, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition15).
followedBy(timelineProposition15, timelineProposition17).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition10, goalBox24).
interpretedAs(timelineProposition1, goalBox18).
interpretedAs(timelineProposition7, goal23).
interpretedAs(timelineProposition13, goal25).
interpretedAs(timelineProposition17, goalBox18).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition4, timelineProposition3).
modifies(timelineProposition8, timelineProposition7).
modifies(timelineProposition10, timelineProposition9).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition14, timelineProposition13).
modifies(timelineProposition16, timelineProposition15).
text(timelineProposition10, 'for help.').
text(timelineProposition2, 'He still won\'t sit on the toilet and do his thing.').
text(goal21, 'the narrator relaxes').
text(timelineProposition12, 'on his chest,').
text(timelineProposition6, 'I was watching Nicktoons,').
text(timelineProposition13, 'So I dashed him').
text(goalBox20, 'The narrator\'s goal for the narrator to relax.').
text(timelineProposition7, 'and the little boy was hiding').
text(goalBox22, 'The boy\'s goal for the boy to play.').
text(timelineProposition8, 'in the closet').
text(timelineProposition3, 'Though he wants me to dump his poopie').
text(timelineProposition9, 'when he bursted out crying').
text(goalBox18, 'The narrator\'s goal for the boy to use a toilet.').
text(timelineProposition11, 'with clasp hands').
text(goal19, 'the boy uses a toilet').
text(timelineProposition15, 'He did it on').
text(timelineProposition5, 'and insists that he flushes the toilet on his own.').
text(timelineProposition16, 'his nappy,').
text(goal23, 'the boy plays').
text(timelineProposition4, 'in the toilet,').
text(goal25, 'the narrator helps the boy').
text(timelineProposition14, 'to the bathroom').
text(goalBox24, 'The boy\'s goal for the narrator to help the boy.').

