encoding('a026_dreamt_that_tears_turned_to_laughter_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the narrator').
agent(timelineProposition18, 'the narrator').
agent(goalBox19, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal20, 'the narrator').
agent(goalBox21, 'the narrator').
agent(goal22, 'the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(goalBox19, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal20, goal).
type(goalBox21, goalBox).
type(goal22, goal).
sourceTextEndOffset(timelineProposition1, 146).
sourceTextEndOffset(timelineProposition2, 172).
sourceTextEndOffset(timelineProposition3, 240).
sourceTextEndOffset(timelineProposition4, 220).
sourceTextEndOffset(timelineProposition5, 283).
sourceTextEndOffset(timelineProposition6, 338).
sourceTextEndOffset(timelineProposition7, 416).
sourceTextEndOffset(timelineProposition8, 427).
sourceTextEndOffset(timelineProposition9, 441).
sourceTextEndOffset(timelineProposition10, 469).
sourceTextEndOffset(timelineProposition11, 638).
sourceTextEndOffset(timelineProposition12, 624).
sourceTextEndOffset(timelineProposition13, 669).
sourceTextEndOffset(timelineProposition14, 681).
sourceTextEndOffset(timelineProposition15, 723).
sourceTextEndOffset(timelineProposition16, 737).
sourceTextEndOffset(timelineProposition17, 731).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition3, goalBox19).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition5, goal22).
providesFor(dummy48, dummy49).
providesFor(goal20, 'THE_NARRATOR:Core_Goal').
providesFor(goal22, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 133).
sourceTextBeginOffset(timelineProposition2, 139).
sourceTextBeginOffset(timelineProposition3, 221).
sourceTextBeginOffset(timelineProposition4, 173).
sourceTextBeginOffset(timelineProposition5, 270).
sourceTextBeginOffset(timelineProposition6, 302).
sourceTextBeginOffset(timelineProposition7, 355).
sourceTextBeginOffset(timelineProposition8, 417).
sourceTextBeginOffset(timelineProposition9, 428).
sourceTextBeginOffset(timelineProposition10, 442).
sourceTextBeginOffset(timelineProposition11, 600).
sourceTextBeginOffset(timelineProposition12, 610).
sourceTextBeginOffset(timelineProposition13, 641).
sourceTextBeginOffset(timelineProposition14, 670).
sourceTextBeginOffset(timelineProposition15, 708).
sourceTextBeginOffset(timelineProposition16, 732).
sourceTextBeginOffset(timelineProposition17, 726).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal20, goalBox19).
interpNodeIn(goal22, goalBox21).
sourceText(timelineProposition1, 'I was running').
sourceText(timelineProposition2, 'running through a field of grass,').
sourceText(timelineProposition3, 'I was running away.').
sourceText(timelineProposition4, 'carrying an armful of what I think were papers.').
sourceText(timelineProposition5, 'Grieved. Sad.').
sourceText(timelineProposition6, 'I tossed the papers all away from me').
sourceText(timelineProposition7, 'But as I ran like this, my crying turned into quiet laughter.').
sourceText(timelineProposition8, 'Then I ran').
sourceText(timelineProposition9, 'faster still,').
sourceText(timelineProposition10, 'and my laughing got louder.').
sourceText(timelineProposition11, 'I tumbled into the grass onto my back.').
sourceText(timelineProposition12, 'into the grass').
sourceText(timelineProposition13, 'stared up into the clear sky').
sourceText(timelineProposition14, 'and laughed').
sourceText(timelineProposition15, 'Then I breathed').
sourceText(timelineProposition16, happy).
sourceText(timelineProposition17, 'long,').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition11, goal22).
attemptToCause(timelineProposition12, goal22).
attemptToCause(timelineProposition8, goal20).
attemptToCause(timelineProposition9, goal20).
attemptToCause(timelineProposition6, goal22).
attemptToCause(timelineProposition2, goal20).
attemptToCause(timelineProposition1, goal20).
attemptToCause(timelineProposition13, goal22).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition15).
followedBy(timelineProposition15, timelineProposition18).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition15, goal22).
interpretedAs(timelineProposition14, goal22).
interpretedAs(timelineProposition10, goal22).
interpretedAs(timelineProposition17, goal22).
interpretedAs(timelineProposition16, goal22).
interpretedAs(timelineProposition7, goal22).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition3, timelineProposition1).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition16, timelineProposition15).
modifies(timelineProposition17, timelineProposition15).
text(timelineProposition4, 'carrying an armful of what I think were papers.').
text(goalBox19, 'The narrator\'s goal for the narrator to escape.').
text(goal22, 'the narrator is happy').
text(timelineProposition3, 'I was running away.').
text(timelineProposition14, 'and laughed').
text(goalBox21, 'The narrator\'s goal for the narrator to be happy.').
text(timelineProposition13, 'stared up into the clear sky').
text(timelineProposition6, 'I tossed the papers all away from me').
text(timelineProposition9, 'faster still,').
text(timelineProposition10, 'and my laughing got louder.').
text(timelineProposition12, 'into the grass').
text(timelineProposition2, 'running through a field of grass,').
text(timelineProposition16, happy).
text(timelineProposition15, 'Then I breathed').
text(goal20, 'the narrator escapes').
text(timelineProposition1, 'I was running').
text(timelineProposition8, 'Then I ran').
text(timelineProposition11, 'I tumbled into the grass onto my back.').
text(timelineProposition5, 'Grieved. Sad.').
text(timelineProposition17, 'long,').
text(timelineProposition7, 'But as I ran like this, my crying turned into quiet laughter.').

