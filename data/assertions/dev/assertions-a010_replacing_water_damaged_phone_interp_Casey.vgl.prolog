encoding('a010_replacing_water_damaged_phone_interp_Casey.vgl').
agent(timelineProposition1, 'the father of the narrator').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the father of the narrator').
agent(timelineProposition3, 'the father of the narrator').
agent(timelineProposition4, 'the father of the narrator').
agent(timelineProposition5, 'the first telephone').
agent(timelineProposition6, 'the father of the narrator').
agent(timelineProposition7, 'the father of the narrator').
agent(timelineProposition8, 'the father of the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the father of the narrator').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the phone company').
agent(timelineProposition11, 'the phone company').
agent(timelineProposition12, 'the phone company').
agent(timelineProposition13, 'the father of the narrator').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the father of the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the retailer').
agent(timelineProposition16, 'the father of the narrator').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the father of the narrator').
agent(timelineProposition17, 'the narrator').
agent(goalBox18, 'the father of the narrator').
agent(goalBox18, 'the narrator').
agent('THE_FATHER_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal', 'the father of the narrator').
agent('THE_FATHER_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal19, 'the father of the narrator').
agent(goal19, 'the narrator').
agent(goalBox20, 'the father of the narrator').
agent('THE_FATHER_OF_THE_NARRATOR:Core_Goal', 'the father of the narrator').
agent(goal21, 'the father of the narrator').
agent(goalBox22, 'the father of the narrator').
agent(goal23, 'the father of the narrator').
agent(goalBox24, 'the father of the narrator').
agent(goal25, 'the father of the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(goalBox18, goalBox).
type('THE_FATHER_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal', coreGoal).
type(goal19, goal).
type(goalBox20, goalBox).
type('THE_FATHER_OF_THE_NARRATOR:Core_Goal', coreGoal).
type(goal21, goal).
type(goalBox22, goalBox).
type(goal23, goal).
type(goalBox24, goalBox).
type(goal25, goal).
sourceTextEndOffset(timelineProposition1, 92).
sourceTextEndOffset(timelineProposition2, 130).
sourceTextEndOffset(timelineProposition3, 113).
sourceTextEndOffset(timelineProposition4, 140).
sourceTextEndOffset(timelineProposition6, 172).
sourceTextEndOffset(timelineProposition7, 204).
sourceTextEndOffset(timelineProposition8, 223).
sourceTextEndOffset(timelineProposition9, 254).
sourceTextEndOffset(timelineProposition10, 307).
sourceTextEndOffset(timelineProposition11, 322).
sourceTextEndOffset(timelineProposition12, 369).
sourceTextEndOffset(timelineProposition13, 521).
sourceTextEndOffset(timelineProposition14, 555).
sourceTextEndOffset(timelineProposition15, 586).
sourceTextEndOffset(timelineProposition16, 649).
sourceTextEndOffset(timelineProposition17, 725).
because(dummy38, dummy39).
implies(dummy40, dummy41).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition5, goal21).
ceases(timelineProposition5, goal19).
providesFor(dummy48, dummy49).
providesFor(goal23, 'THE_FATHER_OF_THE_NARRATOR:Core_Goal').
providesFor(goal19, 'THE_FATHER_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal').
providesFor(goal25, 'THE_FATHER_OF_THE_NARRATOR:Core_Goal').
providesFor(goal21, 'THE_FATHER_OF_THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 57).
sourceTextBeginOffset(timelineProposition2, 114).
sourceTextBeginOffset(timelineProposition3, 93).
sourceTextBeginOffset(timelineProposition4, 131).
sourceTextBeginOffset(timelineProposition6, 141).
sourceTextBeginOffset(timelineProposition7, 173).
sourceTextBeginOffset(timelineProposition8, 199).
sourceTextBeginOffset(timelineProposition9, 224).
sourceTextBeginOffset(timelineProposition10, 255).
sourceTextBeginOffset(timelineProposition11, 308).
sourceTextBeginOffset(timelineProposition12, 323).
sourceTextBeginOffset(timelineProposition13, 492).
sourceTextBeginOffset(timelineProposition14, 522).
sourceTextBeginOffset(timelineProposition15, 556).
sourceTextBeginOffset(timelineProposition16, 587).
sourceTextBeginOffset(timelineProposition17, 695).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal19, goalBox18).
interpNodeIn(goal21, goalBox20).
interpNodeIn(goal23, goalBox22).
interpNodeIn(goal25, goalBox24).
sourceText(timelineProposition1, 'me and my father bought iPhone 3G\'s').
sourceText(timelineProposition2, 'he went swimming').
sourceText(timelineProposition3, 'then two weeks later').
sourceText(timelineProposition4, 'with his.').
sourceText(timelineProposition6, 'So he wanted to get another one').
sourceText(timelineProposition7, 'but didnt want to pay the $400.').
sourceText(timelineProposition8, '$400. So we went to at&t').
sourceText(timelineProposition9, 'and we told them what happened').
sourceText(timelineProposition10, 'and they said with an iPhone you are always eligable').
sourceText(timelineProposition11, 'for an upgrade').
sourceText(timelineProposition12, 'and you can get a new iPhone at the same price').
sourceText(timelineProposition13, 'So we went to the apple store').
sourceText(timelineProposition14, 'and told them we had water damage').
sourceText(timelineProposition15, 'they said we need a new phone.').
sourceText(timelineProposition16, 'So we went like we were getting a phone and we got a new phone').
sourceText(timelineProposition17, 'Then sold the water damged one').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition10, goal23).
attemptToCause(timelineProposition13, goal23).
attemptToCause(timelineProposition8, goal23).
attemptToCause(timelineProposition12, goal23).
attemptToCause(timelineProposition17, goal25).
attemptToCause(timelineProposition14, goal23).
attemptToCause(timelineProposition15, goal23).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition2).
followedBy(timelineProposition2, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition15).
followedBy(timelineProposition15, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition7, goalBox24).
interpretedAs(timelineProposition16, goal23).
interpretedAs(timelineProposition1, goal19).
interpretedAs(timelineProposition1, goalBox18).
interpretedAs(timelineProposition6, goalBox22).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition3, timelineProposition2).
modifies(timelineProposition4, timelineProposition2).
modifies(timelineProposition11, timelineProposition10).
text(timelineProposition17, 'Then sold the water damged one').
text(timelineProposition6, 'So he wanted to get another one').
text(timelineProposition11, 'for an upgrade').
text(timelineProposition13, 'So we went to the apple store').
text(goalBox22, 'The father of the narrator\'s goal for the father of the narrator to obtain the second telephone.').
text(timelineProposition8, '$400. So we went to at&t').
text(timelineProposition12, 'and you can get a new iPhone at the same price').
text(timelineProposition1, 'me and my father bought iPhone 3G\'s').
text(timelineProposition3, 'then two weeks later').
text(timelineProposition7, 'but didnt want to pay the $400.').
text(timelineProposition14, 'and told them we had water damage').
text(goalBox18, 'The father of the narrator and the narrator goal for the father of the narrator and the narrator to obtain the first telephone and the telephone.').
text(timelineProposition2, 'he went swimming').
text(goal21, 'the first telephone is functional').
text(goal19, 'the father of the narrator and the narrator obtains the first telephone and the telephone').
text(timelineProposition9, 'and we told them what happened').
text(goalBox24, 'The father of the narrator\'s goal for the father of the narrator to not spend a money.').
text(goalBox20, 'The father of the narrator\'s goal for the first telephone to be functional.').
text(timelineProposition4, 'with his.').
text(goal25, 'the father of the narrator doesn\'t spend a money').
text(timelineProposition10, 'and they said with an iPhone you are always eligable').
text(timelineProposition16, 'So we went like we were getting a phone and we got a new phone').
text(goal23, 'the father of the narrator obtains the second telephone').
text(timelineProposition15, 'they said we need a new phone.').

