encoding('A109-TheFoxandTheCrow.vgl').
agent(timelineProposition1, 'the crow').
agent(timelineProposition2, 'the crow').
agent(timelineProposition3, 'the cheese').
agent(timelineProposition4, 'the fox').
agent(timelineProposition5, 'the fox').
agent(timelineProposition6, 'the fox').
agent(timelineProposition7, 'the fox').
agent(timelineProposition8, 'the fox').
agent(timelineProposition9, 'the fox').
agent(timelineProposition10, 'the fox').
agent(timelineProposition11, 'the fox').
agent(timelineProposition12, 'the fox').
agent(timelineProposition13, 'the fox').
agent(timelineProposition14, 'the crow').
agent(timelineProposition15, 'the crow').
agent(timelineProposition16, 'the crow').
agent(timelineProposition17, 'the crow').
agent(timelineProposition18, 'the cheese').
agent(timelineProposition19, 'the fox').
agent(timelineProposition20, 'the fox').
agent(timelineProposition21, 'the fox').
agent(hypothetical22, 'the fox').
agent(hypothetical23, 'the fox').
agent(goalBox24, 'the fox').
agent(goal25, 'the fox').
agent(goal26, 'the fox').
agent(goal27, 'the fox').
agent('THE_FOX:Core_Goal', 'the fox').
agent(goalBox28, 'the fox').
agent(goal29, 'the fox').
agent(goalBox30, 'the crow').
agent(beliefBox31, 'the fox').
agent(belief32, 'the fox').
agent('THE_CROW:Core_Goal', 'the crow').
agent(goal33, 'the crow').
agent(goal34, 'the fox').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition20, beliefBox31).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(hypothetical22, hypothetical).
type(hypothetical23, hypothetical).
type(goalBox24, goalBox).
type(goal25, goal).
type(goal26, goal).
type(goal27, goal).
type('THE_FOX:Core_Goal', coreGoal).
type(goalBox28, goalBox).
type(goal29, goal).
type(goalBox30, goalBox).
type(beliefBox31, beliefBox).
type(belief32, belief).
type('THE_CROW:Core_Goal', coreGoal).
type(goal33, goal).
type(goal34, goal).
sourceTextEndOffset(timelineProposition1, 18).
sourceTextEndOffset(timelineProposition2, 40).
sourceTextEndOffset(timelineProposition3, 75).
sourceTextEndOffset(timelineProposition4, 99).
sourceTextEndOffset(timelineProposition5, 168).
sourceTextEndOffset(timelineProposition6, 176).
sourceTextEndOffset(timelineProposition7, 189).
sourceTextEndOffset(timelineProposition8, 204).
sourceTextEndOffset(timelineProposition9, 217).
sourceTextEndOffset(timelineProposition10, 262).
sourceTextEndOffset(timelineProposition11, 291).
sourceTextEndOffset(timelineProposition12, 325).
sourceTextEndOffset(timelineProposition13, 429).
sourceTextEndOffset(timelineProposition14, 469).
sourceTextEndOffset(timelineProposition15, 535).
sourceTextEndOffset(timelineProposition16, 530).
sourceTextEndOffset(timelineProposition17, 514).
sourceTextEndOffset(timelineProposition18, 560).
sourceTextEndOffset(timelineProposition19, 598).
sourceTextEndOffset(timelineProposition20, 630).
sourceTextEndOffset(timelineProposition21, 661).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition20, hypothetical23).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
equivalentOf(goal33, goal29).
equivalentOf(goal29, goal33).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(belief32, 'THE_CROW:Core_Goal').
providesFor(beliefBox31, 'THE_CROW:Core_Goal').
providesFor(goal27, 'THE_FOX:Core_Goal').
wouldPrevent(dummy50, dummy51).
wouldPrevent(hypothetical23, beliefBox31).
damages(dummy52, dummy53).
damages(hypothetical22, 'THE_CROW:Core_Goal').
damages(goal27, 'THE_CROW:Core_Goal').
sourceTextBeginOffset(timelineProposition1, 0).
sourceTextBeginOffset(timelineProposition2, 19).
sourceTextBeginOffset(timelineProposition3, 41).
sourceTextBeginOffset(timelineProposition4, 76).
sourceTextBeginOffset(timelineProposition5, 100).
sourceTextBeginOffset(timelineProposition6, 170).
sourceTextBeginOffset(timelineProposition7, 177).
sourceTextBeginOffset(timelineProposition8, 190).
sourceTextBeginOffset(timelineProposition9, 205).
sourceTextBeginOffset(timelineProposition10, 218).
sourceTextBeginOffset(timelineProposition11, 263).
sourceTextBeginOffset(timelineProposition12, 292).
sourceTextBeginOffset(timelineProposition13, 326).
sourceTextBeginOffset(timelineProposition14, 431).
sourceTextBeginOffset(timelineProposition15, 515).
sourceTextBeginOffset(timelineProposition16, 526).
sourceTextBeginOffset(timelineProposition17, 479).
sourceTextBeginOffset(timelineProposition18, 537).
sourceTextBeginOffset(timelineProposition19, 573).
sourceTextBeginOffset(timelineProposition20, 599).
sourceTextBeginOffset(timelineProposition21, 638).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal25, goalBox24).
interpNodeIn(goal26, goalBox24).
interpNodeIn(goal27, goalBox24).
interpNodeIn(goal29, goalBox28).
interpNodeIn(belief32, beliefBox31).
interpNodeIn(beliefBox31, goalBox30).
interpNodeIn(goal33, goalBox30).
interpNodeIn(goalBox30, goalBox28).
interpNodeIn(goal34, goalBox28).
sourceText(timelineProposition1, 'A Crow was sitting').
sourceText(timelineProposition2, 'on a branch of a tree').
sourceText(timelineProposition3, 'with a piece of cheese in her beak').
sourceText(timelineProposition4, 'when a Fox observed her').
sourceText(timelineProposition5, 'and set his wits to work to discover some way of getting the cheese.').
sourceText(timelineProposition6, 'Coming').
sourceText(timelineProposition7, 'and standing').
sourceText(timelineProposition8, 'under the tree').
sourceText(timelineProposition9, 'he looked up').
sourceText(timelineProposition10, 'and said, "What a noble bird I see above me!').
sourceText(timelineProposition11, 'Her beauty is without equal,').
sourceText(timelineProposition12, 'the hue of her plumage exquisite.').
sourceText(timelineProposition13, 'If only her voice is as sweet as her looks are fair, she ought without doubt to be Queen of the Birds."').
sourceText(timelineProposition14, 'The Crow was hugely flattered by this,').
sourceText(timelineProposition15, 'she gave a loud caw.').
sourceText(timelineProposition16, loud).
sourceText(timelineProposition17, 'to show the Fox that she could sing').
sourceText(timelineProposition18, 'Down came the cheese,of').
sourceText(timelineProposition19, 'the Fox, snatching it up,').
sourceText(timelineProposition20, 'said, "You have a voice, madam,').
sourceText(timelineProposition21, 'what you want is wits."').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
actualizes(timelineProposition15, goal29).
actualizes(timelineProposition15, goal33).
actualizes(timelineProposition15, goal25).
actualizes(timelineProposition18, goal26).
actualizes(timelineProposition14, goal34).
actualizes(timelineProposition19, goal27).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition7, goal34).
attemptToCause(timelineProposition13, goal34).
attemptToCause(timelineProposition10, goal34).
attemptToCause(timelineProposition16, beliefBox31).
attemptToCause(timelineProposition6, goal34).
attemptToCause(timelineProposition11, goal34).
attemptToCause(timelineProposition12, goal34).
attemptToCause(timelineProposition8, goal34).
attemptToCause(timelineProposition9, goal34).
wouldCause(dummy62, dummy63).
wouldCause(goal33, belief32).
wouldCause(goal29, goal25).
wouldCause(goal25, goal26).
wouldCause(goalBox30, goal29).
wouldCause(goal34, goalBox30).
wouldCause(goal26, goal27).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition15).
followedBy(timelineProposition15, timelineProposition18).
followedBy(timelineProposition18, timelineProposition19).
followedBy(timelineProposition19, timelineProposition20).
followedBy(timelineProposition20, timelineProposition21).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition17, beliefBox31).
interpretedAs(timelineProposition5, goalBox24).
interpretedAs(timelineProposition21, hypothetical22).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition8, timelineProposition7).
modifies(timelineProposition16, timelineProposition15).
modifies(timelineProposition17, timelineProposition15).
text(timelineProposition6, 'Coming').
text(goal27, 'the fox obtains the cheese from the beak of the crow').
text(timelineProposition20, 'said, "You have a voice, madam,').
text(hypothetical23, 'the fox doesn\'t believe that the crow is able to sing').
text(timelineProposition14, 'The Crow was hugely flattered by this,').
text(timelineProposition3, 'with a piece of cheese in her beak').
text(goal34, 'the fox flatters the crow').
text(timelineProposition12, 'the hue of her plumage exquisite.').
text(timelineProposition15, 'she gave a loud caw.').
text(goalBox30, 'The crow\'s goal (which would cause the crow to sing) for the crow to sing,  which would cause the fox to have a belief that the crow is able to sing; for the fox\'s belief that she is able to sing.').
text(goalBox28, 'The fox\'s goal for the fox to flatter the crow,  which would cause the crow to have a goal (which would cause the crow to sing) for the crow to sing,  which would cause the fox to have a belief that the crow is able to sing; for the fox\'s belief that she is able to sing.').
text(timelineProposition5, 'and set his wits to work to discover some way of getting the cheese.').
text(goal26, 'the crow drops the cheese').
text(timelineProposition8, 'under the tree').
text(timelineProposition10, 'and said, "What a noble bird I see above me!').
text(goal25, 'the crow opens the beak of the crow').
text(timelineProposition19, 'the Fox, snatching it up,').
text(timelineProposition17, 'to show the Fox that she could sing').
text(timelineProposition4, 'when a Fox observed her').
text(goalBox24, 'The fox\'s goal for the crow to open the beak of the crow,  which would cause the crow to drop the cheese.').
text(timelineProposition11, 'Her beauty is without equal,').
text(timelineProposition9, 'he looked up').
text(hypothetical22, 'the fox insults the crow').
text(timelineProposition2, 'on a branch of a tree').
text(timelineProposition16, loud).
text(timelineProposition7, 'and standing').
text(beliefBox31, 'The fox\'s belief that the crow is able to sing.').
text(goal33, 'the crow sings').
text(belief32, 'the crow is able to sing').
text(goal29, 'the crow sings').
text(timelineProposition18, 'Down came the cheese,of').
text(timelineProposition21, 'what you want is wits."').
text(timelineProposition1, 'A Crow was sitting').
text(timelineProposition13, 'If only her voice is as sweet as her looks are fair, she ought without doubt to be Queen of the Birds."').

