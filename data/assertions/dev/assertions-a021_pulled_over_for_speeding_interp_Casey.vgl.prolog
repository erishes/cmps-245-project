encoding('a021_pulled_over_for_speeding_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the police officer').
agent(timelineProposition8, 'the police officer').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the police officer').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the police officer').
agent(timelineProposition14, 'the police officer').
agent(timelineProposition15, 'the police officer').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the father of the narrator').
agent(timelineProposition17, 'the mother of the narrator').
agent(timelineProposition18, 'the narrator').
agent(goalBox19, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal20, 'the narrator').
agent(goalBox21, 'the police officer').
agent('THE_POLICE_OFFICER:Core_Goal', 'the police officer').
agent(goal22, 'the police officer').
agent(goalBox23, 'the narrator').
agent(goal24, 'the narrator').
agent(goalBox25, 'the father of the narrator').
agent(goalBox25, 'the mother of the narrator').
agent(goal26, 'the father of the narrator').
agent(goal26, 'the mother of the narrator').
agent('THE_FATHER_OF_THE_NARRATOR_AND_THE_MOTHER_OF_THE_NARRATOR:Core_Goal', 'the father of the narrator').
agent('THE_FATHER_OF_THE_NARRATOR_AND_THE_MOTHER_OF_THE_NARRATOR:Core_Goal', 'the mother of the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition9, goal22).
attemptToPrevent(timelineProposition11, goal22).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(goalBox19, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal20, goal).
type(goalBox21, goalBox).
type('THE_POLICE_OFFICER:Core_Goal', coreGoal).
type(goal22, goal).
type(goalBox23, goalBox).
type(goal24, goal).
type(goalBox25, goalBox).
type(goal26, goal).
type('THE_FATHER_OF_THE_NARRATOR_AND_THE_MOTHER_OF_THE_NARRATOR:Core_Goal', coreGoal).
sourceTextEndOffset(timelineProposition1, 70).
sourceTextEndOffset(timelineProposition2, 85).
sourceTextEndOffset(timelineProposition3, 121).
sourceTextEndOffset(timelineProposition4, 216).
sourceTextEndOffset(timelineProposition6, 177).
sourceTextEndOffset(timelineProposition7, 17).
sourceTextEndOffset(timelineProposition8, 359).
sourceTextEndOffset(timelineProposition9, 383).
sourceTextEndOffset(timelineProposition10, 418).
sourceTextEndOffset(timelineProposition12, 455).
sourceTextEndOffset(timelineProposition13, 478).
sourceTextEndOffset(timelineProposition14, 524).
sourceTextEndOffset(timelineProposition16, 694).
sourceTextEndOffset(timelineProposition17, 769).
sourceTextEndOffset(timelineProposition18, 809).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition17, goalBox25).
implies(timelineProposition7, goalBox21).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition15, goal22).
ceases(timelineProposition14, goal22).
providesFor(dummy48, dummy49).
providesFor(goal26, 'THE_FATHER_OF_THE_NARRATOR_AND_THE_MOTHER_OF_THE_NARRATOR:Core_Goal').
providesFor(goal22, 'THE_POLICE_OFFICER:Core_Goal').
providesFor(goal24, 'THE_NARRATOR:Core_Goal').
providesFor(goal20, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
damages(goal22, 'THE_NARRATOR:Core_Goal').
sourceTextBeginOffset(timelineProposition1, 50).
sourceTextBeginOffset(timelineProposition2, 71).
sourceTextBeginOffset(timelineProposition3, 92).
sourceTextBeginOffset(timelineProposition4, 207).
sourceTextBeginOffset(timelineProposition6, 128).
sourceTextBeginOffset(timelineProposition7, 0).
sourceTextBeginOffset(timelineProposition8, 325).
sourceTextBeginOffset(timelineProposition9, 360).
sourceTextBeginOffset(timelineProposition10, 384).
sourceTextBeginOffset(timelineProposition12, 410).
sourceTextBeginOffset(timelineProposition13, 456).
sourceTextBeginOffset(timelineProposition14, 493).
sourceTextBeginOffset(timelineProposition16, 655).
sourceTextBeginOffset(timelineProposition17, 695).
sourceTextBeginOffset(timelineProposition18, 770).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal20, goalBox19).
interpNodeIn(goal22, goalBox21).
interpNodeIn(goal24, goalBox23).
interpNodeIn(goal26, goalBox25).
sourceText(timelineProposition1, 'morning... On my way').
sourceText(timelineProposition2, 'back to Owings').
sourceText(timelineProposition3, 'for my sorority retreat deal.').
sourceText(timelineProposition4, 'doing 85.').
sourceText(timelineProposition6, 'Totally didn\'t notice the cops like I usually do,').
sourceText(timelineProposition7, 'I got pulled over').
sourceText(timelineProposition8, 'so he asked me what my excuse was.').
sourceText(timelineProposition9, '\'I don\'t have one Sir.\'').
sourceText(timelineProposition10, '\'License and registration please.\'').
sourceText(timelineProposition12, 'please.\' I sit there, waiting for the ticket.').
sourceText(timelineProposition13, 'He hands me everything').
sourceText(timelineProposition14, 'Slow down. That\'s way too fast.').
sourceText(timelineProposition16, 'Told mom and dad I was doing 70 in a 55').
sourceText(timelineProposition17, 'they said if I get a ticket, I\'ll more than likely not have car insurance.').
sourceText(timelineProposition18, 'Gah. Either way, no more than 10 above.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition8, goal22).
attemptToCause(timelineProposition4, goal20).
attemptToCause(timelineProposition10, goal22).
attemptToCause(timelineProposition1, goal20).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition4).
followedBy(timelineProposition4, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition15).
followedBy(timelineProposition15, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
followedBy(timelineProposition17, timelineProposition18).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition18, goal26).
interpretedAs(timelineProposition3, goalBox19).
interpretedAs(timelineProposition16, goal24).
interpretedAs(timelineProposition16, goalBox23).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition3, timelineProposition1).
modifies(timelineProposition5, timelineProposition4).
text(timelineProposition18, 'Gah. Either way, no more than 10 above.').
text(goalBox21, 'The police officer\'s goal for the police officer to punish the narrator.').
text(timelineProposition2, 'back to Owings').
text(timelineProposition14, 'Slow down. That\'s way too fast.').
text(timelineProposition3, 'for my sorority retreat deal.').
text(goal22, 'the police officer punishes the narrator').
text(goal20, 'the narrator attends to the retreat').
text(timelineProposition8, 'so he asked me what my excuse was.').
text(goal24, 'the narrator confesses to the father of the narrator and the mother of the narrator').
text(timelineProposition17, 'they said if I get a ticket, I\'ll more than likely not have car insurance.').
text(goalBox19, 'The narrator\'s goal for the narrator to attend to the retreat.').
text(timelineProposition7, 'I got pulled over').
text(timelineProposition12, 'please.\' I sit there, waiting for the ticket.').
text(timelineProposition16, 'Told mom and dad I was doing 70 in a 55').
text(goal26, 'the narrator is law-abiding').
text(goalBox23, 'The narrator\'s goal for the narrator to confess to the father of the narrator and the mother of the narrator.').
text(timelineProposition6, 'Totally didn\'t notice the cops like I usually do,').
text(timelineProposition13, 'He hands me everything').
text(timelineProposition10, '\'License and registration please.\'').
text(timelineProposition4, 'doing 85.').
text(timelineProposition1, 'morning... On my way').
text(goalBox25, 'The father of the narrator and the mother of the narrator goal for the narrator to be law-abiding.').
text(timelineProposition9, '\'I don\'t have one Sir.\'').

