encoding('a009_car_accident_hug_interp_Casey.vgl').
agent(timelineProposition1, 'the employer').
agent(timelineProposition2, 'the employer').
agent(timelineProposition3, 'the employer').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the girl').
agent(timelineProposition9, 'the girl').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the girl').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the girl').
agent(timelineProposition18, 'the girl').
agent(timelineProposition19, 'the narrator').
agent(timelineProposition20, 'the narrator').
agent(timelineProposition23, 'the narrator').
agent(timelineProposition24, 'the narrator').
agent(goalBox25, 'the narrator').
agent(goal26, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goalBox27, 'the girl').
agent('THE_GIRL:Core_Goal', 'the girl').
agent(goal28, 'the girl').
agent(goalBox29, 'the narrator').
agent(goal30, 'the narrator').
agent(goalBox31, 'the narrator').
agent(goal32, 'the narrator').
agent(goalBox33, 'the narrator').
agent(goal34, 'the narrator').
agent(goalBox35, 'the girl').
agent(goal36, 'the girl').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(timelineProposition22, timelineProposition).
type(timelineProposition23, timelineProposition).
type(timelineProposition24, timelineProposition).
type(goalBox25, goalBox).
type(goal26, goal).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goalBox27, goalBox).
type('THE_GIRL:Core_Goal', coreGoal).
type(goal28, goal).
type(goalBox29, goalBox).
type(goal30, goal).
type(goalBox31, goalBox).
type(goal32, goal).
type(goalBox33, goalBox).
type(goal34, goal).
type(goalBox35, goalBox).
type(goal36, goal).
sourceTextEndOffset(timelineProposition1, 785).
sourceTextEndOffset(timelineProposition2, 810).
sourceTextEndOffset(timelineProposition3, 845).
sourceTextEndOffset(timelineProposition4, 110).
sourceTextEndOffset(timelineProposition5, 133).
sourceTextEndOffset(timelineProposition7, 258).
sourceTextEndOffset(timelineProposition8, 222).
sourceTextEndOffset(timelineProposition10, 299).
sourceTextEndOffset(timelineProposition11, 315).
sourceTextEndOffset(timelineProposition12, 335).
sourceTextEndOffset(timelineProposition13, 353).
sourceTextEndOffset(timelineProposition14, 365).
sourceTextEndOffset(timelineProposition15, 424).
sourceTextEndOffset(timelineProposition16, 459).
sourceTextEndOffset(timelineProposition17, 519).
sourceTextEndOffset(timelineProposition18, 547).
sourceTextEndOffset(timelineProposition19, 589).
sourceTextEndOffset(timelineProposition20, 615).
sourceTextEndOffset(timelineProposition22, 742).
sourceTextEndOffset(timelineProposition23, 665).
sourceTextEndOffset(timelineProposition24, 714).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition8, goalBox27).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
equivalentOf(goal36, timelineProposition18).
equivalentOf(timelineProposition18, goal36).
ceases(dummy46, dummy47).
ceases(timelineProposition8, goal26).
ceases(timelineProposition8, goal28).
providesFor(dummy48, dummy49).
providesFor(goal36, 'THE_NARRATOR:Core_Goal').
providesFor(goal34, 'THE_GIRL:Core_Goal').
providesFor(goal34, 'THE_NARRATOR:Core_Goal').
providesFor(goal28, 'THE_GIRL:Core_Goal').
providesFor(goal32, 'THE_NARRATOR:Core_Goal').
providesFor(goal26, 'THE_NARRATOR:Core_Goal').
providesFor(goal30, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 776).
sourceTextBeginOffset(timelineProposition2, 786).
sourceTextBeginOffset(timelineProposition3, 811).
sourceTextBeginOffset(timelineProposition4, 97).
sourceTextBeginOffset(timelineProposition5, 111).
sourceTextBeginOffset(timelineProposition7, 228).
sourceTextBeginOffset(timelineProposition8, 196).
sourceTextBeginOffset(timelineProposition10, 279).
sourceTextBeginOffset(timelineProposition11, 295).
sourceTextBeginOffset(timelineProposition12, 316).
sourceTextBeginOffset(timelineProposition13, 336).
sourceTextBeginOffset(timelineProposition14, 354).
sourceTextBeginOffset(timelineProposition15, 384).
sourceTextBeginOffset(timelineProposition16, 425).
sourceTextBeginOffset(timelineProposition17, 481).
sourceTextBeginOffset(timelineProposition18, 533).
sourceTextBeginOffset(timelineProposition19, 573).
sourceTextBeginOffset(timelineProposition20, 590).
sourceTextBeginOffset(timelineProposition22, 715).
sourceTextBeginOffset(timelineProposition23, 634).
sourceTextBeginOffset(timelineProposition24, 666).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal26, goalBox25).
interpNodeIn(goal28, goalBox27).
interpNodeIn(goal30, goalBox29).
interpNodeIn(goal32, goalBox31).
interpNodeIn(goal34, goalBox33).
interpNodeIn(goal36, goalBox35).
sourceText(timelineProposition1, 'She works').
sourceText(timelineProposition2, 'at this \'the edge\' kiosk').
sourceText(timelineProposition3, 'in the trumbull and Milford malls.').
sourceText(timelineProposition4, 'I was heading').
sourceText(timelineProposition5, 'to Staci\'s B-day party').
sourceText(timelineProposition7, 'I\'m about to hit the stoplight').
sourceText(timelineProposition8, 'and this girl Gina hits me').
sourceText(timelineProposition10, 'I get out of my car,').
sourceText(timelineProposition11, 'car, she\'s spazzing.').
sourceText(timelineProposition12, 'I checked my bumper').
sourceText(timelineProposition13, 'where she hit me.').
sourceText(timelineProposition14, 'Scraped up.').
sourceText(timelineProposition15, 'I see her license plate frame broke off,').
sourceText(timelineProposition16, 'picked it up and handed it to her.').
sourceText(timelineProposition17, 'asking if we should trade information,').
sourceText(timelineProposition18, 'apologizing...').
sourceText(timelineProposition19, 'I gave her a hug').
sourceText(timelineProposition20, 'and told her it was okay.').
sourceText(timelineProposition22, 'The hug really did wonders.').
sourceText(timelineProposition23, 'It made me indescribably happy.').
sourceText(timelineProposition24, 'Not the accident but the easing of her spazzing.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition20, goal34).
attemptToCause(timelineProposition17, goal36).
attemptToCause(timelineProposition16, goal34).
attemptToCause(timelineProposition19, goal34).
attemptToCause(timelineProposition7, goal26).
attemptToCause(timelineProposition4, goal26).
attemptToCause(timelineProposition18, goal36).
attemptToCause(timelineProposition10, goal30).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition4).
followedBy(timelineProposition4, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition14).
followedBy(timelineProposition14, timelineProposition15).
followedBy(timelineProposition15, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
followedBy(timelineProposition17, timelineProposition18).
followedBy(timelineProposition18, timelineProposition19).
followedBy(timelineProposition19, timelineProposition20).
followedBy(timelineProposition20, timelineProposition21).
followedBy(timelineProposition21, timelineProposition23).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition5, goalBox25).
interpretedAs(timelineProposition21, goal34).
interpretedAs(timelineProposition12, goalBox29).
interpretedAs(timelineProposition16, goalBox33).
interpretedAs(timelineProposition14, goal30).
interpretedAs(timelineProposition22, goal34).
interpretedAs(timelineProposition15, goal32).
interpretedAs(timelineProposition15, goalBox31).
interpretedAs(timelineProposition18, goalBox35).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition3, timelineProposition1).
modifies(timelineProposition5, timelineProposition4).
modifies(timelineProposition6, timelineProposition4).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition13, timelineProposition12).
modifies(timelineProposition22, timelineProposition21).
modifies(timelineProposition24, timelineProposition23).
text(timelineProposition18, 'apologizing...').
text(timelineProposition19, 'I gave her a hug').
text(goal30, 'the narrator checks the second car for injury').
text(timelineProposition20, 'and told her it was okay.').
text(timelineProposition15, 'I see her license plate frame broke off,').
text(goalBox25, 'The narrator\'s goal for the narrator to visit the woman.').
text(timelineProposition16, 'picked it up and handed it to her.').
text(timelineProposition17, 'asking if we should trade information,').
text(goal36, 'the girl justifies').
text(timelineProposition24, 'Not the accident but the easing of her spazzing.').
text(timelineProposition1, 'She works').
text(goal28, 'the girl travels').
text(timelineProposition10, 'I get out of my car,').
text(goalBox27, 'The girl\'s goal for the girl to travel.').
text(goalBox35, 'The girl\'s goal for the girl to justify.').
text(goalBox31, 'The narrator\'s goal for the narrator to check the first car for injury.').
text(timelineProposition13, 'where she hit me.').
text(timelineProposition22, 'The hug really did wonders.').
text(timelineProposition2, 'at this \'the edge\' kiosk').
text(timelineProposition23, 'It made me indescribably happy.').
text(timelineProposition5, 'to Staci\'s B-day party').
text(goal26, 'the narrator visits the woman').
text(goal34, 'the narrator helps the girl').
text(timelineProposition8, 'and this girl Gina hits me').
text(goal32, 'the narrator checks the first car for injury').
text(timelineProposition12, 'I checked my bumper').
text(timelineProposition3, 'in the trumbull and Milford malls.').
text(timelineProposition11, 'car, she\'s spazzing.').
text(timelineProposition14, 'Scraped up.').
text(goalBox33, 'The narrator\'s goal for the narrator to help the girl.').
text(goalBox29, 'The narrator\'s goal for the narrator to check the second car for injury.').
text(timelineProposition4, 'I was heading').
text(timelineProposition7, 'I\'m about to hit the stoplight').

