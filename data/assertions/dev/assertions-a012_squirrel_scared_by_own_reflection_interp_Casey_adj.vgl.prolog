encoding('a012_squirrel_scared_by_own_reflection_interp_Casey_adj.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the bowl').
agent(timelineProposition5, 'the group of birds').
agent(timelineProposition6, 'the group of birds').
agent(timelineProposition7, 'the group of birds').
agent(timelineProposition8, 'the group of birds').
agent(timelineProposition9, 'the group of birds').
agent(timelineProposition10, 'the group of birds').
agent(timelineProposition11, 'the group of squirrels').
agent(timelineProposition12, 'the second squirrel').
agent(timelineProposition13, 'the second squirrel').
agent(timelineProposition14, 'the second squirrel').
agent(timelineProposition15, 'the second squirrel').
agent(timelineProposition16, 'the second squirrel').
agent(timelineProposition17, 'the second squirrel').
agent(timelineProposition18, 'the second squirrel').
agent(timelineProposition19, 'the second squirrel').
agent(timelineProposition20, 'the second squirrel').
agent(timelineProposition21, 'the second squirrel').
agent(timelineProposition22, 'the second squirrel').
agent(timelineProposition23, 'the second squirrel').
agent(timelineProposition24, 'the paw of the second squirrel').
agent(timelineProposition25, 'the paw of the second squirrel').
agent(timelineProposition26, 'the second squirrel').
agent(timelineProposition27, 'the second squirrel').
agent(goalBox28, 'the narrator').
agent('THE_DOG:Core_Goal', 'the dog').
agent(goal29, 'the narrator').
agent(goalBox30, 'the group of birds').
agent('THE_GROUP_OF_BIRDS:Core_Goal', 'the group of birds').
agent(goal31, 'the group of birds').
agent(goalBox32, 'the group of squirrels').
agent(goal33, 'the group of squirrels').
agent('THE_GROUP_OF_SQUIRRELS:Core_Goal', 'the group of squirrels').
agent(goalBox34, 'the second squirrel').
agent('THE_SQUIRREL:Core_Goal', 'the second squirrel').
agent(goal35, 'the second squirrel').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition14, goal35).
attemptToPrevent(timelineProposition18, goal35).
attemptToPrevent(timelineProposition16, goal35).
attemptToPrevent(timelineProposition21, goal35).
attemptToPrevent(timelineProposition24, goal35).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(timelineProposition22, timelineProposition).
type(timelineProposition23, timelineProposition).
type(timelineProposition24, timelineProposition).
type(timelineProposition25, timelineProposition).
type(timelineProposition26, timelineProposition).
type(timelineProposition27, timelineProposition).
type(goalBox28, goalBox).
type('THE_DOG:Core_Goal', coreGoal).
type(goal29, goal).
type(goalBox30, goalBox).
type('THE_GROUP_OF_BIRDS:Core_Goal', coreGoal).
type(goal31, goal).
type(goalBox32, goalBox).
type(goal33, goal).
type('THE_GROUP_OF_SQUIRRELS:Core_Goal', coreGoal).
type(goalBox34, goalBox).
type('THE_SQUIRREL:Core_Goal', coreGoal).
type(goal35, goal).
sourceTextEndOffset(timelineProposition1, 128).
sourceTextEndOffset(timelineProposition2, 157).
sourceTextEndOffset(timelineProposition4, 225).
sourceTextEndOffset(timelineProposition5, 272).
sourceTextEndOffset(timelineProposition6, 289).
sourceTextEndOffset(timelineProposition8, 317).
sourceTextEndOffset(timelineProposition9, 332).
sourceTextEndOffset(timelineProposition10, 353).
sourceTextEndOffset(timelineProposition11, 393).
sourceTextEndOffset(timelineProposition12, 429).
sourceTextEndOffset(timelineProposition14, 464).
sourceTextEndOffset(timelineProposition15, 505).
sourceTextEndOffset(timelineProposition16, 568).
sourceTextEndOffset(timelineProposition17, 555).
sourceTextEndOffset(timelineProposition18, 588).
sourceTextEndOffset(timelineProposition19, 602).
sourceTextEndOffset(timelineProposition21, 654).
sourceTextEndOffset(timelineProposition22, 654).
sourceTextEndOffset(timelineProposition24, 692).
sourceTextEndOffset(timelineProposition26, 724).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition12, goalBox34).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition26, goal35).
providesFor(dummy48, dummy49).
providesFor(goal31, 'THE_GROUP_OF_BIRDS:Core_Goal').
providesFor(goal35, 'THE_SQUIRREL:Core_Goal').
providesFor(goal33, 'THE_GROUP_OF_SQUIRRELS:Core_Goal').
providesFor(goal29, 'THE_DOG:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 58).
sourceTextBeginOffset(timelineProposition2, 129).
sourceTextBeginOffset(timelineProposition4, 185).
sourceTextBeginOffset(timelineProposition5, 226).
sourceTextBeginOffset(timelineProposition6, 273).
sourceTextBeginOffset(timelineProposition8, 290).
sourceTextBeginOffset(timelineProposition9, 318).
sourceTextBeginOffset(timelineProposition10, 333).
sourceTextBeginOffset(timelineProposition11, 354).
sourceTextBeginOffset(timelineProposition12, 394).
sourceTextBeginOffset(timelineProposition14, 430).
sourceTextBeginOffset(timelineProposition15, 465).
sourceTextBeginOffset(timelineProposition16, 519).
sourceTextBeginOffset(timelineProposition17, 519).
sourceTextBeginOffset(timelineProposition18, 580).
sourceTextBeginOffset(timelineProposition19, 589).
sourceTextBeginOffset(timelineProposition21, 603).
sourceTextBeginOffset(timelineProposition22, 620).
sourceTextBeginOffset(timelineProposition24, 655).
sourceTextBeginOffset(timelineProposition26, 693).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal29, goalBox28).
interpNodeIn(goal31, goalBox30).
interpNodeIn(goal33, goalBox32).
interpNodeIn(goal35, goalBox34).
sourceText(timelineProposition1, 'We keep a large stainless steel bowl of water outside on the back deck').
sourceText(timelineProposition2, 'for Benjamin to drink out of').
sourceText(timelineProposition4, 'His bowl has become a very popular site.').
sourceText(timelineProposition5, 'Throughout the day, many birds drink out of it').
sourceText(timelineProposition6, 'and bathe in it.').
sourceText(timelineProposition8, 'The birds literally line up').
sourceText(timelineProposition9, 'on the railing').
sourceText(timelineProposition10, 'and wait their turn.').
sourceText(timelineProposition11, 'Squirrels also come to drink out of it.').
sourceText(timelineProposition12, 'The craziest squirrel just came by-').
sourceText(timelineProposition14, 'he was literally jumping in fright').
sourceText(timelineProposition15, 'at what I believe was his own reflection').
sourceText(timelineProposition16, 'He was startled so much at one point that he leap').
sourceText(timelineProposition17, 'He was startled so much at one point').
sourceText(timelineProposition18, 'and fell').
sourceText(timelineProposition19, 'off the deck.').
sourceText(timelineProposition21, 'But not quite, I saw his one little paw hanging on!').
sourceText(timelineProposition22, 'saw his one little paw hanging on!').
sourceText(timelineProposition24, 'After a moment or two his paw slipped').
sourceText(timelineProposition26, 'and he tumbled down a few feet.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition1, goal29).
attemptToCause(timelineProposition12, goal35).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition8).
followedBy(timelineProposition8, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition14).
followedBy(timelineProposition14, timelineProposition16).
followedBy(timelineProposition16, timelineProposition18).
followedBy(timelineProposition18, timelineProposition21).
followedBy(timelineProposition21, timelineProposition24).
followedBy(timelineProposition24, timelineProposition26).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition2, goalBox28).
interpretedAs(timelineProposition5, goal31).
interpretedAs(timelineProposition5, goalBox30).
interpretedAs(timelineProposition11, goalBox32).
interpretedAs(timelineProposition11, goal33).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition3, timelineProposition1).
modifies(timelineProposition7, timelineProposition6).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition10, timelineProposition8).
modifies(timelineProposition13, timelineProposition12).
modifies(timelineProposition15, timelineProposition14).
modifies(timelineProposition17, timelineProposition16).
modifies(timelineProposition19, timelineProposition18).
modifies(timelineProposition20, timelineProposition18).
modifies(timelineProposition22, timelineProposition21).
modifies(timelineProposition23, timelineProposition21).
modifies(timelineProposition25, timelineProposition24).
modifies(timelineProposition27, timelineProposition26).
text(goalBox28, 'The narrator\'s goal for the narrator to provide a water to the dog.').
text(timelineProposition16, 'He was startled so much at one point that he leap').
text(goal29, 'the narrator provides a water to the dog').
text(timelineProposition6, 'and bathe in it.').
text(goal33, 'the group of squirrels drinks').
text(timelineProposition18, 'and fell').
text(timelineProposition24, 'After a moment or two his paw slipped').
text(goalBox32, 'The group of squirrels\'s goal for the group of squirrels to drink.').
text(timelineProposition21, 'But not quite, I saw his one little paw hanging on!').
text(timelineProposition9, 'on the railing').
text(timelineProposition17, 'He was startled so much at one point').
text(timelineProposition1, 'We keep a large stainless steel bowl of water outside on the back deck').
text(timelineProposition12, 'The craziest squirrel just came by-').
text(goal31, 'the group of birds drinks').
text(timelineProposition11, 'Squirrels also come to drink out of it.').
text(timelineProposition22, 'saw his one little paw hanging on!').
text(goalBox34, 'The second squirrel\'s goal for the second squirrel to drink.').
text(timelineProposition4, 'His bowl has become a very popular site.').
text(timelineProposition8, 'The birds literally line up').
text(timelineProposition5, 'Throughout the day, many birds drink out of it').
text(goal35, 'the second squirrel drinks').
text(timelineProposition19, 'off the deck.').
text(timelineProposition14, 'he was literally jumping in fright').
text(timelineProposition10, 'and wait their turn.').
text(timelineProposition15, 'at what I believe was his own reflection').
text(goalBox30, 'The group of birds\'s goal for the group of birds to drink.').
text(timelineProposition26, 'and he tumbled down a few feet.').
text(timelineProposition2, 'for Benjamin to drink out of').

