encoding('a015_fixing_car_with_crayola_marker_interp_Casey.vgl').
agent(timelineProposition1, 'the girl').
agent(timelineProposition2, 'the girl').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the girl').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the girl').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the girl').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the girl').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the girl').
agent(timelineProposition11, 'the girl').
agent(timelineProposition12, 'the girl').
agent(timelineProposition13, 'the girl').
agent(timelineProposition14, 'the girl').
agent(timelineProposition15, 'the girl').
agent(timelineProposition16, 'the girl').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the girl').
agent(timelineProposition17, 'the narrator').
agent(timelineProposition18, 'the girl').
agent(timelineProposition18, 'the narrator').
agent(goalBox19, 'the girl').
agent(goalBox19, 'the narrator').
agent(goal20, 'the girl').
agent(goal20, 'the narrator').
agent(goal21, 'the girl').
agent(goal21, 'the narrator').
agent('THE_GIRL_AND_THE_NARRATOR:Core_Goal', 'the girl').
agent('THE_GIRL_AND_THE_NARRATOR:Core_Goal', 'the narrator').
agent(goalBox22, 'the narrator').
agent(goal23, 'the narrator').
agent('THE_GIRL:Core_Goal', 'the girl').
agent(goalBox24, 'the girl').
agent(goal25, 'the girl').
agent(goalBox26, 'the girl').
agent(goal27, 'the girl').
agent(goalBox28, 'the girl').
agent(goalBox28, 'the narrator').
agent(goal29, 'the girl').
agent(goal29, 'the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(goalBox19, goalBox).
type(goal20, goal).
type(goal21, goal).
type('THE_GIRL_AND_THE_NARRATOR:Core_Goal', coreGoal).
type(goalBox22, goalBox).
type(goal23, goal).
type('THE_GIRL:Core_Goal', coreGoal).
type(goalBox24, goalBox).
type(goal25, goal).
type(goalBox26, goalBox).
type(goal27, goal).
type(goalBox28, goalBox).
type(goal29, goal).
sourceTextEndOffset(timelineProposition1, 110).
sourceTextEndOffset(timelineProposition2, 145).
sourceTextEndOffset(timelineProposition3, 177).
sourceTextEndOffset(timelineProposition4, 209).
sourceTextEndOffset(timelineProposition5, 367).
sourceTextEndOffset(timelineProposition6, 167).
sourceTextEndOffset(timelineProposition7, 209).
sourceTextEndOffset(timelineProposition8, 263).
sourceTextEndOffset(timelineProposition9, 274).
sourceTextEndOffset(timelineProposition10, 291).
sourceTextEndOffset(timelineProposition11, 317).
sourceTextEndOffset(timelineProposition12, 445).
sourceTextEndOffset(timelineProposition13, 509).
sourceTextEndOffset(timelineProposition14, 530).
sourceTextEndOffset(timelineProposition15, 706).
sourceTextEndOffset(timelineProposition16, 724).
sourceTextEndOffset(timelineProposition17, 736).
sourceTextEndOffset(timelineProposition18, 800).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition6, goalBox19).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
equivalentOf(goal21, timelineProposition16).
equivalentOf(timelineProposition16, goal21).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal20, 'THE_GIRL_AND_THE_NARRATOR:Core_Goal').
providesFor(goal23, 'THE_GIRL:Core_Goal').
providesFor(goal29, 'THE_GIRL_AND_THE_NARRATOR:Core_Goal').
providesFor(goal27, 'THE_GIRL:Core_Goal').
providesFor(goal25, 'THE_GIRL:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 89).
sourceTextBeginOffset(timelineProposition2, 111).
sourceTextBeginOffset(timelineProposition3, 168).
sourceTextBeginOffset(timelineProposition4, 178).
sourceTextBeginOffset(timelineProposition5, 318).
sourceTextBeginOffset(timelineProposition6, 146).
sourceTextBeginOffset(timelineProposition7, 160).
sourceTextBeginOffset(timelineProposition8, 251).
sourceTextBeginOffset(timelineProposition9, 264).
sourceTextBeginOffset(timelineProposition10, 275).
sourceTextBeginOffset(timelineProposition11, 292).
sourceTextBeginOffset(timelineProposition12, 396).
sourceTextBeginOffset(timelineProposition13, 436).
sourceTextBeginOffset(timelineProposition14, 510).
sourceTextBeginOffset(timelineProposition15, 697).
sourceTextBeginOffset(timelineProposition16, 712).
sourceTextBeginOffset(timelineProposition17, 725).
sourceTextBeginOffset(timelineProposition18, 746).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal20, goalBox19).
interpNodeIn(goal21, goalBox19).
interpNodeIn(goal23, goalBox22).
interpNodeIn(goal25, goalBox24).
interpNodeIn(goal27, goalBox26).
interpNodeIn(goal29, goalBox28).
sourceText(timelineProposition1, 'Lisa reversed her car').
sourceText(timelineProposition2, 'and bumped into the car behind us.').
sourceText(timelineProposition3, 'I checked').
sourceText(timelineProposition4, 'and there wasnt anything there.').
sourceText(timelineProposition5, 'I didnt notice because her car isn\'t exactly new.').
sourceText(timelineProposition6, 'We drove away because').
sourceText(timelineProposition7, 'because I checked and there wasnt anything there.').
sourceText(timelineProposition8, 'Then we went').
sourceText(timelineProposition9, 'to Chevron').
sourceText(timelineProposition10, 'and she checked.').
sourceText(timelineProposition11, 'There was a tiny scratch.').
sourceText(timelineProposition12, 'So she GOES \'DUDE lets color it IN with markers!\'').
sourceText(timelineProposition13, 'markers!\' So then she actually goes in her car and pulls out a red marker').
sourceText(timelineProposition14, 'and starts coloring.').
sourceText(timelineProposition15, 'it worked').
sourceText(timelineProposition16, 'then we went').
sourceText(timelineProposition17, 'to barcode.').
sourceText(timelineProposition18, 'Oh and the guy that was casher-ing us was pretty cute.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition14, goal27).
attemptToCause(timelineProposition6, goal21).
attemptToCause(timelineProposition13, goal27).
wouldCause(dummy62, dummy63).
wouldCause(goal21, goal20).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition6).
followedBy(timelineProposition6, timelineProposition8).
followedBy(timelineProposition8, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition15).
followedBy(timelineProposition15, timelineProposition16).
followedBy(timelineProposition16, timelineProposition18).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition15, goal27).
interpretedAs(timelineProposition9, goal20).
interpretedAs(timelineProposition11, goal25).
interpretedAs(timelineProposition12, goalBox26).
interpretedAs(timelineProposition17, goal29).
interpretedAs(timelineProposition17, goalBox28).
interpretedAs(timelineProposition10, goalBox24).
interpretedAs(timelineProposition4, goal23).
interpretedAs(timelineProposition16, goalBox28).
interpretedAs(timelineProposition3, goalBox22).
interpretedAs(timelineProposition8, goal20).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition5, timelineProposition4).
modifies(timelineProposition7, timelineProposition6).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition17, timelineProposition16).
text(goal29, 'the girl and the narrator goes to the store').
text(goal20, 'the girl and the narrator goes to the pumping station').
text(goal21, 'the girl and the narrator travels').
text(timelineProposition4, 'and there wasnt anything there.').
text(goalBox28, 'The girl and the narrator goal for the girl and the narrator to go back to the store.').
text(timelineProposition13, 'markers!\' So then she actually goes in her car and pulls out a red marker').
text(timelineProposition5, 'I didnt notice because her car isn\'t exactly new.').
text(timelineProposition18, 'Oh and the guy that was casher-ing us was pretty cute.').
text(goalBox22, 'The narrator\'s goal for the narrator to check the first car for scratch.').
text(timelineProposition9, 'to Chevron').
text(timelineProposition14, 'and starts coloring.').
text(timelineProposition15, 'it worked').
text(goalBox19, 'The girl and the narrator goal for the girl and the narrator to travel,  which would cause the girl and the narrator to go back to the pumping station.').
text(timelineProposition16, 'then we went').
text(goalBox24, 'The girl\'s goal for the girl to check the first car for scratch.').
text(timelineProposition7, 'because I checked and there wasnt anything there.').
text(goal23, 'the narrator checks the first car for scratch').
text(timelineProposition11, 'There was a tiny scratch.').
text(goal27, 'the girl removes scratch').
text(timelineProposition3, 'I checked').
text(timelineProposition10, 'and she checked.').
text(timelineProposition12, 'So she GOES \'DUDE lets color it IN with markers!\'').
text(timelineProposition6, 'We drove away because').
text(timelineProposition1, 'Lisa reversed her car').
text(timelineProposition17, 'to barcode.').
text(goalBox26, 'The girl\'s goal for the girl to remove scratch.').
text(timelineProposition8, 'Then we went').
text(timelineProposition2, 'and bumped into the car behind us.').
text(goal25, 'the girl checks the first car for scratch').

