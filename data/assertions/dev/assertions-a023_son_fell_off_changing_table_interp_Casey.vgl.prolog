encoding('a023_son_fell_off_changing_table_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the baby').
agent(timelineProposition8, 'the baby').
agent(timelineProposition9, 'the head of the baby').
agent(timelineProposition10, 'the skull of the baby').
agent(timelineProposition11, 'the brain of the baby').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the skull of the baby').
agent(timelineProposition14, 'the brain of the baby').
agent(goalBox15, 'the narrator').
agent('THE_BABY:Core_Goal', 'the baby').
agent(goal16, 'the narrator').
agent(goalBox17, 'the narrator').
agent(goal18, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goalBox19, 'the narrator').
agent(goal20, 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition7, goal20).
attemptToPrevent(timelineProposition11, goal20).
attemptToPrevent(timelineProposition8, goal20).
attemptToPrevent(timelineProposition10, goal20).
attemptToPrevent(timelineProposition9, goal20).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(goalBox15, goalBox).
type('THE_BABY:Core_Goal', coreGoal).
type(goal16, goal).
type(goalBox17, goalBox).
type(goal18, goal).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goalBox19, goalBox).
type(goal20, goal).
sourceTextEndOffset(timelineProposition1, 158).
sourceTextEndOffset(timelineProposition2, 124).
sourceTextEndOffset(timelineProposition3, 222).
sourceTextEndOffset(timelineProposition4, 249).
sourceTextEndOffset(timelineProposition5, 267).
sourceTextEndOffset(timelineProposition6, 295).
sourceTextEndOffset(timelineProposition7, 333).
sourceTextEndOffset(timelineProposition8, 405).
sourceTextEndOffset(timelineProposition9, 451).
sourceTextEndOffset(timelineProposition10, 546).
sourceTextEndOffset(timelineProposition11, 571).
sourceTextEndOffset(timelineProposition12, 625).
sourceTextEndOffset(timelineProposition13, 748).
sourceTextEndOffset(timelineProposition14, 782).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition12, goalBox19).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal20, 'THE_BABY:Core_Goal').
providesFor(goal16, 'THE_BABY:Core_Goal').
providesFor(goal18, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 106).
sourceTextBeginOffset(timelineProposition2, 106).
sourceTextBeginOffset(timelineProposition3, 197).
sourceTextBeginOffset(timelineProposition4, 241).
sourceTextBeginOffset(timelineProposition5, 250).
sourceTextBeginOffset(timelineProposition6, 268).
sourceTextBeginOffset(timelineProposition7, 296).
sourceTextBeginOffset(timelineProposition8, 371).
sourceTextBeginOffset(timelineProposition9, 406).
sourceTextBeginOffset(timelineProposition10, 515).
sourceTextBeginOffset(timelineProposition11, 547).
sourceTextBeginOffset(timelineProposition12, 572).
sourceTextBeginOffset(timelineProposition13, 719).
sourceTextBeginOffset(timelineProposition14, 749).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal16, goalBox15).
interpNodeIn(goal18, goalBox17).
interpNodeIn(goal20, goalBox19).
sourceText(timelineProposition1, 'I was changing him on his rather high changing table').
sourceText(timelineProposition2, 'I was changing him').
sourceText(timelineProposition3, 'heard someone approaching').
sourceText(timelineProposition4, 'I turned').
sourceText(timelineProposition5, 'for half a second').
sourceText(timelineProposition6, 'to see who it was (my MIL).').
sourceText(timelineProposition7, 'My son chose that moment to roll over').
sourceText(timelineProposition8, 'and he rolled right off the table.').
sourceText(timelineProposition9, 'He landed head-first on a solid marble floor.').
sourceText(timelineProposition10, 'He fractured his tiny skull and').
sourceText(timelineProposition11, 'had some brain bruising.').
sourceText(timelineProposition12, 'I have never forgiven myself and I know I never will.').
sourceText(timelineProposition13, 'everything was back to normal').
sourceText(timelineProposition14, 'in regards to his brain activity.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition14, goal20).
attemptToCause(timelineProposition4, goal18).
attemptToCause(timelineProposition1, goal16).
attemptToCause(timelineProposition13, goal20).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition2, goalBox15).
interpretedAs(timelineProposition6, goal18).
interpretedAs(timelineProposition4, goalBox17).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition5, timelineProposition4).
modifies(timelineProposition8, timelineProposition7).
text(timelineProposition3, 'heard someone approaching').
text(timelineProposition7, 'My son chose that moment to roll over').
text(timelineProposition4, 'I turned').
text(timelineProposition2, 'I was changing him').
text(timelineProposition13, 'everything was back to normal').
text(goal20, 'the baby is healthy').
text(goalBox17, 'The narrator\'s goal for the narrator to see a person.').
text(timelineProposition8, 'and he rolled right off the table.').
text(timelineProposition10, 'He fractured his tiny skull and').
text(timelineProposition9, 'He landed head-first on a solid marble floor.').
text(goal18, 'the narrator sees a person').
text(goalBox19, 'The narrator\'s goal for the baby to be healthy.').
text(timelineProposition14, 'in regards to his brain activity.').
text(timelineProposition5, 'for half a second').
text(goal16, 'the narrator bathes the baby').
text(timelineProposition6, 'to see who it was (my MIL).').
text(timelineProposition12, 'I have never forgiven myself and I know I never will.').
text(timelineProposition11, 'had some brain bruising.').
text(goalBox15, 'The narrator\'s goal for the narrator to bathe the baby.').
text(timelineProposition1, 'I was changing him on his rather high changing table').

