encoding('a025_bridal_magazine_incident_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the counterperson').
agent(timelineProposition8, 'the counterperson').
agent(timelineProposition9, 'the counterperson').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the narrator').
agent(goalBox14, 'the narrator').
agent(goal15, 'the narrator').
agent(goal16, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition8, goal16).
attemptToPrevent(timelineProposition12, goal15).
attemptToPrevent(timelineProposition10, goal15).
attemptToPrevent(timelineProposition13, goal15).
attemptToPrevent(timelineProposition7, goal16).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(goalBox14, goalBox).
type(goal15, goal).
type(goal16, goal).
type('THE_NARRATOR:Core_Goal', coreGoal).
sourceTextEndOffset(timelineProposition1, 118).
sourceTextEndOffset(timelineProposition2, 186).
sourceTextEndOffset(timelineProposition3, 218).
sourceTextEndOffset(timelineProposition5, 235).
sourceTextEndOffset(timelineProposition8, 454).
sourceTextEndOffset(timelineProposition11, 496).
sourceTextEndOffset(timelineProposition12, 564).
sourceTextEndOffset(timelineProposition13, 592).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition2, goalBox14).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal16, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 101).
sourceTextBeginOffset(timelineProposition2, 165).
sourceTextBeginOffset(timelineProposition3, 187).
sourceTextBeginOffset(timelineProposition5, 219).
sourceTextBeginOffset(timelineProposition8, 422).
sourceTextBeginOffset(timelineProposition11, 455).
sourceTextBeginOffset(timelineProposition12, 539).
sourceTextBeginOffset(timelineProposition13, 565).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal15, goalBox14).
interpNodeIn(goal16, goalBox14).
sourceText(timelineProposition1, 'I go there a lot,').
sourceText(timelineProposition2, 'I saw he was working,').
sourceText(timelineProposition3, 'so I grabbed a couple magazines').
sourceText(timelineProposition5, 'and headed over.').
sourceText(timelineProposition8, 'And of course he said something.').
sourceText(timelineProposition11, 'And of course my only reply was to mumble').
sourceText(timelineProposition12, 'He didn\'t look convinced.').
sourceText(timelineProposition13, 'So, now I\'m the weird chick').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition11, goal15).
attemptToCause(timelineProposition5, goal16).
attemptToCause(timelineProposition3, goal16).
wouldCause(dummy62, dummy63).
wouldCause(goal15, goal16).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition2).
followedBy(timelineProposition2, timelineProposition3).
followedBy(timelineProposition3, timelineProposition5).
followedBy(timelineProposition5, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
interpretedAs(dummy66, dummy67).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition4, timelineProposition3).
modifies(timelineProposition6, timelineProposition5).
modifies(timelineProposition9, timelineProposition8).
text(timelineProposition11, 'And of course my only reply was to mumble').
text(timelineProposition13, 'So, now I\'m the weird chick').
text(timelineProposition3, 'so I grabbed a couple magazines').
text(timelineProposition1, 'I go there a lot,').
text(goalBox14, 'The narrator\'s goal for the narrator to be attractive,  which would cause the narrator to impress the counterperson.').
text(timelineProposition5, 'and headed over.').
text(timelineProposition2, 'I saw he was working,').
text(timelineProposition8, 'And of course he said something.').
text(goal16, 'the narrator impresses the counterperson').
text(timelineProposition12, 'He didn\'t look convinced.').
text(goal15, 'the narrator is attractive').

