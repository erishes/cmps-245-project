encoding('a028_got_a_ride_home_from_crush_in_class_interp_Casey.vgl').
agent(timelineProposition1, 'the woman').
agent(timelineProposition2, 'the woman').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition3, 'the student').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition4, 'the student').
agent(timelineProposition5, 'the student').
agent(timelineProposition6, 'the student').
agent(timelineProposition7, 'the student').
agent(timelineProposition8, 'the woman').
agent(timelineProposition9, 'the woman').
agent(timelineProposition10, 'the woman').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the woman').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the woman').
agent(timelineProposition18, 'the woman').
agent(timelineProposition19, 'the narrator').
agent(timelineProposition20, 'the narrator').
agent(goalBox21, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal22, 'the narrator').
agent(goalBox23, 'the narrator').
agent(goal24, 'the narrator').
agent(goalBox25, 'the student').
agent('THE_STUDENT:Core_Goal', 'the student').
agent(goal26, 'the student').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition16, goal22).
attemptToPrevent(timelineProposition12, goal22).
attemptToPrevent(timelineProposition11, goal22).
attemptToPrevent(timelineProposition15, goal22).
attemptToPrevent(timelineProposition14, goal22).
attemptToPrevent(timelineProposition13, goal22).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(goalBox21, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal22, goal).
type(goalBox23, goalBox).
type(goal24, goal).
type(goalBox25, goalBox).
type('THE_STUDENT:Core_Goal', coreGoal).
type(goal26, goal).
sourceTextEndOffset(timelineProposition1, 63).
sourceTextEndOffset(timelineProposition2, 124).
sourceTextEndOffset(timelineProposition3, 173).
sourceTextEndOffset(timelineProposition5, 214).
sourceTextEndOffset(timelineProposition6, 241).
sourceTextEndOffset(timelineProposition8, 260).
sourceTextEndOffset(timelineProposition9, 277).
sourceTextEndOffset(timelineProposition10, 280).
sourceTextEndOffset(timelineProposition11, 362).
sourceTextEndOffset(timelineProposition13, 451).
sourceTextEndOffset(timelineProposition14, 475).
sourceTextEndOffset(timelineProposition15, 483).
sourceTextEndOffset(timelineProposition17, 404).
sourceTextEndOffset(timelineProposition18, 416).
sourceTextEndOffset(timelineProposition19, 612).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition1, goalBox21).
implies(timelineProposition20, goalBox21).
implies(timelineProposition19, goalBox21).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal26, 'THE_STUDENT:Core_Goal').
providesFor(goal24, 'THE_NARRATOR:Core_Goal').
providesFor(goal22, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 0).
sourceTextBeginOffset(timelineProposition2, 64).
sourceTextBeginOffset(timelineProposition3, 125).
sourceTextBeginOffset(timelineProposition5, 174).
sourceTextBeginOffset(timelineProposition6, 215).
sourceTextBeginOffset(timelineProposition8, 242).
sourceTextBeginOffset(timelineProposition9, 261).
sourceTextBeginOffset(timelineProposition10, 278).
sourceTextBeginOffset(timelineProposition11, 338).
sourceTextBeginOffset(timelineProposition13, 417).
sourceTextBeginOffset(timelineProposition14, 452).
sourceTextBeginOffset(timelineProposition15, 476).
sourceTextBeginOffset(timelineProposition17, 363).
sourceTextBeginOffset(timelineProposition18, 405).
sourceTextBeginOffset(timelineProposition19, 484).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal22, goalBox21).
interpNodeIn(goal24, goalBox23).
interpNodeIn(goal26, goalBox25).
sourceText(timelineProposition1, 'There\'s a woman in one f my classes who I am very attracted to.').
sourceText(timelineProposition2, 'Lucky for me, she offered to give me a ride home every week.').
sourceText(timelineProposition3, 'Last week she drove me and another student home,').
sourceText(timelineProposition5, 'but the other guy has a car now I guess,').
sourceText(timelineProposition6, 'so it was just me and her.').
sourceText(timelineProposition8, 'She sort of walked').
sourceText(timelineProposition9, 'right up next to').
sourceText(timelineProposition10, me).
sourceText(timelineProposition11, 'But I didn\'t make a move').
sourceText(timelineProposition13, 'she mentioned she has a boyfriend,').
sourceText(timelineProposition14, 'so I didn\'t make a move').
sourceText(timelineProposition15, 'on her.').
sourceText(timelineProposition17, 'then, and right before she dropped me off').
sourceText(timelineProposition18, 'at my house').
sourceText(timelineProposition19, 'Does this mean I had my chance and missed it, or that I said something stupid in the car and she isn\'t attracted to me any more?').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition4, goal26).
attemptToCause(timelineProposition4, goal24).
attemptToCause(timelineProposition17, goal24).
attemptToCause(timelineProposition18, goal24).
attemptToCause(timelineProposition3, goal26).
attemptToCause(timelineProposition3, goal24).
attemptToCause(timelineProposition5, goal26).
attemptToCause(timelineProposition7, goal26).
attemptToCause(timelineProposition2, goal26).
attemptToCause(timelineProposition2, goal24).
attemptToCause(timelineProposition6, goal26).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition2).
followedBy(timelineProposition2, timelineProposition3).
followedBy(timelineProposition3, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition8).
followedBy(timelineProposition8, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition17).
followedBy(timelineProposition17, timelineProposition19).
interpretedAs(dummy66, dummy67).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition4, timelineProposition3).
modifies(timelineProposition7, timelineProposition6).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition10, timelineProposition8).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition15, timelineProposition14).
modifies(timelineProposition16, timelineProposition14).
modifies(timelineProposition18, timelineProposition17).
modifies(timelineProposition20, timelineProposition19).
text(timelineProposition13, 'she mentioned she has a boyfriend,').
text(timelineProposition18, 'at my house').
text(timelineProposition11, 'But I didn\'t make a move').
text(goalBox21, 'The narrator\'s goal for the narrator to date the woman.').
text(timelineProposition14, 'so I didn\'t make a move').
text(timelineProposition1, 'There\'s a woman in one f my classes who I am very attracted to.').
text(goal24, 'the narrator travels to the house').
text(timelineProposition2, 'Lucky for me, she offered to give me a ride home every week.').
text(goal22, 'the narrator dates the woman').
text(timelineProposition3, 'Last week she drove me and another student home,').
text(timelineProposition19, 'Does this mean I had my chance and missed it, or that I said something stupid in the car and she isn\'t attracted to me any more?').
text(timelineProposition9, 'right up next to').
text(timelineProposition6, 'so it was just me and her.').
text(goal26, 'the student travels to the second house').
text(goalBox25, 'The student\'s goal for the student to travel to the second house.').
text(timelineProposition5, 'but the other guy has a car now I guess,').
text(goalBox23, 'The narrator\'s goal for the narrator to travel to the house.').
text(timelineProposition15, 'on her.').
text(timelineProposition10, me).
text(timelineProposition17, 'then, and right before she dropped me off').
text(timelineProposition8, 'She sort of walked').

