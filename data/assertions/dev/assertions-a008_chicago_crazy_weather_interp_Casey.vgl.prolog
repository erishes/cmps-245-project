encoding('a008_chicago_crazy_weather_interp_Casey.vgl').
agent(timelineProposition1, 'Chicago').
agent(timelineProposition2, 'Chicago').
agent(timelineProposition3, 'the rain').
agent(timelineProposition4, 'the rain').
agent(timelineProposition5, 'the rain').
agent(timelineProposition6, 'the rain').
agent(timelineProposition7, 'the man').
agent(timelineProposition8, 'the man').
agent(timelineProposition9, 'the man').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the man').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the man').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the man').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the man').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the man').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the man').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the narrator').
agent(timelineProposition18, 'the narrator').
agent(timelineProposition19, 'the narrator').
agent(timelineProposition20, 'the narrator').
agent(timelineProposition21, 'the man').
agent(timelineProposition22, 'the man').
agent(timelineProposition23, 'the dad of the narrator').
agent(goalBox24, 'the man').
agent(goalBox24, 'the narrator').
agent('THE_MAN_AND_THE_NARRATOR:Core_Goal', 'the man').
agent('THE_MAN_AND_THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal25, 'the man').
agent(goal25, 'the narrator').
agent(goalBox26, 'the narrator').
agent('THE_GROUP_OF_DEER:Core_Goal', 'the first group of deer').
agent(goal27, 'the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(timelineProposition22, timelineProposition).
type(timelineProposition23, timelineProposition).
type(goalBox24, goalBox).
type('THE_MAN_AND_THE_NARRATOR:Core_Goal', coreGoal).
type(goal25, goal).
type(goalBox26, goalBox).
type('THE_GROUP_OF_DEER:Core_Goal', coreGoal).
type(goal27, goal).
sourceTextEndOffset(timelineProposition1, 103).
sourceTextEndOffset(timelineProposition3, 221).
sourceTextEndOffset(timelineProposition5, 331).
sourceTextEndOffset(timelineProposition6, 331).
sourceTextEndOffset(timelineProposition7, 359).
sourceTextEndOffset(timelineProposition8, 346).
sourceTextEndOffset(timelineProposition9, 424).
sourceTextEndOffset(timelineProposition10, 486).
sourceTextEndOffset(timelineProposition11, 499).
sourceTextEndOffset(timelineProposition12, 587).
sourceTextEndOffset(timelineProposition13, 621).
sourceTextEndOffset(timelineProposition14, 635).
sourceTextEndOffset(timelineProposition15, 659).
sourceTextEndOffset(timelineProposition16, 687).
sourceTextEndOffset(timelineProposition17, 775).
sourceTextEndOffset(timelineProposition18, 833).
sourceTextEndOffset(timelineProposition19, 907).
sourceTextEndOffset(timelineProposition21, 873).
sourceTextEndOffset(timelineProposition22, 889).
sourceTextEndOffset(timelineProposition23, 1011).
because(dummy38, dummy39).
implies(dummy40, dummy41).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition21, goal27).
ceases(timelineProposition11, goal25).
providesFor(dummy48, dummy49).
providesFor(goal25, 'THE_MAN_AND_THE_NARRATOR:Core_Goal').
providesFor(goal27, 'THE_GROUP_OF_DEER:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 0).
sourceTextBeginOffset(timelineProposition3, 175).
sourceTextBeginOffset(timelineProposition5, 286).
sourceTextBeginOffset(timelineProposition6, 310).
sourceTextBeginOffset(timelineProposition7, 332).
sourceTextBeginOffset(timelineProposition8, 341).
sourceTextBeginOffset(timelineProposition9, 360).
sourceTextBeginOffset(timelineProposition10, 459).
sourceTextBeginOffset(timelineProposition11, 487).
sourceTextBeginOffset(timelineProposition12, 500).
sourceTextBeginOffset(timelineProposition13, 588).
sourceTextBeginOffset(timelineProposition14, 622).
sourceTextBeginOffset(timelineProposition15, 636).
sourceTextBeginOffset(timelineProposition16, 660).
sourceTextBeginOffset(timelineProposition17, 703).
sourceTextBeginOffset(timelineProposition18, 776).
sourceTextBeginOffset(timelineProposition19, 890).
sourceTextBeginOffset(timelineProposition21, 834).
sourceTextBeginOffset(timelineProposition22, 874).
sourceTextBeginOffset(timelineProposition23, 908).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal25, goalBox24).
interpNodeIn(goal27, goalBox26).
sourceText(timelineProposition1, 'We\'ve seriously had some crazy weather these past couple of days... it was nonstop rain, and more rain!').
sourceText(timelineProposition3, 'We got a tiny amount of water in our basement,').
sourceText(timelineProposition5, 'The rain finally let up early this afternoon,').
sourceText(timelineProposition6, 'early this afternoon,').
sourceText(timelineProposition7, 'and John and I went driving').
sourceText(timelineProposition8, 'and I').
sourceText(timelineProposition9, 'around and were stunned by all the flooding in our neighborhood.').
sourceText(timelineProposition10, 'We tried to go see a movie,').
sourceText(timelineProposition11, 'but couldn\'t').
sourceText(timelineProposition12, 'get to the movie theatre because all the surrounding roads were flooded and closed off.').
sourceText(timelineProposition13, 'At one point we saw about 25 deer').
sourceText(timelineProposition14, 'in a clearing').
sourceText(timelineProposition15, 'near a forest preserve,').
sourceText(timelineProposition16, 'and I saw a bunch more deer').
sourceText(timelineProposition17, 'When I looked into the forest, I could see how flooded and murky it was.').
sourceText(timelineProposition18, 'The poor deer have nowhere to go! I feel so bad for them.').
sourceText(timelineProposition19, 'yes, I wanted to.').
sourceText(timelineProposition21, 'John would not let me bring any of them').
sourceText(timelineProposition22, 'home with us...').
sourceText(timelineProposition23, 'My dad told me that coyotes are traveling around the city in packs in broad daylight, looking for food,').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition7, goal25).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition5).
followedBy(timelineProposition5, timelineProposition7).
followedBy(timelineProposition7, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
followedBy(timelineProposition17, timelineProposition18).
followedBy(timelineProposition18, timelineProposition19).
followedBy(timelineProposition19, timelineProposition21).
followedBy(timelineProposition21, timelineProposition23).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition19, goalBox26).
interpretedAs(timelineProposition10, goalBox24).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition4, timelineProposition3).
modifies(timelineProposition6, timelineProposition5).
modifies(timelineProposition8, timelineProposition7).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition14, timelineProposition13).
modifies(timelineProposition15, timelineProposition13).
modifies(timelineProposition20, timelineProposition19).
modifies(timelineProposition22, timelineProposition21).
text(timelineProposition13, 'At one point we saw about 25 deer').
text(goal25, 'the man and the narrator watches a movie').
text(timelineProposition15, 'near a forest preserve,').
text(timelineProposition5, 'The rain finally let up early this afternoon,').
text(timelineProposition19, 'yes, I wanted to.').
text(timelineProposition7, 'and John and I went driving').
text(timelineProposition8, 'and I').
text(timelineProposition1, 'We\'ve seriously had some crazy weather these past couple of days... it was nonstop rain, and more rain!').
text(timelineProposition23, 'My dad told me that coyotes are traveling around the city in packs in broad daylight, looking for food,').
text(timelineProposition6, 'early this afternoon,').
text(timelineProposition11, 'but couldn\'t').
text(goalBox24, 'The man and the narrator goal for the man and the narrator to watch a movie.').
text(timelineProposition16, 'and I saw a bunch more deer').
text(timelineProposition3, 'We got a tiny amount of water in our basement,').
text(goal27, 'the narrator brings to the home the first group of deer').
text(timelineProposition18, 'The poor deer have nowhere to go! I feel so bad for them.').
text(timelineProposition21, 'John would not let me bring any of them').
text(timelineProposition12, 'get to the movie theatre because all the surrounding roads were flooded and closed off.').
text(timelineProposition22, 'home with us...').
text(timelineProposition10, 'We tried to go see a movie,').
text(timelineProposition14, 'in a clearing').
text(timelineProposition9, 'around and were stunned by all the flooding in our neighborhood.').
text(timelineProposition17, 'When I looked into the forest, I could see how flooded and murky it was.').
text(goalBox26, 'The narrator\'s goal for the narrator to bring to the home the first group of deer.').

