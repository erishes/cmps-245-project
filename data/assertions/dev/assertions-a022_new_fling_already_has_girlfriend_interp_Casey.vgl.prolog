encoding('a022_new_fling_already_has_girlfriend_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition4, 'the boy').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition8, 'the boy').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition9, 'the boy').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the narrator').
agent(timelineProposition17, 'the narrator').
agent(goalBox18, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal19, 'the narrator').
agent(goalBox20, 'the narrator').
agent(goal21, 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition10, goal19).
attemptToPrevent(timelineProposition16, goal19).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(goalBox18, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal19, goal).
type(goalBox20, goalBox).
type(goal21, goal).
sourceTextEndOffset(timelineProposition1, 14).
sourceTextEndOffset(timelineProposition2, 21).
sourceTextEndOffset(timelineProposition3, 238).
sourceTextEndOffset(timelineProposition4, 72).
sourceTextEndOffset(timelineProposition5, 293).
sourceTextEndOffset(timelineProposition8, 96).
sourceTextEndOffset(timelineProposition9, 161).
sourceTextEndOffset(timelineProposition10, 354).
sourceTextEndOffset(timelineProposition11, 398).
sourceTextEndOffset(timelineProposition12, 372).
sourceTextEndOffset(timelineProposition13, 597).
sourceTextEndOffset(timelineProposition14, 639).
sourceTextEndOffset(timelineProposition15, 607).
sourceTextEndOffset(timelineProposition16, 675).
sourceTextEndOffset(timelineProposition17, 705).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition13, goalBox18).
implies(timelineProposition5, goalBox18).
implies(timelineProposition3, goalBox18).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal19, 'THE_NARRATOR:Core_Goal').
providesFor(goal21, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 0).
sourceTextBeginOffset(timelineProposition2, 15).
sourceTextBeginOffset(timelineProposition3, 202).
sourceTextBeginOffset(timelineProposition4, 50).
sourceTextBeginOffset(timelineProposition5, 239).
sourceTextBeginOffset(timelineProposition8, 73).
sourceTextBeginOffset(timelineProposition9, 141).
sourceTextBeginOffset(timelineProposition10, 294).
sourceTextBeginOffset(timelineProposition11, 373).
sourceTextBeginOffset(timelineProposition12, 355).
sourceTextBeginOffset(timelineProposition13, 555).
sourceTextBeginOffset(timelineProposition14, 598).
sourceTextBeginOffset(timelineProposition15, 602).
sourceTextBeginOffset(timelineProposition16, 640).
sourceTextBeginOffset(timelineProposition17, 676).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal19, goalBox18).
interpNodeIn(goal21, goalBox20).
sourceText(timelineProposition1, 'I met this boy').
sourceText(timelineProposition2, 'Sunday').
sourceText(timelineProposition3, 'I fell for him the second I saw him,').
sourceText(timelineProposition4, 'did a lot of flirting!').
sourceText(timelineProposition5, 'and then when I got to know him I was head over heals!').
sourceText(timelineProposition8, 'Then we started texting').
sourceText(timelineProposition9, 'saying flirty things').
sourceText(timelineProposition10, 'Then I found out he had he had a girl friend the whole time!').
sourceText(timelineProposition11, 'I don\'t know how to cope,').
sourceText(timelineProposition12, 'He never told me!').
sourceText(timelineProposition13, 'And I really want to stay friends with him').
sourceText(timelineProposition14, 'and maybe it will lead to something else!').
sourceText(timelineProposition15, maybe).
sourceText(timelineProposition16, 'But I dont know if I can trust him!').
sourceText(timelineProposition17, 'Im so lost and I need advice,').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition8, goal19).
attemptToCause(timelineProposition4, goal19).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition14, goalBox18).
interpretedAs(timelineProposition17, goalBox20).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition6, timelineProposition5).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition15, timelineProposition14).
text(timelineProposition4, 'did a lot of flirting!').
text(timelineProposition11, 'I don\'t know how to cope,').
text(goalBox20, 'The narrator\'s goal for the narrator to be advised.').
text(timelineProposition14, 'and maybe it will lead to something else!').
text(timelineProposition10, 'Then I found out he had he had a girl friend the whole time!').
text(timelineProposition17, 'Im so lost and I need advice,').
text(timelineProposition5, 'and then when I got to know him I was head over heals!').
text(timelineProposition16, 'But I dont know if I can trust him!').
text(timelineProposition12, 'He never told me!').
text(timelineProposition1, 'I met this boy').
text(timelineProposition8, 'Then we started texting').
text(timelineProposition15, maybe).
text(goalBox18, 'The narrator\'s goal for the narrator to date the boy.').
text(goal21, 'the narrator is advised').
text(timelineProposition13, 'And I really want to stay friends with him').
text(timelineProposition3, 'I fell for him the second I saw him,').
text(goal19, 'the narrator dates the boy').
text(timelineProposition2, 'Sunday').
text(timelineProposition9, 'saying flirty things').

