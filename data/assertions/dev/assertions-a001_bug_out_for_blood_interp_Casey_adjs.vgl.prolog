encoding('a001_bug_out_for_blood_interp_Casey_adjs.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the group of bugs').
agent(timelineProposition5, 'the group of bugs').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the group of bugs').
agent(timelineProposition17, 'the narrator').
agent(timelineProposition18, 'the narrator').
agent(timelineProposition19, 'the narrator').
agent(timelineProposition20, 'the narrator').
agent(timelineProposition21, 'the narrator').
agent(timelineProposition22, 'the narrator').
agent(timelineProposition23, 'the narrator').
agent(timelineProposition24, 'the leader of the group of bugs').
agent(timelineProposition25, 'the leader of the group of bugs').
agent(timelineProposition26, 'the leader of the group of bugs').
agent(timelineProposition27, 'the leader of the group of bugs').
agent(timelineProposition28, 'the leader of the group of bugs').
agent(timelineProposition29, 'the leader of the group of bugs').
agent(timelineProposition30, 'the leader of the group of bugs').
agent(timelineProposition31, 'the narrator').
agent(timelineProposition32, 'the narrator').
agent(timelineProposition33, 'the narrator').
agent(timelineProposition34, 'the narrator').
agent(timelineProposition35, 'the narrator').
agent(timelineProposition36, 'the group of cicadas').
agent(timelineProposition37, 'the group of cicadas').
agent(timelineProposition38, 'the group of cicadas').
agent(timelineProposition39, 'the group of cicadas').
agent(timelineProposition40, 'every action of the story of the narrator').
agent(timelineProposition41, 'every action of the story of the narrator').
agent(goalBox42, 'the narrator').
agent(goal43, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goalBox44, 'the group of bugs').
agent('THE_GROUP_OF_BUGS:Core_Goal', 'the group of bugs').
agent(goal45, 'the group of bugs').
agent(goalBox46, 'the narrator').
agent(goal47, 'the narrator').
agent(goalBox48, 'the narrator').
agent(goal49, 'the narrator').
agent(goalBox50, 'the leader of the group of bugs').
agent('THE_LEADER_OF_THE_GROUP_OF_BUGS:Core_Goal', 'the leader of the group of bugs').
agent(goal51, 'the leader of the group of bugs').
agent(goalBox52, 'the narrator').
agent(goal53, 'the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(timelineProposition22, timelineProposition).
type(timelineProposition23, timelineProposition).
type(timelineProposition24, timelineProposition).
type(timelineProposition25, timelineProposition).
type(timelineProposition26, timelineProposition).
type(timelineProposition27, timelineProposition).
type(timelineProposition28, timelineProposition).
type(timelineProposition29, timelineProposition).
type(timelineProposition30, timelineProposition).
type(timelineProposition31, timelineProposition).
type(timelineProposition32, timelineProposition).
type(timelineProposition33, timelineProposition).
type(timelineProposition34, timelineProposition).
type(timelineProposition35, timelineProposition).
type(timelineProposition36, timelineProposition).
type(timelineProposition37, timelineProposition).
type(timelineProposition38, timelineProposition).
type(timelineProposition39, timelineProposition).
type(timelineProposition40, timelineProposition).
type(timelineProposition41, timelineProposition).
type(goalBox42, goalBox).
type(goal43, goal).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goalBox44, goalBox).
type('THE_GROUP_OF_BUGS:Core_Goal', coreGoal).
type(goal45, goal).
type(goalBox46, goalBox).
type(goal47, goal).
type(goalBox48, goalBox).
type(goal49, goal).
type(goalBox50, goalBox).
type('THE_LEADER_OF_THE_GROUP_OF_BUGS:Core_Goal', coreGoal).
type(goal51, goal).
type(goalBox52, goalBox).
type(goal53, goal).
sourceTextEndOffset(timelineProposition1, 61).
sourceTextEndOffset(timelineProposition2, 34).
sourceTextEndOffset(timelineProposition3, 78).
sourceTextEndOffset(timelineProposition4, 118).
sourceTextEndOffset(timelineProposition6, 139).
sourceTextEndOffset(timelineProposition7, 145).
sourceTextEndOffset(timelineProposition8, 170).
sourceTextEndOffset(timelineProposition10, 175).
sourceTextEndOffset(timelineProposition11, 170).
sourceTextEndOffset(timelineProposition12, 211).
sourceTextEndOffset(timelineProposition14, 314).
sourceTextEndOffset(timelineProposition15, 239).
sourceTextEndOffset(timelineProposition16, 335).
sourceTextEndOffset(timelineProposition17, 335).
sourceTextEndOffset(timelineProposition18, 314).
sourceTextEndOffset(timelineProposition20, 370).
sourceTextEndOffset(timelineProposition21, 402).
sourceTextEndOffset(timelineProposition22, 428).
sourceTextEndOffset(timelineProposition23, 457).
sourceTextEndOffset(timelineProposition24, 493).
sourceTextEndOffset(timelineProposition25, 519).
sourceTextEndOffset(timelineProposition26, 501).
sourceTextEndOffset(timelineProposition27, 519).
sourceTextEndOffset(timelineProposition28, 464).
sourceTextEndOffset(timelineProposition29, 567).
sourceTextEndOffset(timelineProposition30, 528).
sourceTextEndOffset(timelineProposition31, 595).
sourceTextEndOffset(timelineProposition32, 615).
sourceTextEndOffset(timelineProposition33, 636).
sourceTextEndOffset(timelineProposition34, 675).
sourceTextEndOffset(timelineProposition35, 675).
sourceTextEndOffset(timelineProposition36, 727).
sourceTextEndOffset(timelineProposition37, 714).
sourceTextEndOffset(timelineProposition38, 727).
sourceTextEndOffset(timelineProposition39, 727).
sourceTextEndOffset(timelineProposition40, 685).
because(dummy38, dummy39).
implies(dummy40, dummy41).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
equivalentOf(goal43, timelineProposition1).
equivalentOf(timelineProposition1, goal43).
equivalentOf(goal45, timelineProposition4).
equivalentOf(timelineProposition4, goal45).
equivalentOf(goal47, timelineProposition8).
equivalentOf(timelineProposition8, goal47).
ceases(dummy46, dummy47).
ceases(timelineProposition25, goal49).
ceases(timelineProposition22, goal49).
ceases(timelineProposition10, goal47).
providesFor(dummy48, dummy49).
providesFor(goal43, 'THE_NARRATOR:Core_Goal').
providesFor(goal45, 'THE_GROUP_OF_BUGS:Core_Goal').
providesFor(goal53, 'THE_NARRATOR:Core_Goal').
providesFor(goal49, 'THE_NARRATOR:Core_Goal').
providesFor(goal51, 'THE_LEADER_OF_THE_GROUP_OF_BUGS:Core_Goal').
providesFor(goal47, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
damages(goal49, 'THE_GROUP_OF_BUGS:Core_Goal').
sourceTextBeginOffset(timelineProposition1, 35).
sourceTextBeginOffset(timelineProposition2, 18).
sourceTextBeginOffset(timelineProposition3, 62).
sourceTextBeginOffset(timelineProposition4, 79).
sourceTextBeginOffset(timelineProposition6, 119).
sourceTextBeginOffset(timelineProposition7, 140).
sourceTextBeginOffset(timelineProposition8, 140).
sourceTextBeginOffset(timelineProposition10, 140).
sourceTextBeginOffset(timelineProposition11, 140).
sourceTextBeginOffset(timelineProposition12, 176).
sourceTextBeginOffset(timelineProposition14, 240).
sourceTextBeginOffset(timelineProposition15, 212).
sourceTextBeginOffset(timelineProposition16, 323).
sourceTextBeginOffset(timelineProposition17, 315).
sourceTextBeginOffset(timelineProposition18, 283).
sourceTextBeginOffset(timelineProposition20, 336).
sourceTextBeginOffset(timelineProposition21, 371).
sourceTextBeginOffset(timelineProposition22, 404).
sourceTextBeginOffset(timelineProposition23, 429).
sourceTextBeginOffset(timelineProposition24, 473).
sourceTextBeginOffset(timelineProposition25, 458).
sourceTextBeginOffset(timelineProposition26, 465).
sourceTextBeginOffset(timelineProposition27, 502).
sourceTextBeginOffset(timelineProposition28, 458).
sourceTextBeginOffset(timelineProposition29, 520).
sourceTextBeginOffset(timelineProposition30, 520).
sourceTextBeginOffset(timelineProposition31, 568).
sourceTextBeginOffset(timelineProposition32, 596).
sourceTextBeginOffset(timelineProposition33, 616).
sourceTextBeginOffset(timelineProposition34, 637).
sourceTextBeginOffset(timelineProposition35, 647).
sourceTextBeginOffset(timelineProposition36, 686).
sourceTextBeginOffset(timelineProposition37, 710).
sourceTextBeginOffset(timelineProposition38, 715).
sourceTextBeginOffset(timelineProposition39, 723).
sourceTextBeginOffset(timelineProposition40, 676).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal43, goalBox42).
interpNodeIn(goal45, goalBox44).
interpNodeIn(goal47, goalBox46).
interpNodeIn(goal49, goalBox48).
interpNodeIn(goal51, goalBox50).
interpNodeIn(goal53, goalBox52).
sourceText(timelineProposition1, 'I left the patio door open').
sourceText(timelineProposition2, 'the other night,').
sourceText(timelineProposition3, 'just long enough').
sourceText(timelineProposition4, 'to let in a dozen bugs of various size.').
sourceText(timelineProposition6, 'I didn\'t notice them').
sourceText(timelineProposition7, until).
sourceText(timelineProposition8, 'until the middle of the night,').
sourceText(timelineProposition10, 'until the middle of the night, when').
sourceText(timelineProposition11, 'until the middle of the night,').
sourceText(timelineProposition12, 'I saw them clinging to the ceiling.').
sourceText(timelineProposition14, 'I grabbed the closest object within reach, and with a rolled-up comic book').
sourceText(timelineProposition15, 'Since I\'m such a bugaphobe,').
sourceText(timelineProposition16, 'mine enemies').
sourceText(timelineProposition17, 'I smote mine enemies').
sourceText(timelineProposition18, 'and with a rolled-up comic book').
sourceText(timelineProposition20, 'and smeared their greasy bug guts.').
sourceText(timelineProposition21, 'All except for the biggest one.').
sourceText(timelineProposition22, 'I only clipped that one,').
sourceText(timelineProposition23, 'taking off one of its limbs.').
sourceText(timelineProposition24, 'a five-limbed insect').
sourceText(timelineProposition25, 'So now there\'s a five-limbed insect lurking in the apartment,').
sourceText(timelineProposition26, 'there\'s a five-limbed insect lurking').
sourceText(timelineProposition27, 'in the apartment,').
sourceText(timelineProposition28, 'So now').
sourceText(timelineProposition29, 'no doubt looking for some vengeance against me.').
sourceText(timelineProposition30, 'no doubt').
sourceText(timelineProposition31, 'I\'m looking around corners,').
sourceText(timelineProposition32, 'checking the toilet').
sourceText(timelineProposition33, 'before sitting down,').
sourceText(timelineProposition34, 'wondering when it\'s going to jump out.').
sourceText(timelineProposition35, 'when it\'s going to jump out.').
sourceText(timelineProposition36, 'and the cicadas haven\'t even arrived yet.').
sourceText(timelineProposition37, even).
sourceText(timelineProposition38, 'arrived yet.').
sourceText(timelineProposition39, 'yet.').
sourceText(timelineProposition40, 'All this,').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition23, goal49).
attemptToCause(timelineProposition32, goal53).
attemptToCause(timelineProposition31, goal53).
attemptToCause(timelineProposition14, goal49).
attemptToCause(timelineProposition21, goal49).
attemptToCause(timelineProposition26, goal51).
attemptToCause(timelineProposition20, goal49).
attemptToCause(timelineProposition17, goal49).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition4).
followedBy(timelineProposition4, timelineProposition6).
followedBy(timelineProposition6, timelineProposition8).
followedBy(timelineProposition8, timelineProposition10).
followedBy(timelineProposition10, timelineProposition12).
followedBy(timelineProposition12, timelineProposition14).
followedBy(timelineProposition14, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
followedBy(timelineProposition17, timelineProposition20).
followedBy(timelineProposition20, timelineProposition21).
followedBy(timelineProposition21, timelineProposition22).
followedBy(timelineProposition22, timelineProposition23).
followedBy(timelineProposition23, timelineProposition24).
followedBy(timelineProposition24, timelineProposition25).
followedBy(timelineProposition25, timelineProposition26).
followedBy(timelineProposition26, timelineProposition29).
followedBy(timelineProposition29, timelineProposition31).
followedBy(timelineProposition31, timelineProposition32).
followedBy(timelineProposition32, timelineProposition34).
followedBy(timelineProposition34, timelineProposition36).
followedBy(timelineProposition36, timelineProposition40).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition8, goal47).
interpretedAs(timelineProposition8, goalBox46).
interpretedAs(timelineProposition1, goal43).
interpretedAs(timelineProposition1, goalBox42).
interpretedAs(timelineProposition16, goalBox48).
interpretedAs(timelineProposition34, goalBox52).
interpretedAs(timelineProposition29, goalBox50).
interpretedAs(timelineProposition4, goal45).
interpretedAs(timelineProposition4, goalBox44).
interpretedAs(timelineProposition17, goalBox48).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition3, timelineProposition1).
modifies(timelineProposition5, timelineProposition4).
modifies(timelineProposition7, timelineProposition6).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition11, timelineProposition10).
modifies(timelineProposition13, timelineProposition12).
modifies(timelineProposition15, timelineProposition14).
modifies(timelineProposition18, timelineProposition17).
modifies(timelineProposition19, timelineProposition17).
modifies(timelineProposition27, timelineProposition26).
modifies(timelineProposition28, timelineProposition26).
modifies(timelineProposition30, timelineProposition29).
modifies(timelineProposition33, timelineProposition32).
modifies(timelineProposition35, timelineProposition34).
modifies(timelineProposition37, timelineProposition36).
modifies(timelineProposition38, timelineProposition36).
modifies(timelineProposition39, timelineProposition36).
modifies(timelineProposition41, timelineProposition40).
text(goalBox42, 'The narrator\'s goal for the narrator to open the door of the patio of the narrator.').
text(timelineProposition1, 'I left the patio door open').
text(timelineProposition7, until).
text(timelineProposition12, 'I saw them clinging to the ceiling.').
text(timelineProposition33, 'before sitting down,').
text(timelineProposition35, 'when it\'s going to jump out.').
text(timelineProposition39, 'yet.').
text(timelineProposition22, 'I only clipped that one,').
text(goal53, 'the narrator finds the leader of the group of bugs').
text(timelineProposition36, 'and the cicadas haven\'t even arrived yet.').
text(goal45, 'the group of bugs enters the apartment of the narrator').
text(timelineProposition3, 'just long enough').
text(timelineProposition40, 'All this,').
text(timelineProposition14, 'I grabbed the closest object within reach, and with a rolled-up comic book').
text(timelineProposition2, 'the other night,').
text(timelineProposition34, 'wondering when it\'s going to jump out.').
text(goal43, 'the narrator opens the door of the patio of the narrator').
text(timelineProposition15, 'Since I\'m such a bugaphobe,').
text(goalBox44, 'The group of bugs\'s goal for the group of bugs to enter the apartment of the narrator.').
text(timelineProposition16, 'mine enemies').
text(timelineProposition25, 'So now there\'s a five-limbed insect lurking in the apartment,').
text(goal49, 'the narrator kills the group of bugs').
text(goalBox48, 'The narrator\'s goal for the narrator to kill the group of bugs.').
text(timelineProposition28, 'So now').
text(timelineProposition4, 'to let in a dozen bugs of various size.').
text(timelineProposition24, 'a five-limbed insect').
text(goalBox46, 'The narrator\'s goal for the narrator to sleep.').
text(timelineProposition21, 'All except for the biggest one.').
text(timelineProposition29, 'no doubt looking for some vengeance against me.').
text(timelineProposition38, 'arrived yet.').
text(timelineProposition26, 'there\'s a five-limbed insect lurking').
text(timelineProposition8, 'until the middle of the night,').
text(timelineProposition31, 'I\'m looking around corners,').
text(goalBox52, 'The narrator\'s goal for the narrator to find the leader of the group of bugs.').
text(timelineProposition18, 'and with a rolled-up comic book').
text(timelineProposition37, even).
text(timelineProposition17, 'I smote mine enemies').
text(timelineProposition11, 'until the middle of the night,').
text(timelineProposition23, 'taking off one of its limbs.').
text(timelineProposition32, 'checking the toilet').
text(timelineProposition20, 'and smeared their greasy bug guts.').
text(goal47, 'the narrator sleeps').
text(goalBox50, 'The leader of the group of bugs\'s goal for the leader of the group of bugs to retaliate against the narrator.').
text(timelineProposition30, 'no doubt').
text(timelineProposition10, 'until the middle of the night, when').
text(goal51, 'the leader of the group of bugs retaliates against the narrator').
text(timelineProposition27, 'in the apartment,').
text(timelineProposition6, 'I didn\'t notice them').

