encoding('a024_heart_was_skipping_beats_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the heart of the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the narrator').
agent(timelineProposition16, 'the doctor').
agent(goalBox17, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal18, 'the narrator').
agent(goalBox19, 'the narrator').
agent(goal20, 'the narrator').
agent(goalBox21, 'the narrator').
agent(goal22, 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition6, goal22).
attemptToPrevent(timelineProposition5, goal22).
attemptToPrevent(timelineProposition4, goal22).
attemptToPrevent(timelineProposition7, goal22).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(goalBox17, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal18, goal).
type(goalBox19, goalBox).
type(goal20, goal).
type(goalBox21, goalBox).
type(goal22, goal).
sourceTextEndOffset(timelineProposition1, 63).
sourceTextEndOffset(timelineProposition2, 79).
sourceTextEndOffset(timelineProposition3, 105).
sourceTextEndOffset(timelineProposition4, 142).
sourceTextEndOffset(timelineProposition5, 188).
sourceTextEndOffset(timelineProposition6, 251).
sourceTextEndOffset(timelineProposition7, 272).
sourceTextEndOffset(timelineProposition8, 295).
sourceTextEndOffset(timelineProposition9, 346).
sourceTextEndOffset(timelineProposition10, 391).
sourceTextEndOffset(timelineProposition11, 466).
sourceTextEndOffset(timelineProposition13, 492).
sourceTextEndOffset(timelineProposition14, 514).
sourceTextEndOffset(timelineProposition15, 514).
sourceTextEndOffset(timelineProposition16, 631).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition8, goalBox21).
implies(timelineProposition2, goalBox17).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
equivalentOf(goal20, timelineProposition3).
equivalentOf(timelineProposition3, goal20).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal18, 'THE_NARRATOR:Core_Goal').
providesFor(goal20, 'THE_NARRATOR:Core_Goal').
providesFor(goal22, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 39).
sourceTextBeginOffset(timelineProposition2, 64).
sourceTextBeginOffset(timelineProposition3, 80).
sourceTextBeginOffset(timelineProposition4, 106).
sourceTextBeginOffset(timelineProposition5, 143).
sourceTextBeginOffset(timelineProposition6, 189).
sourceTextBeginOffset(timelineProposition7, 252).
sourceTextBeginOffset(timelineProposition8, 273).
sourceTextBeginOffset(timelineProposition9, 296).
sourceTextBeginOffset(timelineProposition10, 347).
sourceTextBeginOffset(timelineProposition11, 392).
sourceTextBeginOffset(timelineProposition13, 488).
sourceTextBeginOffset(timelineProposition14, 493).
sourceTextBeginOffset(timelineProposition15, 508).
sourceTextBeginOffset(timelineProposition16, 599).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal18, goalBox17).
interpNodeIn(goal20, goalBox19).
interpNodeIn(goal22, goalBox21).
sourceText(timelineProposition1, 'morning. I was all ready').
sourceText(timelineProposition2, 'to go to school').
sourceText(timelineProposition3, '&amp; eating my breakfast').
sourceText(timelineProposition4, 'when I got this really weird feeling').
sourceText(timelineProposition5, 'and it felt like my heart was skipping beats.').
sourceText(timelineProposition6, 'I couldn\'t move or anything, like my whole body was paralyzed.').
sourceText(timelineProposition7, 'Then it hurt so bad.').
sourceText(timelineProposition8, 'I started freaking out').
sourceText(timelineProposition9, 'to the point where I wanted to go to the hospital.').
sourceText(timelineProposition10, 'Atfirst I thought it was just a panic attack').
sourceText(timelineProposition11, 'but then I was like, uhh that\'\'s never happened when im having one before.').
sourceText(timelineProposition13, went).
sourceText(timelineProposition14, 'to the doctors at 10.').
sourceText(timelineProposition15, 'at 10.').
sourceText(timelineProposition16, 'So I got blood taken and an EKG.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition14, goal22).
attemptToCause(timelineProposition9, goal22).
attemptToCause(timelineProposition16, goal22).
attemptToCause(timelineProposition12, goal22).
attemptToCause(timelineProposition13, goal22).
attemptToCause(timelineProposition1, goal18).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition16).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition3, goalBox19).
interpretedAs(timelineProposition3, goal20).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition14, timelineProposition13).
modifies(timelineProposition15, timelineProposition13).
text(timelineProposition1, 'morning. I was all ready').
text(timelineProposition7, 'Then it hurt so bad.').
text(timelineProposition9, 'to the point where I wanted to go to the hospital.').
text(goal18, 'the narrator attends to a school').
text(timelineProposition8, 'I started freaking out').
text(timelineProposition13, went).
text(goalBox17, 'The narrator\'s goal for the narrator to attend to a school.').
text(timelineProposition4, 'when I got this really weird feeling').
text(timelineProposition15, 'at 10.').
text(timelineProposition10, 'Atfirst I thought it was just a panic attack').
text(timelineProposition16, 'So I got blood taken and an EKG.').
text(goalBox21, 'The narrator\'s goal for the narrator to be healthy.').
text(timelineProposition2, 'to go to school').
text(timelineProposition5, 'and it felt like my heart was skipping beats.').
text(goal20, 'the narrator eats a breakfast').
text(goalBox19, 'The narrator\'s goal for the narrator to eat a breakfast.').
text(timelineProposition3, '&amp; eating my breakfast').
text(timelineProposition14, 'to the doctors at 10.').
text(goal22, 'the narrator is healthy').
text(timelineProposition6, 'I couldn\'t move or anything, like my whole body was paralyzed.').
text(timelineProposition11, 'but then I was like, uhh that\'\'s never happened when im having one before.').

