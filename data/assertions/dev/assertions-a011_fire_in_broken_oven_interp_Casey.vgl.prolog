encoding('a011_fire_in_broken_oven_interp_Casey.vgl').
agent(timelineProposition1, 'the first oven').
agent(timelineProposition2, 'the first oven').
agent(timelineProposition3, 'the first oven').
agent(timelineProposition4, 'the first oven').
agent(timelineProposition5, 'the husband of the narrator').
agent(timelineProposition6, 'the husband of the narrator').
agent(timelineProposition7, 'the husband of the narrator').
agent(timelineProposition8, 'the husband of the narrator').
agent(timelineProposition9, 'the husband of the narrator').
agent(timelineProposition10, 'the husband of the narrator').
agent(timelineProposition11, 'the husband of the narrator').
agent(timelineProposition12, 'the narrator').
agent(timelineProposition13, 'the husband of the narrator').
agent(timelineProposition14, 'the fire').
agent(timelineProposition15, 'the fire').
agent(timelineProposition16, 'the first oven').
agent(timelineProposition17, 'the fire extinguisher').
agent(timelineProposition18, 'the fire').
agent(timelineProposition19, 'the house').
agent(timelineProposition20, 'the husband of the narrator').
agent(timelineProposition20, 'the narrator').
agent(timelineProposition21, 'the husband of the narrator').
agent(timelineProposition21, 'the narrator').
agent(timelineProposition22, 'the husband of the narrator').
agent(timelineProposition22, 'the narrator').
agent(timelineProposition23, 'the husband of the narrator').
agent(timelineProposition23, 'the narrator').
agent(timelineProposition24, 'the narrator').
agent(timelineProposition25, 'the narrator').
agent(timelineProposition26, 'the narrator').
agent(goalBox27, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal28, 'the narrator').
agent(goalBox29, 'the husband of the narrator').
agent(goalBox29, 'the narrator').
agent('THE_HUSBAND_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal', 'the husband of the narrator').
agent('THE_HUSBAND_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal30, 'the husband of the narrator').
agent(goal30, 'the narrator').
agent(goalBox31, 'the husband of the narrator').
agent(goalBox31, 'the narrator').
agent(goal32, 'the husband of the narrator').
agent(goal32, 'the narrator').
agent(goalBox33, 'the narrator').
agent(goal34, 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition4, goal28).
attemptToPrevent(timelineProposition14, goal30).
attemptToPrevent(timelineProposition6, goal28).
attemptToPrevent(timelineProposition3, goal28).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(timelineProposition16, timelineProposition).
type(timelineProposition17, timelineProposition).
type(timelineProposition18, timelineProposition).
type(timelineProposition19, timelineProposition).
type(timelineProposition20, timelineProposition).
type(timelineProposition21, timelineProposition).
type(timelineProposition22, timelineProposition).
type(timelineProposition23, timelineProposition).
type(timelineProposition24, timelineProposition).
type(timelineProposition25, timelineProposition).
type(timelineProposition26, timelineProposition).
type(goalBox27, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal28, goal).
type(goalBox29, goalBox).
type('THE_HUSBAND_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal', coreGoal).
type(goal30, goal).
type(goalBox31, goalBox).
type(goal32, goal).
type(goalBox33, goalBox).
type(goal34, goal).
sourceTextEndOffset(timelineProposition1, 179).
sourceTextEndOffset(timelineProposition2, 270).
sourceTextEndOffset(timelineProposition5, 312).
sourceTextEndOffset(timelineProposition6, 338).
sourceTextEndOffset(timelineProposition7, 367).
sourceTextEndOffset(timelineProposition8, 382).
sourceTextEndOffset(timelineProposition9, 413).
sourceTextEndOffset(timelineProposition10, 399).
sourceTextEndOffset(timelineProposition11, 446).
sourceTextEndOffset(timelineProposition12, 481).
sourceTextEndOffset(timelineProposition13, 505).
sourceTextEndOffset(timelineProposition14, 540).
sourceTextEndOffset(timelineProposition15, 558).
sourceTextEndOffset(timelineProposition16, 620).
sourceTextEndOffset(timelineProposition17, 595).
sourceTextEndOffset(timelineProposition18, 640).
sourceTextEndOffset(timelineProposition19, 699).
sourceTextEndOffset(timelineProposition20, 712).
sourceTextEndOffset(timelineProposition21, 752).
sourceTextEndOffset(timelineProposition22, 809).
sourceTextEndOffset(timelineProposition23, 831).
sourceTextEndOffset(timelineProposition24, 64).
sourceTextEndOffset(timelineProposition25, 19).
sourceTextEndOffset(timelineProposition26, 28).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition21, goal32).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition1, goal28).
ceases(timelineProposition16, goal28).
providesFor(dummy48, dummy49).
providesFor(goal32, 'THE_HUSBAND_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal').
providesFor(goal28, 'THE_NARRATOR:Core_Goal').
providesFor(goal30, 'THE_HUSBAND_OF_THE_NARRATOR_AND_THE_NARRATOR:Core_Goal').
providesFor(goal34, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 154).
sourceTextBeginOffset(timelineProposition2, 180).
sourceTextBeginOffset(timelineProposition5, 271).
sourceTextBeginOffset(timelineProposition6, 313).
sourceTextBeginOffset(timelineProposition7, 339).
sourceTextBeginOffset(timelineProposition8, 368).
sourceTextBeginOffset(timelineProposition9, 383).
sourceTextBeginOffset(timelineProposition10, 392).
sourceTextBeginOffset(timelineProposition11, 417).
sourceTextBeginOffset(timelineProposition12, 447).
sourceTextBeginOffset(timelineProposition13, 482).
sourceTextBeginOffset(timelineProposition14, 506).
sourceTextBeginOffset(timelineProposition15, 541).
sourceTextBeginOffset(timelineProposition16, 600).
sourceTextBeginOffset(timelineProposition17, 559).
sourceTextBeginOffset(timelineProposition18, 621).
sourceTextBeginOffset(timelineProposition19, 679).
sourceTextBeginOffset(timelineProposition20, 700).
sourceTextBeginOffset(timelineProposition21, 713).
sourceTextBeginOffset(timelineProposition22, 753).
sourceTextBeginOffset(timelineProposition23, 810).
sourceTextBeginOffset(timelineProposition24, 29).
sourceTextBeginOffset(timelineProposition25, 9).
sourceTextBeginOffset(timelineProposition26, 20).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal28, goalBox27).
interpNodeIn(goal30, goalBox29).
interpNodeIn(goal32, goalBox31).
interpNodeIn(goal34, goalBox33).
sourceText(timelineProposition1, 'The oven malfunctioned...').
sourceText(timelineProposition2, 'it was supposed to be baking at 350 degrees but somehow broiler came on and got stuck??...').
sourceText(timelineProposition5, 'my husband went to get a batch of cookies').
sourceText(timelineProposition6, 'and found them in flames.').
sourceText(timelineProposition7, 'He tried to put the fire out').
sourceText(timelineProposition8, unsuccessfully).
sourceText(timelineProposition9, 'and then quickly shut the door').
sourceText(timelineProposition10, quickly).
sourceText(timelineProposition11, 'keep the fire from spreading.').
sourceText(timelineProposition12, 'I ran and got a fire extinguisher.').
sourceText(timelineProposition13, 'He opened the oven door').
sourceText(timelineProposition14, 'and the flames shot all the way up').
sourceText(timelineProposition15, 'to the ceiling!!!').
sourceText(timelineProposition16, 'our stove was toast.').
sourceText(timelineProposition17, 'Luckily the fire extinguisher worked').
sourceText(timelineProposition18, 'Nothing got damaged').
sourceText(timelineProposition19, 'airing the house out').
sourceText(timelineProposition20, 'and cleaning').
sourceText(timelineProposition21, 'before things were 100% back to normal.').
sourceText(timelineProposition22, 'Since then we have been using a plug-in burner, griddle,').
sourceText(timelineProposition23, 'and counter top oven.').
sourceText(timelineProposition24, 'new range I ordered a few days ago.').
sourceText(timelineProposition25, 'So excited').
sourceText(timelineProposition26, 'about my').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition12, goal30).
attemptToCause(timelineProposition24, goal34).
attemptToCause(timelineProposition20, goal32).
attemptToCause(timelineProposition9, goal30).
attemptToCause(timelineProposition19, goal32).
attemptToCause(timelineProposition13, goal30).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition7).
followedBy(timelineProposition7, timelineProposition9).
followedBy(timelineProposition9, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
followedBy(timelineProposition13, timelineProposition14).
followedBy(timelineProposition14, timelineProposition16).
followedBy(timelineProposition16, timelineProposition17).
followedBy(timelineProposition17, timelineProposition18).
followedBy(timelineProposition18, timelineProposition19).
followedBy(timelineProposition19, timelineProposition20).
followedBy(timelineProposition20, timelineProposition22).
followedBy(timelineProposition22, timelineProposition23).
followedBy(timelineProposition23, timelineProposition24).
followedBy(timelineProposition24, timelineProposition25).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition7, goalBox29).
interpretedAs(timelineProposition24, goalBox33).
interpretedAs(timelineProposition11, goalBox29).
interpretedAs(timelineProposition17, goal30).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition8, timelineProposition7).
modifies(timelineProposition10, timelineProposition9).
modifies(timelineProposition11, timelineProposition9).
modifies(timelineProposition15, timelineProposition14).
modifies(timelineProposition21, timelineProposition20).
modifies(timelineProposition26, timelineProposition25).
text(timelineProposition11, 'keep the fire from spreading.').
text(timelineProposition6, 'and found them in flames.').
text(timelineProposition16, 'our stove was toast.').
text(goal34, 'the narrator replaces the first oven').
text(timelineProposition10, quickly).
text(goal30, 'the husband of the narrator and the narrator eliminates the fire').
text(timelineProposition13, 'He opened the oven door').
text(timelineProposition14, 'and the flames shot all the way up').
text(goalBox31, 'The husband of the narrator and the narrator goal for the husband of the narrator and the narrator to fix the house.').
text(timelineProposition19, 'airing the house out').
text(timelineProposition25, 'So excited').
text(timelineProposition12, 'I ran and got a fire extinguisher.').
text(timelineProposition2, 'it was supposed to be baking at 350 degrees but somehow broiler came on and got stuck??...').
text(timelineProposition20, 'and cleaning').
text(timelineProposition22, 'Since then we have been using a plug-in burner, griddle,').
text(goalBox27, 'The narrator\'s goal for the first oven to be functional.').
text(goal28, 'the first oven is functional').
text(timelineProposition23, 'and counter top oven.').
text(goal32, 'the husband of the narrator and the narrator fixes the house').
text(timelineProposition26, 'about my').
text(timelineProposition8, unsuccessfully).
text(timelineProposition18, 'Nothing got damaged').
text(timelineProposition7, 'He tried to put the fire out').
text(timelineProposition9, 'and then quickly shut the door').
text(timelineProposition1, 'The oven malfunctioned...').
text(timelineProposition15, 'to the ceiling!!!').
text(timelineProposition5, 'my husband went to get a batch of cookies').
text(timelineProposition21, 'before things were 100% back to normal.').
text(timelineProposition17, 'Luckily the fire extinguisher worked').
text(goalBox29, 'The husband of the narrator and the narrator goal for the husband of the narrator and the narrator to eliminate the fire.').
text(timelineProposition24, 'new range I ordered a few days ago.').
text(goalBox33, 'The narrator\'s goal for the narrator to replace the first oven.').

