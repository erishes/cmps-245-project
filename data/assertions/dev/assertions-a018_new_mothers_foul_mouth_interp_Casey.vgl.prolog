encoding('a018_new_mothers_foul_mouth_interp_Casey.vgl').
agent(timelineProposition1, 'the narrator').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the cat').
agent(timelineProposition12, 'the cat').
agent(timelineProposition13, 'the narrator').
agent(timelineProposition14, 'the narrator').
agent(timelineProposition15, 'the girl').
agent(goalBox16, 'the narrator').
agent(goal17, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
attemptToPrevent(dummy36, dummy37).
attemptToPrevent(timelineProposition13, goal17).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(timelineProposition14, timelineProposition).
type(timelineProposition15, timelineProposition).
type(goalBox16, goalBox).
type(goal17, goal).
type('THE_NARRATOR:Core_Goal', coreGoal).
sourceTextEndOffset(timelineProposition1, 32).
sourceTextEndOffset(timelineProposition2, 64).
sourceTextEndOffset(timelineProposition3, 122).
sourceTextEndOffset(timelineProposition4, 443).
sourceTextEndOffset(timelineProposition5, 648).
sourceTextEndOffset(timelineProposition6, 664).
sourceTextEndOffset(timelineProposition7, 680).
sourceTextEndOffset(timelineProposition8, 721).
sourceTextEndOffset(timelineProposition9, 612).
sourceTextEndOffset(timelineProposition10, 619).
sourceTextEndOffset(timelineProposition11, 837).
sourceTextEndOffset(timelineProposition12, 853).
sourceTextEndOffset(timelineProposition13, 991).
sourceTextEndOffset(timelineProposition14, 955).
sourceTextEndOffset(timelineProposition15, 1019).
because(dummy38, dummy39).
implies(dummy40, dummy41).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal17, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 13).
sourceTextBeginOffset(timelineProposition2, 33).
sourceTextBeginOffset(timelineProposition3, 65).
sourceTextBeginOffset(timelineProposition4, 392).
sourceTextBeginOffset(timelineProposition5, 620).
sourceTextBeginOffset(timelineProposition6, 649).
sourceTextBeginOffset(timelineProposition7, 665).
sourceTextBeginOffset(timelineProposition8, 692).
sourceTextBeginOffset(timelineProposition9, 594).
sourceTextBeginOffset(timelineProposition10, 613).
sourceTextBeginOffset(timelineProposition11, 796).
sourceTextBeginOffset(timelineProposition12, 838).
sourceTextBeginOffset(timelineProposition13, 923).
sourceTextBeginOffset(timelineProposition14, 942).
sourceTextBeginOffset(timelineProposition15, 992).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal17, goalBox16).
sourceText(timelineProposition1, 'children, I worried').
sourceText(timelineProposition2, 'about my incredibly foul mouth.').
sourceText(timelineProposition3, 'Would my kids start cursing like little toddling sailors?').
sourceText(timelineProposition4, 'But then I realized that I could control my cursing').
sourceText(timelineProposition5, 'dropped a big bottle of soda').
sourceText(timelineProposition6, 'on my bare foot').
sourceText(timelineProposition7, 'in the elevator').
sourceText(timelineProposition8, 'and shouted \'Oh, crumbles!!!\'').
sourceText(timelineProposition9, 'I was justly proud').
sourceText(timelineProposition10, 'when I').
sourceText(timelineProposition11, 'Our (stray, semi-adopted) cat took a dump').
sourceText(timelineProposition12, 'right on my bed').
sourceText(timelineProposition13, 'I groaned and when Zoe asked why I said \'Hissfahan shit on the bed\',').
sourceText(timelineProposition14, 'Zoe asked why').
sourceText(timelineProposition15, 'and she said \'what\'s shit?\'').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition4, goal17).
attemptToCause(timelineProposition8, goal17).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition8).
followedBy(timelineProposition8, timelineProposition9).
followedBy(timelineProposition9, timelineProposition11).
followedBy(timelineProposition11, timelineProposition13).
followedBy(timelineProposition13, timelineProposition15).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition3, goalBox16).
interpretedAs(timelineProposition15, goal17).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition6, timelineProposition5).
modifies(timelineProposition7, timelineProposition5).
modifies(timelineProposition10, timelineProposition9).
modifies(timelineProposition12, timelineProposition11).
modifies(timelineProposition14, timelineProposition13).
text(timelineProposition13, 'I groaned and when Zoe asked why I said \'Hissfahan shit on the bed\',').
text(timelineProposition12, 'right on my bed').
text(timelineProposition11, 'Our (stray, semi-adopted) cat took a dump').
text(timelineProposition9, 'I was justly proud').
text(timelineProposition8, 'and shouted \'Oh, crumbles!!!\'').
text(goalBox16, 'The narrator\'s goal for a child of the narrator to not be foul-mouthed.').
text(timelineProposition2, 'about my incredibly foul mouth.').
text(timelineProposition6, 'on my bare foot').
text(goal17, 'a child of the narrator isn\'t foul-mouthed').
text(timelineProposition14, 'Zoe asked why').
text(timelineProposition7, 'in the elevator').
text(timelineProposition3, 'Would my kids start cursing like little toddling sailors?').
text(timelineProposition5, 'dropped a big bottle of soda').
text(timelineProposition10, 'when I').
text(timelineProposition1, 'children, I worried').
text(timelineProposition15, 'and she said \'what\'s shit?\'').
text(timelineProposition4, 'But then I realized that I could control my cursing').

