encoding('a027_irrigation_canal_photo_opportunity_interp_Casey.vgl').
agent(timelineProposition1, 'a rain').
agent(timelineProposition2, 'the man').
agent(timelineProposition2, 'the narrator').
agent(timelineProposition3, 'the man').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the man').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the man').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the man').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the man').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the man').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'the man').
agent(timelineProposition12, 'the narrator').
agent(goalBox13, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal14, 'the narrator').
agent(goalBox15, 'the man').
agent(goalBox15, 'the narrator').
agent('THE_MAN_AND_THE_NARRATOR:Core_Goal', 'the man').
agent('THE_MAN_AND_THE_NARRATOR:Core_Goal', 'the narrator').
agent(goal16, 'the man').
agent(goal16, 'the narrator').
agent(goalBox17, 'the man').
agent(goalBox17, 'the narrator').
agent(goal18, 'the man').
agent(goal18, 'the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(goalBox13, goalBox).
type('THE_NARRATOR:Core_Goal', coreGoal).
type(goal14, goal).
type(goalBox15, goalBox).
type('THE_MAN_AND_THE_NARRATOR:Core_Goal', coreGoal).
type(goal16, goal).
type(goalBox17, goalBox).
type(goal18, goal).
sourceTextEndOffset(timelineProposition4, 480).
sourceTextEndOffset(timelineProposition5, 131).
sourceTextEndOffset(timelineProposition6, 274).
sourceTextEndOffset(timelineProposition8, 285).
sourceTextEndOffset(timelineProposition9, 334).
sourceTextEndOffset(timelineProposition10, 419).
sourceTextEndOffset(timelineProposition11, 662).
sourceTextEndOffset(timelineProposition12, 773).
because(dummy38, dummy39).
implies(dummy40, dummy41).
implies(timelineProposition11, goalBox17).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
equivalentOf(goal16, timelineProposition2).
equivalentOf(timelineProposition2, goal16).
ceases(dummy46, dummy47).
providesFor(dummy48, dummy49).
providesFor(goal18, 'THE_MAN_AND_THE_NARRATOR:Core_Goal').
providesFor(goal14, 'THE_NARRATOR:Core_Goal').
providesFor(goal16, 'THE_MAN_AND_THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition4, 460).
sourceTextBeginOffset(timelineProposition5, 97).
sourceTextBeginOffset(timelineProposition6, 151).
sourceTextBeginOffset(timelineProposition8, 275).
sourceTextBeginOffset(timelineProposition9, 282).
sourceTextBeginOffset(timelineProposition10, 388).
sourceTextBeginOffset(timelineProposition11, 635).
sourceTextBeginOffset(timelineProposition12, 723).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal14, goalBox13).
interpNodeIn(goal16, goalBox15).
interpNodeIn(goal18, goalBox17).
sourceText(timelineProposition4, 'Dear Preston managed').
sourceText(timelineProposition5, 'Yesterday, the canal caught my eye').
sourceText(timelineProposition6, 'The colors were deeper, saturated, and the landscape nearly a painting. An ordinary irrigation canal beautiful in the rain.').
sourceText(timelineProposition8, 'Pulled off').
sourceText(timelineProposition9, 'off on the side, no shoulder, of a two-lane highway,').
sourceText(timelineProposition10, 'was able to capture what I saw.').
sourceText(timelineProposition11, 'We spent the next few hours').
sourceText(timelineProposition12, 'and truly enjoyed our rainy photographic escapade.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition3, goal16).
attemptToCause(timelineProposition4, goal16).
attemptToCause(timelineProposition9, goal14).
attemptToCause(timelineProposition2, goal16).
attemptToCause(timelineProposition8, goal14).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition2).
followedBy(timelineProposition2, timelineProposition4).
followedBy(timelineProposition4, timelineProposition5).
followedBy(timelineProposition5, timelineProposition6).
followedBy(timelineProposition6, timelineProposition8).
followedBy(timelineProposition8, timelineProposition11).
followedBy(timelineProposition11, timelineProposition12).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition10, goal14).
interpretedAs(timelineProposition10, goalBox13).
interpretedAs(timelineProposition11, goal18).
interpretedAs(timelineProposition12, goal18).
interpretedAs(timelineProposition2, goalBox15).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition3, timelineProposition2).
modifies(timelineProposition7, timelineProposition6).
modifies(timelineProposition9, timelineProposition8).
modifies(timelineProposition10, timelineProposition8).
text(timelineProposition11, 'We spent the next few hours').
text(goalBox13, 'The narrator\'s goal for the narrator to photograph the color of the canal.').
text(timelineProposition6, 'The colors were deeper, saturated, and the landscape nearly a painting. An ordinary irrigation canal beautiful in the rain.').
text(timelineProposition4, 'Dear Preston managed').
text(timelineProposition5, 'Yesterday, the canal caught my eye').
text(timelineProposition12, 'and truly enjoyed our rainy photographic escapade.').
text(timelineProposition10, 'was able to capture what I saw.').
text(goal14, 'the narrator photographes the color of the canal').
text(goal16, 'the man and the narrator travels').
text(goal18, 'the man and the narrator photographes a rain').
text(timelineProposition8, 'Pulled off').
text(goalBox15, 'The man and the narrator goal for the man and the narrator to travel.').
text(timelineProposition9, 'off on the side, no shoulder, of a two-lane highway,').
text(goalBox17, 'The man and the narrator goal for the man and the narrator to photograph a rain.').

