encoding('a016_atm_mistake_gives_temporary_wealth_interp_Casey.vgl').
agent(timelineProposition1, 'ATM').
agent(timelineProposition2, 'ATM').
agent(timelineProposition3, 'the narrator').
agent(timelineProposition4, 'the narrator').
agent(timelineProposition5, 'the narrator').
agent(timelineProposition6, 'the narrator').
agent(timelineProposition7, 'the narrator').
agent(timelineProposition8, 'the narrator').
agent(timelineProposition9, 'the man').
agent(timelineProposition9, 'the narrator').
agent(timelineProposition10, 'the narrator').
agent(timelineProposition11, 'the narrator').
agent(timelineProposition12, 'a bank').
agent(timelineProposition13, 'the narrator').
agent(goalBox14, 'the narrator').
agent(goal15, 'the narrator').
agent('THE_NARRATOR:Core_Goal', 'the narrator').
attemptToPrevent(dummy36, dummy37).
type(timelineProposition1, timelineProposition).
type(timelineProposition2, timelineProposition).
type(timelineProposition3, timelineProposition).
type(timelineProposition4, timelineProposition).
type(timelineProposition5, timelineProposition).
type(timelineProposition6, timelineProposition).
type(timelineProposition7, timelineProposition).
type(timelineProposition8, timelineProposition).
type(timelineProposition9, timelineProposition).
type(timelineProposition10, timelineProposition).
type(timelineProposition11, timelineProposition).
type(timelineProposition12, timelineProposition).
type(timelineProposition13, timelineProposition).
type(goalBox14, goalBox).
type(goal15, goal).
type('THE_NARRATOR:Core_Goal', coreGoal).
sourceTextEndOffset(timelineProposition1, 97).
sourceTextEndOffset(timelineProposition2, 9).
sourceTextEndOffset(timelineProposition3, 166).
sourceTextEndOffset(timelineProposition5, 245).
sourceTextEndOffset(timelineProposition7, 273).
sourceTextEndOffset(timelineProposition8, 322).
sourceTextEndOffset(timelineProposition9, 758).
sourceTextEndOffset(timelineProposition10, 455).
sourceTextEndOffset(timelineProposition11, 362).
sourceTextEndOffset(timelineProposition12, 562).
sourceTextEndOffset(timelineProposition13, 528).
because(dummy38, dummy39).
implies(dummy40, dummy41).
preconditionFor(dummy42, dummy43).
equivalentOf(dummy44, dummy45).
ceases(dummy46, dummy47).
ceases(timelineProposition12, goal15).
providesFor(dummy48, dummy49).
providesFor(goal15, 'THE_NARRATOR:Core_Goal').
wouldPrevent(dummy50, dummy51).
damages(dummy52, dummy53).
sourceTextBeginOffset(timelineProposition1, 0).
sourceTextBeginOffset(timelineProposition2, 0).
sourceTextBeginOffset(timelineProposition3, 98).
sourceTextBeginOffset(timelineProposition5, 225).
sourceTextBeginOffset(timelineProposition7, 246).
sourceTextBeginOffset(timelineProposition8, 290).
sourceTextBeginOffset(timelineProposition9, 706).
sourceTextBeginOffset(timelineProposition10, 363).
sourceTextBeginOffset(timelineProposition11, 316).
sourceTextBeginOffset(timelineProposition12, 544).
sourceTextBeginOffset(timelineProposition13, 456).
interpNodeIn(dummy54, dummy55).
interpNodeIn(goal15, goalBox14).
sourceText(timelineProposition1, 'Last week this Commonwealth Bank ATM on Swanston was spitting out 50 dolla notes instead of 20\'s.').
sourceText(timelineProposition2, 'Last week').
sourceText(timelineProposition3, 'me and about a million others thought we had finally hit the jackpot').
sourceText(timelineProposition5, 'I took out 600 bucks').
sourceText(timelineProposition7, 'which turned into like 1300').
sourceText(timelineProposition8, 'I was so happy... so very happy.').
sourceText(timelineProposition9, 'drunk Paul and I took some photos that splendid nite').
sourceText(timelineProposition10, 'thought I\'d got away with this shit and could pay my credit card off and buy awesome shit...').
sourceText(timelineProposition11, 'happy. It came round to Friday this week and I').
sourceText(timelineProposition12, 'They took it back!').
sourceText(timelineProposition13, 'then I tried to get 20 bucks out and my account was 600 bucks overdrawn.').
inverseOf(dummy56, dummy57).
actualizes(dummy58, dummy59).
attemptToCause(dummy60, dummy61).
attemptToCause(timelineProposition5, goal15).
wouldCause(dummy62, dummy63).
followedBy(dummy64, dummy65).
followedBy(timelineProposition1, timelineProposition3).
followedBy(timelineProposition3, timelineProposition5).
followedBy(timelineProposition5, timelineProposition7).
followedBy(timelineProposition7, timelineProposition8).
followedBy(timelineProposition8, timelineProposition9).
followedBy(timelineProposition9, timelineProposition10).
followedBy(timelineProposition10, timelineProposition12).
followedBy(timelineProposition12, timelineProposition13).
interpretedAs(dummy66, dummy67).
interpretedAs(timelineProposition3, goalBox14).
interpretedAs(timelineProposition7, goal15).
causes(dummy68, dummy69).
modifies(dummy70, dummy71).
modifies(timelineProposition2, timelineProposition1).
modifies(timelineProposition4, timelineProposition3).
modifies(timelineProposition6, timelineProposition5).
modifies(timelineProposition11, timelineProposition10).
text(goalBox14, 'The narrator\'s goal for the narrator to obtain a money.').
text(timelineProposition13, 'then I tried to get 20 bucks out and my account was 600 bucks overdrawn.').
text(goal15, 'the narrator obtains a money').
text(timelineProposition7, 'which turned into like 1300').
text(timelineProposition2, 'Last week').
text(timelineProposition3, 'me and about a million others thought we had finally hit the jackpot').
text(timelineProposition11, 'happy. It came round to Friday this week and I').
text(timelineProposition12, 'They took it back!').
text(timelineProposition5, 'I took out 600 bucks').
text(timelineProposition10, 'thought I\'d got away with this shit and could pay my credit card off and buy awesome shit...').
text(timelineProposition8, 'I was so happy... so very happy.').
text(timelineProposition9, 'drunk Paul and I took some photos that splendid nite').
text(timelineProposition1, 'Last week this Commonwealth Bank ATM on Swanston was spitting out 50 dolla notes instead of 20\'s.').

