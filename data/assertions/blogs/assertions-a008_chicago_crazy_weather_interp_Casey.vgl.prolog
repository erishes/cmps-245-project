type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
type(proposition21, type, proposition).
type(proposition22, type, proposition).
type(proposition23, type, proposition).
type(goal24, type, goal).
type(interpAction25, type, interpAction).
type(leisure26, type, leisure).
type(goal27, type, goal).
type(interpAction28, type, interpAction).
type(life29, type, life).
followedBy(proposition1, proposition3).
followedBy(proposition3, proposition5).
followedBy(proposition5, proposition7).
followedBy(proposition7, proposition9).
followedBy(proposition9, proposition10).
followedBy(proposition10, proposition11).
followedBy(proposition11, proposition13).
followedBy(proposition13, proposition16).
followedBy(proposition16, proposition17).
followedBy(proposition17, proposition18).
followedBy(proposition18, proposition19).
followedBy(proposition19, proposition21).
followedBy(proposition21, proposition23).
ceases(proposition21, interpAction28).
ceases(proposition11, interpAction25).
modifies(proposition2, proposition1).
modifies(proposition4, proposition3).
modifies(proposition6, proposition5).
modifies(proposition8, proposition7).
modifies(proposition12, proposition11).
modifies(proposition14, proposition13).
modifies(proposition15, proposition13).
modifies(proposition20, proposition19).
modifies(proposition22, proposition21).
agent(goal24, '"anon_GROUP(CharacterGender.Neutral, Root Level(CharacterGender.Neutral), (John, anon_noun10345804:narrator(CharacterGender.Female)_1))_1"').
agent(leisure26, '"anon_GROUP(CharacterGender.Neutral, Root Level(CharacterGender.Neutral), (John, anon_noun10345804:narrator(CharacterGender.Female)_1))_1"').
agent(goal27, '"anon_noun10345804:narrator(CharacterGender.Female)_1"').
agent(life29, '"anon_GROUP(CharacterGender.Neutral, noun2430045:deer(CharacterGender.Neutral), ())_1"').
attemptToCause(proposition7, interpAction25).
interpNodeIn(interpAction25, goal24).
interpNodeIn(leisure26, goal24).
interpNodeIn(interpAction28, goal27).
interpNodeIn(life29, goal27).
providesFor(interpAction28, life29).
providesFor(interpAction25, leisure26).
verbalization(interpAction25, 'a man and a narrator who owned a basement and owned a home watches a movie').
verbalization(interpAction28, 'a narrator who owned a basement and owned a home brings to the home a group of deer').
interpretedAs(proposition19, goal27).
interpretedAs(proposition10, goal24).
sourceText(proposition1, 'We've seriously had some crazy weather these past couple of days... it was nonstop rain, and more rain!').
sourceText(proposition3, 'We got a tiny amount of water in our basement,').
sourceText(proposition5, 'The rain finally let up early this afternoon,').
sourceText(proposition6, 'early this afternoon,').
sourceText(proposition7, 'and John and I went driving').
sourceText(proposition8, 'and I').
sourceText(proposition9, 'around and were stunned by all the flooding in our neighborhood.').
sourceText(proposition10, 'We tried to go see a movie,').
sourceText(proposition11, 'but couldn't').
sourceText(proposition12, 'get to the movie theatre because all the surrounding roads were flooded and closed off.').
sourceText(proposition13, 'At one point we saw about 25 deer').
sourceText(proposition14, 'in a clearing').
sourceText(proposition15, 'near a forest preserve,').
sourceText(proposition16, 'and I saw a bunch more deer').
sourceText(proposition17, 'When I looked into the forest, I could see how flooded and murky it was.').
sourceText(proposition18, 'The poor deer have nowhere to go! I feel so bad for them.').
sourceText(proposition19, 'yes, I wanted to.').
sourceText(proposition21, 'John would not let me bring any of them').
sourceText(proposition22, 'home with us...').
sourceText(proposition23, 'My dad told me that coyotes are traveling around the city in packs in broad daylight, looking for food,').

