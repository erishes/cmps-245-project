type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
sourceText(proposition1, 'when he crashed his motorbike.').
sourceText(proposition2, 'A few years ago,').
sourceText(proposition3, 'out on my front lawn').
sourceText(proposition4, 'he was badly injured with a broken neck.').
sourceText(proposition5, 'I was with him in his last moments').
sourceText(proposition6, 'there was nothing I could do').
sourceText(proposition7, 'because he was badly injured with a broken neck.').
sourceText(proposition8, 'a young man died').
sourceText(proposition9, 'out on my front lawn').
sourceText(proposition10, 'when I was visiting the place where he died,').
sourceText(proposition11, 'Later,').
sourceText(proposition12, 'I decided to talk to him as if he were still there').
sourceText(proposition13, 'because I wanted him to know how bad I felt that I couldnt help him.').
sourceText(proposition14, 'I saw this ball of light').
sourceText(proposition15, 'in the trees above me.').
sourceText(proposition16, 'my logical mind thought that it was my brother playing around with his new flashlight').
sourceText(proposition17, 'my logical mind').
sourceText(proposition18, 'I went inside the house,').
sourceText(proposition19, 'everyone was snoring!').
sourceText(proposition20, 'could this have been what they call an orb PS.').
followedBy(proposition1, proposition4).
followedBy(proposition4, proposition5).
followedBy(proposition5, proposition6).
followedBy(proposition6, proposition8).
followedBy(proposition8, proposition10).
followedBy(proposition10, proposition12).
followedBy(proposition12, proposition14).
followedBy(proposition14, proposition16).
followedBy(proposition16, proposition18).
followedBy(proposition18, proposition19).
followedBy(proposition19, proposition20).
modifies(proposition2, proposition1).
modifies(proposition3, proposition1).
modifies(proposition7, proposition6).
modifies(proposition9, proposition8).
modifies(proposition11, proposition10).
modifies(proposition13, proposition12).
modifies(proposition15, proposition14).
modifies(proposition17, proposition16).

