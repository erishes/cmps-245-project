type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
type(proposition21, type, proposition).
type(goal22, type, goal).
type(health23, type, health).
type(interpAction24, type, interpAction).
type(goal25, type, goal).
type(interpAction26, type, interpAction).
type(health27, type, health).
type(goal28, type, goal).
type(health29, type, health).
type(interpAction30, type, interpAction).
type(goal31, type, goal).
type(health32, type, health).
type(interpAction33, type, interpAction).
implies(proposition10, goal31).
attemptToPrevent(proposition13, interpAction33).
attemptToPrevent(proposition11, interpAction33).
attemptToPrevent(proposition15, interpAction33).
attemptToPrevent(proposition18, interpAction33).
attemptToPrevent(proposition20, interpAction33).
followedBy(proposition1, proposition3).
followedBy(proposition3, proposition4).
followedBy(proposition4, proposition5).
followedBy(proposition5, proposition6).
followedBy(proposition6, proposition9).
followedBy(proposition9, proposition10).
followedBy(proposition10, proposition11).
followedBy(proposition11, proposition13).
followedBy(proposition13, proposition15).
followedBy(proposition15, proposition18).
followedBy(proposition18, proposition20).
followedBy(proposition20, proposition21).
ceases(proposition21, interpAction33).
modifies(proposition2, proposition1).
modifies(proposition7, proposition6).
modifies(proposition8, proposition6).
modifies(proposition12, proposition11).
modifies(proposition14, proposition13).
modifies(proposition16, proposition15).
modifies(proposition17, proposition15).
modifies(proposition19, proposition18).
agent(goal22, '"anon_noun10345804:narrator(CharacterGender.Female)_1"').
agent(health23, '"Benjamin"').
agent(goal25, '"anon_GROUP(CharacterGender.Neutral, noun1503061:bird(CharacterGender.Neutral), ())_1"').
agent(health27, '"anon_GROUP(CharacterGender.Neutral, noun1503061:bird(CharacterGender.Neutral), ())_1"').
agent(goal28, '"anon_GROUP(CharacterGender.Neutral, noun2355227:squirrel(CharacterGender.Neutral), ())_1"').
agent(health29, '"anon_GROUP(CharacterGender.Neutral, noun2355227:squirrel(CharacterGender.Neutral), ())_1"').
agent(goal31, '"anon_noun2355227:squirrel(CharacterGender.Neutral)_1"').
agent(health32, '"anon_noun2355227:squirrel(CharacterGender.Neutral)_1"').
attemptToCause(proposition10, interpAction33).
attemptToCause(proposition1, interpAction24).
interpretedAs(proposition9, goal28).
interpretedAs(proposition9, interpAction30).
interpretedAs(proposition4, interpAction26).
interpretedAs(proposition4, goal25).
interpretedAs(proposition2, goal22).
interpNodeIn(health23, goal22).
interpNodeIn(interpAction24, goal22).
interpNodeIn(interpAction26, goal25).
interpNodeIn(health27, goal25).
interpNodeIn(health29, goal28).
interpNodeIn(interpAction30, goal28).
interpNodeIn(health32, goal31).
interpNodeIn(interpAction33, goal31).
providesFor(interpAction30, health29).
providesFor(interpAction26, health27).
providesFor(interpAction33, health32).
providesFor(interpAction24, health23).
verbalization(interpAction24, 'a narrator provides a water to a dog').
verbalization(interpAction26, 'a group of birds drinks').
verbalization(interpAction30, 'a group of squirrels drinks').
verbalization(interpAction33, 'a crazy squirrel drinks').
sourceText(proposition1, 'We keep a large stainless steel bowl of water outside on the back deck').
sourceText(proposition2, 'for Benjamin to drink out of').
sourceText(proposition3, 'His bowl has become a very popular site.').
sourceText(proposition4, 'Throughout the day, many birds drink out of it').
sourceText(proposition5, 'and bathe in it.').
sourceText(proposition6, 'The birds literally line up').
sourceText(proposition7, 'on the railing').
sourceText(proposition8, 'and wait their turn.').
sourceText(proposition9, 'Squirrels also come to drink out of it.').
sourceText(proposition10, 'The craziest squirrel just came by-').
sourceText(proposition11, 'he was literally jumping in fright').
sourceText(proposition12, 'at what I believe was his own reflection').
sourceText(proposition13, 'He was startled so much at one point that he leap').
sourceText(proposition14, 'He was startled so much at one point').
sourceText(proposition15, 'and fell').
sourceText(proposition16, 'off the deck.').
sourceText(proposition18, 'But not quite, I saw his one little paw hanging on!').
sourceText(proposition19, 'saw his one little paw hanging on!').
sourceText(proposition20, 'After a moment or two his paw slipped').
sourceText(proposition21, 'and he tumbled down a few feet.').

