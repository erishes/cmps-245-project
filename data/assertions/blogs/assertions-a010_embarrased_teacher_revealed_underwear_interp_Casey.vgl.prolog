type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
type(proposition21, type, proposition).
type(proposition22, type, proposition).
type(goal23, type, goal).
type(enlightenment24, type, enlightenment).
type(interpAction25, type, interpAction).
type(goal26, type, goal).
type(ego27, type, ego).
type(interpAction28, type, interpAction).
implies(proposition11, goal26).
attemptToPrevent(proposition5, interpAction28).
followedBy(proposition1, proposition2).
followedBy(proposition2, proposition3).
followedBy(proposition3, proposition5).
followedBy(proposition5, proposition7).
followedBy(proposition7, proposition8).
followedBy(proposition8, proposition9).
followedBy(proposition9, proposition11).
followedBy(proposition11, proposition12).
followedBy(proposition12, proposition13).
followedBy(proposition13, proposition15).
followedBy(proposition15, proposition16).
followedBy(proposition16, proposition17).
followedBy(proposition17, proposition19).
followedBy(proposition19, proposition20).
followedBy(proposition20, proposition21).
ceases(proposition7, interpAction28).
modifies(proposition4, proposition3).
modifies(proposition6, proposition5).
modifies(proposition10, proposition9).
modifies(proposition14, proposition13).
modifies(proposition18, proposition17).
modifies(proposition22, proposition21).
agent(goal23, '"anon_noun10345804:narrator(CharacterGender.Female)_1"').
agent(enlightenment24, '"anon_GROUP(CharacterGender.Neutral, Root Level(CharacterGender.Neutral), ())_1"').
agent(goal26, '"anon_noun10345804:narrator(CharacterGender.Female)_1"').
agent(ego27, '"anon_noun10345804:narrator(CharacterGender.Female)_1"').
attemptToCause(proposition4, interpAction25).
attemptToCause(proposition2, interpAction25).
attemptToCause(proposition3, interpAction25).
attemptToCause(proposition19, interpAction25).
attemptToCause(proposition15, interpAction28).
attemptToCause(proposition12, interpAction28).
attemptToCause(proposition13, interpAction28).
attemptToCause(proposition16, interpAction28).
providesFor(interpAction25, enlightenment24).
providesFor(interpAction28, ego27).
interpNodeIn(enlightenment24, goal23).
interpNodeIn(interpAction25, goal23).
interpNodeIn(ego27, goal26).
interpNodeIn(interpAction28, goal26).
verbalization(interpAction25, 'a narrator who was a teacher, owned some white slip and owned a desk teaches a group of students').
verbalization(interpAction28, 'a narrator who was a teacher, owned some white slip and owned a desk conceals the slip').
interpretedAs(proposition17, interpAction28).
interpretedAs(proposition1, goal23).
interpretedAs(proposition18, interpAction28).
sourceText(proposition1, 'Ten years ago I was teaching a mixed class of 5th graders ESL.').
sourceText(proposition2, 'I had taken the register').
sourceText(proposition3, 'and was standing').
sourceText(proposition4, 'at the front of the class').
sourceText(proposition5, 'underslip had somehow made').
sourceText(proposition6, 'Elastic gone').
sourceText(proposition7, 'However, all eyes were not on my face but at my ankles.').
sourceText(proposition8, 'However, all eyes were not on my face but at my ankles.').
sourceText(proposition9, 'I looked down').
sourceText(proposition10, 'Nervously').
sourceText(proposition11, 'to see that my underslip had somehow made its way to the floor.').
sourceText(proposition12, 'So I looked at my underwear round my ankles, looked at the class, looked back at the garment,').
sourceText(proposition13, 'stepped out of it,').
sourceText(proposition14, carefully).
sourceText(proposition15, 'said, "Oh dear look, what on earth can that be?",').
sourceText(proposition16, 'be?", lifted the offending white (not red and black thank the Lord) slip').
sourceText(proposition17, 'and casually popped it in').
sourceText(proposition18, 'the bottom drawer of my desk.').
sourceText(proposition19, 'I continued, "Ok class, page 33."').
sourceText(proposition20, 'No one, strangely, batted an eyelid.').
sourceText(proposition21, 'I wondered how they had described the day to their parents over dinner. "So Ahmed, what did you do in school today?" "We looked at the teacher's underwear."').
sourceText(proposition22, 'eyelid. Later that day').

