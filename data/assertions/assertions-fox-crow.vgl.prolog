type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
type(proposition21, type, proposition).
type(interpAction22, type, interpAction).
type(interpCondition23, type, interpCondition).
type(goal24, type, goal).
type(interpAction25, type, interpAction).
type(interpAction26, type, interpAction).
type(interpAction27, type, interpAction).
type(health28, type, health).
type(ego29, type, ego).
type(goal30, type, goal).
type(interpAction31, type, interpAction).
type(goal32, type, goal).
type(ego33, type, ego).
type(interpAction34, type, interpAction).
type(belief35, type, belief).
type(interpCondition36, type, interpCondition).
type(interpAction37, type, interpAction).
wouldCause(interpAction25, interpAction27).
wouldCause(interpAction34, interpCondition36).
wouldCause(goal32, interpAction37).
wouldCause(interpAction26, interpAction25).
wouldCause(interpAction37, interpAction26).
wouldCause(interpAction31, goal32).
implies(proposition20, interpCondition23).
attemptToPrevent(proposition20, belief35).
followedBy(proposition1, proposition3).
followedBy(proposition3, proposition4).
followedBy(proposition4, proposition5).
followedBy(proposition5, proposition6).
followedBy(proposition6, proposition7).
followedBy(proposition7, proposition9).
followedBy(proposition9, proposition10).
followedBy(proposition10, proposition11).
followedBy(proposition11, proposition12).
followedBy(proposition12, proposition13).
followedBy(proposition13, proposition14).
followedBy(proposition14, proposition15).
followedBy(proposition15, proposition18).
followedBy(proposition18, proposition19).
followedBy(proposition19, proposition20).
followedBy(proposition20, proposition21).
modifies(proposition2, proposition1).
modifies(proposition8, proposition7).
modifies(proposition16, proposition15).
modifies(proposition17, proposition15).
agent(goal24, '"anon_noun2118333:fox(CharacterGender.Male)_1"').
agent(health28, '"anon_noun2118333:fox(CharacterGender.Male)_1"').
agent(ego29, '"anon_noun2118333:fox(CharacterGender.Male)_1"').
agent(goal30, '"anon_noun2118333:fox(CharacterGender.Male)_1"').
agent(goal32, '"anon_noun1579028:crow(CharacterGender.Female)_1"').
agent(ego33, '"anon_noun1579028:crow(CharacterGender.Female)_1"').
agent(belief35, '"anon_noun2118333:fox(CharacterGender.Male)_1"').
attemptToCause(proposition10, interpAction31).
attemptToCause(proposition8, interpAction31).
attemptToCause(proposition12, interpAction31).
attemptToCause(proposition6, interpAction31).
attemptToCause(proposition11, interpAction31).
attemptToCause(proposition9, interpAction31).
attemptToCause(proposition13, interpAction31).
attemptToCause(proposition16, belief35).
attemptToCause(proposition7, interpAction31).
damages(interpAction22, ego33).
wouldPrevent(interpCondition23, belief35).
actualizes(proposition18, interpAction25).
actualizes(proposition19, interpAction27).
actualizes(proposition15, interpAction34).
actualizes(proposition15, interpAction26).
actualizes(proposition15, interpAction37).
actualizes(proposition14, interpAction31).
providesFor(interpAction27, ego29).
providesFor(interpAction27, health28).
providesFor(belief35, ego33).
providesFor(interpCondition36, ego33).
interpretedAs(proposition17, belief35).
interpretedAs(proposition21, interpAction22).
interpretedAs(proposition5, goal24).
interpNodeIn(interpAction25, goal24).
interpNodeIn(interpAction26, goal24).
interpNodeIn(interpAction27, goal24).
interpNodeIn(interpAction31, goal30).
interpNodeIn(ego33, goal32).
interpNodeIn(interpAction34, goal32).
interpNodeIn(interpCondition36, belief35).
interpNodeIn(belief35, goal32).
interpNodeIn(goal32, goal30).
interpNodeIn(interpAction37, goal30).
verbalization(interpAction22, 'a fox insults a crow').
verbalization(interpCondition23, 'a fox doesnt believe that a crow is able to sing').
verbalization(interpAction25, 'a crow drops some cheese').
verbalization(interpAction26, 'a crow opens the beak of the crow').
verbalization(interpAction27, 'a fox obtains some cheese from the beak of a crow').
verbalization(interpAction31, 'a fox flatters a crow').
verbalization(interpAction34, 'a crow sings').
verbalization(interpCondition36, 'a crow is able to sing').
verbalization(interpAction37, 'a crow sings').
sourceText(proposition1, 'A Crow was sitting').
sourceText(proposition2, 'on a branch of a tree').
sourceText(proposition3, 'with a piece of cheese in her beak').
sourceText(proposition4, 'when a Fox observed her').
sourceText(proposition5, 'and set his wits to work to discover some way of getting the cheese.').
sourceText(proposition6, 'Coming').
sourceText(proposition7, 'and standing').
sourceText(proposition8, 'under the tree').
sourceText(proposition9, 'he looked up').
sourceText(proposition10, 'and said, "What a noble bird I see above me!').
sourceText(proposition11, 'Her beauty is without equal,').
sourceText(proposition12, 'the hue of her plumage exquisite.').
sourceText(proposition13, 'If only her voice is as sweet as her looks are fair, she ought without doubt to be Queen of the Birds."').
sourceText(proposition14, 'The Crow was hugely flattered by this,').
sourceText(proposition15, 'she gave a loud caw.').
sourceText(proposition16, loud).
sourceText(proposition17, 'to show the Fox that she could sing').
sourceText(proposition18, 'Down came the cheese,of').
sourceText(proposition19, 'the Fox, snatching it up,').
sourceText(proposition20, 'said, "You have a voice, madam,').
sourceText(proposition21, 'what you want is wits."').

