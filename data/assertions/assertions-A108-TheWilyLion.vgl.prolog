encoding('A108').
type(proposition1, type, proposition).
type(proposition2, type, proposition).
type(proposition3, type, proposition).
type(proposition4, type, proposition).
type(proposition5, type, proposition).
type(proposition6, type, proposition).
type(proposition7, type, proposition).
type(proposition8, type, proposition).
type(proposition9, type, proposition).
type(proposition10, type, proposition).
type(proposition11, type, proposition).
type(proposition12, type, proposition).
type(proposition13, type, proposition).
type(proposition14, type, proposition).
type(proposition15, type, proposition).
type(proposition16, type, proposition).
type(proposition17, type, proposition).
type(proposition18, type, proposition).
type(proposition19, type, proposition).
type(proposition20, type, proposition).
type(proposition21, type, proposition).
type(proposition22, type, proposition).
type(proposition23, type, proposition).
type(proposition24, type, proposition).
type(proposition25, type, proposition).
type(proposition26, type, proposition).
type(proposition27, type, proposition).
type(proposition28, type, proposition).
type(proposition29, type, proposition).
type(interpCondition30, type, interpCondition).
type(interpCondition31, type, interpCondition).
type(goal32, type, goal).
type(interpAction33, type, interpAction).
type(interpAction34, type, interpAction).
type(health35, type, health).
type(interpAction36, type, interpAction).
type(interpAction37, type, interpAction).
type(interpAction38, type, interpAction).
type(goal39, type, goal).
type(health40, type, health).
type(interpAction41, type, interpAction).
type(belief42, type, belief).
type(interpCondition43, type, interpCondition).
wouldCause(interpCondition31, interpAction34).
wouldCause(interpAction38, interpAction37).
wouldCause(interpCondition30, interpAction36).
wouldCause(interpCondition43, interpAction38).
implies(proposition1, interpCondition31).
implies(proposition6, interpCondition43).
implies(proposition4, interpCondition30).
followedBy(proposition1, proposition2).
followedBy(proposition2, proposition4).
followedBy(proposition4, proposition6).
followedBy(proposition6, proposition8).
followedBy(proposition8, proposition10).
followedBy(proposition10, proposition11).
followedBy(proposition11, proposition12).
followedBy(proposition12, proposition14).
followedBy(proposition14, proposition15).
followedBy(proposition15, proposition16).
followedBy(proposition16, proposition17).
followedBy(proposition17, proposition18).
followedBy(proposition18, proposition20).
followedBy(proposition20, proposition21).
followedBy(proposition21, proposition23).
followedBy(proposition23, proposition24).
followedBy(proposition24, proposition25).
followedBy(proposition25, proposition26).
followedBy(proposition26, proposition27).
followedBy(proposition27, proposition28).
ceases(proposition27, interpCondition43).
modifies(proposition3, proposition2).
modifies(proposition5, proposition4).
modifies(proposition7, proposition6).
modifies(proposition9, proposition8).
modifies(proposition13, proposition12).
modifies(proposition19, proposition18).
modifies(proposition22, proposition21).
modifies(proposition29, proposition28).
agent(goal32, '"anon_noun2129165:lion(CharacterGender.Male)_1"').
agent(health35, '"anon_noun2129165:lion(CharacterGender.Male)_1"').
agent(goal39, '"anon_noun2403325:bull(CharacterGender.Male)_1"').
agent(health40, '"anon_noun2403325:bull(CharacterGender.Male)_1"').
agent(belief42, '"anon_noun2129165:lion(CharacterGender.Male)_1"').
attemptToCause(proposition21, interpAction38).
attemptToCause(proposition23, interpAction38).
attemptToCause(proposition19, interpAction38).
attemptToCause(proposition15, interpAction33).
attemptToCause(proposition18, interpAction38).
attemptToCause(proposition13, interpAction38).
attemptToCause(proposition20, interpAction38).
attemptToCause(proposition16, interpAction33).
attemptToCause(proposition12, interpAction38).
attemptToCause(proposition8, interpAction34).
attemptToCause(proposition14, interpAction33).
attemptToCause(proposition17, interpAction33).
preconditionFor(interpAction34, interpAction36).
preconditionFor(interpAction38, interpAction34).
preconditionFor(interpAction33, interpAction38).
preconditionFor(interpAction37, interpAction34).
wouldPrevent(interpCondition43, interpAction34).
actualizes(proposition28, interpAction34).
actualizes(proposition26, interpAction37).
actualizes(proposition5, interpCondition30).
actualizes(proposition24, interpAction38).
actualizes(proposition25, interpAction38).
actualizes(proposition2, interpAction41).
providesFor(interpAction36, health35).
providesFor(interpAction41, health40).
interpretedAs(proposition7, interpCondition43).
interpretedAs(proposition10, interpCondition43).
interpretedAs(proposition11, interpAction38).
interpretedAs(proposition9, interpCondition31).
interpNodeIn(interpAction33, goal32).
interpNodeIn(interpAction34, goal32).
interpNodeIn(health35, goal32).
interpNodeIn(interpAction36, goal32).
interpNodeIn(interpAction37, goal32).
interpNodeIn(interpAction38, goal32).
interpNodeIn(health40, goal39).
interpNodeIn(interpAction41, goal39).
interpNodeIn(interpCondition43, belief42).
verbalization(interpCondition30, 'a fat bull is tasty').
verbalization(interpCondition31, 'a lion is hungry').
verbalization(interpAction33, 'a lion flatters a fat bull').
verbalization(interpAction34, 'a lion kills a fat bull').
verbalization(interpAction36, 'a lion eats a fat bull').
verbalization(interpAction37, 'a fat bull removes the sharp horn of the bull').
verbalization(interpAction38, 'a lion tricks a fat bull').
verbalization(interpAction41, 'a fat bull eats').
verbalization(interpCondition43, 'a fat bull is deadly').
sourceText(proposition1, 'A Lion watched a fat Bull').
sourceText(proposition2, feeding).
sourceText(proposition3, 'in a meadow,').
sourceText(proposition4, 'and his mouth watered').
sourceText(proposition5, 'when he thought of the royal feast he would make,').
sourceText(proposition6, 'but he did not dare to attack him,').
sourceText(proposition7, 'for he was afraid of his sharp horns.').
sourceText(proposition8, 'presently compelled him to do something:').
sourceText(proposition9, 'Hunger,').
sourceText(proposition10, 'and as the use of force did not promise success,').
sourceText(proposition11, 'he determined to resort to artifice.').
sourceText(proposition12, 'Going up to the Bull in friendly fashion,').
sourceText(proposition13, friendly).
sourceText(proposition14, '"I cannot help saying how much I admire your magnificent figure.').
sourceText(proposition15, 'What a fine head!  What').
sourceText(proposition16, 'What powerful shoulders and').
sourceText(proposition17, 'thighs!').
sourceText(proposition18, 'But, my dear friend, what in the world makes you wear those ugly horns?').
sourceText(proposition19, 'ugly horns?').
sourceText(proposition20, 'You must find them as awkward as').
sourceText(proposition21, 'they are unsightly.').
sourceText(proposition23, 'Believe me, you would do much better without them."').
sourceText(proposition24, 'The Bull was foolish').
sourceText(proposition25, 'enough to be persuaded by this flattery').
sourceText(proposition26, 'to have his horns cut off;').
sourceText(proposition27, 'and, having now lost his only means of defence,').
sourceText(proposition28, 'fell an easy prey to the Lion').
sourceText(proposition29, easy).

