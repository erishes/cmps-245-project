#!/usr/bin/python
# Stephanie Lukin, Lena Rishes

import sys

# Transforms a direct speech utterance realized by RealPro into different formats of direct speech
class trimRealPro:
    def __init__(self, filename):
        self.filename = filename

    def trim(self):

        with open(self.filename, 'r') as realpro_file:
            story = realpro_file.readlines()

            part = str(story).partition("Printing document with realized DSyntSs (XML format)")
            story = str(part[2]).partition("<dsynts-list>")
            story = story[2]

            story = story.replace("'\r\n', \n, \r, \\n', '\\n', '", "") # remove \n that was printed in partition
            story = story.replace("</dsynts-list>\\n\']", "")
            story = story.replace("       ", "")
            story = story.replace("\\n\", \"", " ")
            story = story.replace("\\n\", \'", " ")
            story = story.replace("\\n\', \"", " ")
            story = story.replace("\\n\', \'", " ")
            story = story.replace("      </dsynts-list>\\r\\n\']", "")
            story = story.replace("<between_quotes>", "\"")
            story = story.replace("</between_quotes>  ", "\"")
            story = story.replace("<rightmost_comma>", "")
            story = story.replace("</rightmost_comma>  ", ", ")
            story = story.replace("   <rightmost_exclam>", "")
            story = story.replace("</rightmost_exclam> ", "!")

            file = open(self.filename, 'w')
            file.write(story)
            file.close()

if ( __name__ == '__main__'):
        e = trimRealPro(sys.argv[1])
        e.trim()